/****************************************************************************** 
 * 
 *  file:  bucketsearch_mt.hpp
 * 
 *  Copyright (c) 2016, Grand R. Joldes.
 *  All rights reverved.
 * 
 *  
 *  THE SOFTWARE IS PROVIDED _AS IS_, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *  DEALINGS IN THE SOFTWARE.  
 *  
 *****************************************************************************/ 

#ifndef __BUCKETSEARCH_MT_HPP__
#define __BUCKETSEARCH_MT_HPP__

#include <boost/atomic.hpp>

#include "bucketsearch.hpp"

namespace BucketSearch {

class BucketSearch_MT: public BucketSearch {
public:
	BucketSearch_MT(buckets *pBuckets_,float *pfPointsCoords, unsigned int uiNumPoints);
	~BucketSearch_MT();

	void vDistributePointsToBucketsMT(void);
	void vSetNumberOfThreads(unsigned int uiNum) {uiNumberOfThreads = uiNum;};

	// Structures with additional control variables
	typedef struct stThreadControl {
		bool boHandlePoints;
		unsigned int uiFirstPointIdx, uiLastPointIdx;
	} tstThreadControl;
	friend void vBucketPointsThread(BucketSearch_MT *pBucketSearch, tstThreadControl *pstThreadControl);
	
private:
	tstThreadControl *astThreadControl;
	boost::atomic<int> *aAtomicInt;
	unsigned int uiNumberOfThreads;
};

} // namespace
#endif /* __BUCKETSEARCH_MT_HPP__ */