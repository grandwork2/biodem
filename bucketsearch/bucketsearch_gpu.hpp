/****************************************************************************** 
 * 
 *  file:  bucketsearch_gpu.hpp
 * 
 *  Copyright (c) 2016, Grand R. Joldes.
 *  All rights reverved.
 * 
 *  
 *  THE SOFTWARE IS PROVIDED _AS IS_, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *  DEALINGS IN THE SOFTWARE.  
 *  
 *****************************************************************************/ 

#ifndef __BUCKETSEARCH_GPU_HPP__
#define __BUCKETSEARCH_GPU_HPP__

#include "points_gpu.hpp"
#include "bucketsearch.hpp"

namespace BucketSearch {

class buckets_GPU {
public:
	buckets_GPU(buckets *pBuckets_);
	~buckets_GPU();

	buckets::stBuckets *pGetDeviceBucketsPointer(void) {return pDeviceBuckets;};
	buckets *pGetBucketsPointer(void) {return pBuckets;};
private:
	buckets::stBuckets *pDeviceBuckets;
	buckets *pBuckets;
};


class BucketSearch_GPU {
public:
	BucketSearch_GPU(buckets_GPU *pBuckets_, BioDEM::points_GPU *pPoints_);
	~BucketSearch_GPU();

	void vDistributePointsToBucketsGPU(void);
	void vReadBucketedPoints(BucketSearch *pBucketSearch);
	
private:
	BucketSearch::tstBucketedPoints *pstDeviceBucketedPoints;
	BucketSearch::tstBucketedPoints *pstBucketedPoints;
	buckets_GPU *pBuckets;
	BioDEM::points_GPU *pPoints;
};

} // namespace

#endif /* __BUCKETSEARCH_GPU_HPP__ */