/****************************************************************************** 
 * 
 *  file:  bucketsearch_mt.cpp
 * 
 *  Copyright (c) 2016, Grand R. Joldes.
 *  All rights reverved.
 * 
 *  
 *  THE SOFTWARE IS PROVIDED _AS IS_, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *  DEALINGS IN THE SOFTWARE.  
 *  
 *****************************************************************************/ 
#include <boost/thread/thread.hpp>

#include "bucketsearch_mt.hpp"

namespace BucketSearch {

BucketSearch_MT::BucketSearch_MT(buckets *pBuckets_, float *pfPointsCoords, unsigned int uiNumPoints):BucketSearch(pBuckets_, pfPointsCoords, uiNumPoints)
{
	uiNumberOfThreads = boost::thread::hardware_concurrency();
	unsigned int uiNumBuckets = pBuckets_->uiGetNumberOfBuckets();
	
	// allocate memory
	aAtomicInt = new boost::atomic<int>[uiNumBuckets];
	astThreadControl = new tstThreadControl[uiNumberOfThreads];
	// initialize thread controls
	for (unsigned int i = 0; i < uiNumBuckets; i++)
	{
		aAtomicInt[i] = 0;
	}

	unsigned int uiNumPointsPerThread = uiNumPoints/uiNumberOfThreads + 1;
	for (unsigned int i = 0; i < uiNumberOfThreads; i++)
	{
		tstThreadControl *pstThreadControl = &astThreadControl[i];
		pstThreadControl->uiFirstPointIdx = i*uiNumPointsPerThread;
		if (pstThreadControl->uiFirstPointIdx >= uiNumPoints) pstThreadControl->boHandlePoints = false;
		else {
			pstThreadControl->boHandlePoints = true;
			pstThreadControl->uiLastPointIdx = pstThreadControl->uiFirstPointIdx + uiNumPointsPerThread - 1;
			if (pstThreadControl->uiLastPointIdx >= uiNumPoints) pstThreadControl->uiLastPointIdx = uiNumPoints - 1;
		}
	}
}

BucketSearch_MT::~BucketSearch_MT()
{
	delete[] astThreadControl;
	delete[] aAtomicInt;
}

void vBucketPointsThread(BucketSearch_MT *pBucketSearch, BucketSearch_MT::tstThreadControl *pstThreadControl)
{
	unsigned int pointIndex;
	// distribute points in buckets
	if (pstThreadControl->boHandlePoints)
	{
		for (pointIndex = pstThreadControl->uiFirstPointIdx; pointIndex <= pstThreadControl->uiLastPointIdx; pointIndex++)
		{
			float xp = pBucketSearch->pPointsCoord[3*pointIndex];
			float yp = pBucketSearch->pPointsCoord[3*pointIndex+1];
			float zp = pBucketSearch->pPointsCoord[3*pointIndex+2];
			unsigned int idxBucketX = pBucketSearch->pBuckets->uiGetBucketIndexX(xp);
			unsigned int idxBucketY = pBucketSearch->pBuckets->uiGetBucketIndexY(yp);
			unsigned int idxBucketZ = pBucketSearch->pBuckets->uiGetBucketIndexZ(zp);
			unsigned int idxLinear = pBucketSearch->pBuckets->uiGetLinearBucketIndex(idxBucketX, idxBucketY, idxBucketZ);
			// sync
			while (pBucketSearch->aAtomicInt[idxLinear].exchange(1, boost::memory_order_acquire) == 1) {
				/* busy-wait */
			}
			if (pBucketSearch->uiGetNumPointsInBucket(idxLinear) == 0)
			{
				pBucketSearch->vSetNumPointsInBucket(idxLinear, 1);
				pBucketSearch->vSetLastPointInBucket(idxLinear, pointIndex);
				pBucketSearch->aAtomicInt[idxLinear].store(0, boost::memory_order_release);
				pBucketSearch->vSetFirstPointInBucket(idxLinear, pointIndex);
			} else 
			{
				unsigned int uiLastPointIdx = pBucketSearch->uiGetLastPointInBucket(idxLinear);
				pBucketSearch->vSetLastPointInBucket(idxLinear, pointIndex);
				pBucketSearch->aAtomicInt[idxLinear].store(0, boost::memory_order_release);
				pBucketSearch->vSetNextPointInBucket(uiLastPointIdx, pointIndex);
				pBucketSearch->pstBucketedPoints->puiNumPointsPerBucket[idxLinear]++;
			}
		}
	}
}

void BucketSearch_MT::vDistributePointsToBucketsMT(void)
{
	boost::thread_group ThreadGroup;
	unsigned int uiNumBuckets = pBuckets->uiGetNumberOfBuckets();
	for (unsigned int i = 0; i < uiNumBuckets; i++)
	{
		vSetNumPointsInBucket(i, 0);
	}
	for (unsigned int t = 0; t < uiNumberOfThreads; t++)
	{
		tstThreadControl *pstThreadControl = &astThreadControl[t];
		ThreadGroup.create_thread(boost::bind(vBucketPointsThread, this, pstThreadControl));
	}
	ThreadGroup.join_all();
}

}