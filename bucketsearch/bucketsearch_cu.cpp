/****************************************************************************** 
 * 
 *  file:  bucketsearch.cu
 * 
 *  Copyright (c) 2016, Grand R. Joldes.
 *  All rights reverved.
 * 
 *  
 *  THE SOFTWARE IS PROVIDED _AS IS_, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *  DEALINGS IN THE SOFTWARE.  
 *  
 *****************************************************************************/ 
#include <limits.h>
#include <cuda_runtime.h>
#include "helper_cuda.h"

#include "bucketsearch_gpu.hpp"

namespace BucketSearch {

buckets_GPU::buckets_GPU(buckets *pBuckets_):pBuckets(pBuckets_)
{
	checkCudaErrors(cudaMalloc(&pDeviceBuckets, sizeof(buckets::stBuckets)));
	// copy data to device memory
	checkCudaErrors(cudaMemcpy(pDeviceBuckets, pBuckets->pstGetBuckets(), 
			sizeof(buckets::stBuckets), cudaMemcpyHostToDevice));
}

buckets_GPU::~buckets_GPU()
{
	checkCudaErrors(cudaFree(pDeviceBuckets));
}

BucketSearch_GPU::BucketSearch_GPU(buckets_GPU *pBuckets_, BioDEM::points_GPU *pPoints_)
	:pBuckets(pBuckets_), pPoints(pPoints_)
{
	void *pvDevicePtr;
	pstBucketedPoints = new BucketSearch::tstBucketedPoints;
	checkCudaErrors(cudaMalloc(&pstDeviceBucketedPoints, sizeof(BucketSearch::tstBucketedPoints)));

	unsigned int uiNumPoints = pPoints->uiGetNumPoints();
	checkCudaErrors(cudaMalloc(&pvDevicePtr, uiNumPoints*sizeof(unsigned int)));
	pstBucketedPoints->puiNumPointsPerBucket = (unsigned int*)pvDevicePtr;
	checkCudaErrors(cudaMalloc(&pvDevicePtr, pBuckets_->pGetBucketsPointer()->uiGetNumberOfBuckets()*sizeof(unsigned int)));
	pstBucketedPoints->puiStartingIndex = (unsigned int*)pvDevicePtr;
	checkCudaErrors(cudaMalloc(&pvDevicePtr, pBuckets_->pGetBucketsPointer()->uiGetNumberOfBuckets()*sizeof(unsigned int)));
	pstBucketedPoints->puiLastIndex = (unsigned int*)pvDevicePtr;
	checkCudaErrors(cudaMalloc(&pvDevicePtr, uiNumPoints*sizeof(unsigned int)));
	pstBucketedPoints->puiNextIndex = (unsigned int*)pvDevicePtr;
	// copy data to device memory
	checkCudaErrors(cudaMemcpy(pstDeviceBucketedPoints, pstBucketedPoints, sizeof(BucketSearch::tstBucketedPoints), cudaMemcpyHostToDevice));
}

BucketSearch_GPU::~BucketSearch_GPU()
{
	checkCudaErrors(cudaFree(pstBucketedPoints->puiNumPointsPerBucket));
	checkCudaErrors(cudaFree(pstBucketedPoints->puiStartingIndex));
	checkCudaErrors(cudaFree(pstBucketedPoints->puiLastIndex));
	checkCudaErrors(cudaFree(pstBucketedPoints->puiNextIndex));
	checkCudaErrors(cudaFree(pstDeviceBucketedPoints));
	delete pstBucketedPoints;
}

void BucketSearch_GPU::vReadBucketedPoints(BucketSearch *pBucketSearch)
{
	checkCudaErrors(cudaMemcpy(pBucketSearch->puiGetNumPointsPerBucketPointer(), pstBucketedPoints->puiNumPointsPerBucket, 
			pBucketSearch->pGetBucketsPointer()->uiGetNumberOfBuckets()*sizeof(unsigned int), cudaMemcpyDeviceToHost));
	checkCudaErrors(cudaMemcpy(pBucketSearch->puiGetStartingIndexPointer(), pstBucketedPoints->puiStartingIndex, 
			pBucketSearch->pGetBucketsPointer()->uiGetNumberOfBuckets()*sizeof(unsigned int), cudaMemcpyDeviceToHost));
	checkCudaErrors(cudaMemcpy(pBucketSearch->puiGetLastIndexPointer(), pstBucketedPoints->puiLastIndex, 
			pBucketSearch->pGetBucketsPointer()->uiGetNumberOfBuckets()*sizeof(unsigned int), cudaMemcpyDeviceToHost));
	checkCudaErrors(cudaMemcpy(pBucketSearch->puiGetNextIndexPointer(), pstBucketedPoints->puiNextIndex, 
			pBucketSearch->uiGetNumPoints()*sizeof(unsigned int), cudaMemcpyDeviceToHost));
}

__global__ void vInitBuckets_kernel(const buckets::stBuckets *pstDeviceBuckets, 
									const BucketSearch::tstBucketedPoints *pstDeviceBucketedPoints)
{
	int idx = blockIdx.x * blockDim.x + threadIdx.x;
	if (idx < pstDeviceBuckets->uiNumBuckets)
	{
		pstDeviceBucketedPoints->puiNumPointsPerBucket[idx] = 0;
		pstDeviceBucketedPoints->puiLastIndex[idx] = UINT_MAX;
	}
}

#define uiGetBucketIndexX(pstBuckets, xCoord) ((unsigned int)((xCoord-pstBuckets->xMin)/pstBuckets->sizeX))
#define uiGetBucketIndexY(pstBuckets, yCoord) ((unsigned int)((yCoord-pstBuckets->yMin)/pstBuckets->sizeY))
#define uiGetBucketIndexZ(pstBuckets, zCoord) ((unsigned int)((zCoord-pstBuckets->zMin)/pstBuckets->sizeZ))
#define uiGetLinearBucketIndex(pstBuckets, idxX, idxY, idxZ) (idxX + pstBuckets->numX*idxY + pstBuckets->uiStrideZ*idxZ)

__global__ void vDistributePointsToBuckets_kernel(const buckets::stBuckets *pstDeviceBuckets, const BioDEM::points_GPU::tstDevicePoints *pstDevicePoints,
			const BucketSearch::tstBucketedPoints *pstDeviceBucketedPoints)
{

	int idx = blockIdx.x * blockDim.x + threadIdx.x;
	if (idx < pstDevicePoints->uiCount)
	{
		unsigned int idxBucketX = uiGetBucketIndexX(pstDeviceBuckets, pstDevicePoints->afX[idx]);
		unsigned int idxBucketY = uiGetBucketIndexY(pstDeviceBuckets, pstDevicePoints->afY[idx]);
		unsigned int idxBucketZ = uiGetBucketIndexZ(pstDeviceBuckets, pstDevicePoints->afZ[idx]);
		unsigned int idxLinear = uiGetLinearBucketIndex(pstDeviceBuckets, idxBucketX, idxBucketY, idxBucketZ);
		if (atomicCAS((unsigned int *)&(pstDeviceBucketedPoints->puiNumPointsPerBucket[idxLinear]), 0, 1) == 0)
		{
			// first one here
			atomicExch((unsigned int *)&(pstDeviceBucketedPoints->puiLastIndex[idxLinear]), idx);
			pstDeviceBucketedPoints->puiStartingIndex[idxLinear] = idx;
			//printf("%d in %d!\n", idx, idxLinear);
		}
		else
		{
			// wait until first thread handling the bucket done its job
			unsigned int uiLastIndex;
			while (atomicCAS((unsigned int *)&(pstDeviceBucketedPoints->puiLastIndex[idxLinear]), UINT_MAX, UINT_MAX) == UINT_MAX) {};
			uiLastIndex = atomicExch((unsigned int *)&(pstDeviceBucketedPoints->puiLastIndex[idxLinear]), idx);
			pstDeviceBucketedPoints->puiNextIndex[uiLastIndex] = idx;
			atomicAdd((unsigned int *)&(pstDeviceBucketedPoints->puiNumPointsPerBucket[idxLinear]), 1);
			//printf("%d in %d\n", idx, idxLinear);
		}
	}
}

void BucketSearch_GPU::vDistributePointsToBucketsGPU(void) 
{
	// set kernel launch configuration
	cudaEvent_t start, stop;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);

	cudaEventRecord(start);
	unsigned int uiBlockSize = 512;
	unsigned int n = pBuckets->pGetBucketsPointer()->uiGetNumberOfBuckets();
	unsigned int uiNumBlocks = n/uiBlockSize;
	if (n%uiBlockSize) uiNumBlocks++;
	vInitBuckets_kernel<<<uiNumBlocks, uiBlockSize>>>(pBuckets->pGetDeviceBucketsPointer(), pstDeviceBucketedPoints);

	n = pPoints->uiGetNumPoints();
	uiNumBlocks = n/uiBlockSize;
	if (n%uiBlockSize) uiNumBlocks++;
	vDistributePointsToBuckets_kernel<<<uiNumBlocks, uiBlockSize>>>(pBuckets->pGetDeviceBucketsPointer(), pPoints->pstGetDevicePoints(), pstDeviceBucketedPoints);
	cudaEventRecord(stop);
	cudaEventSynchronize(stop);
	float milliseconds = 0;
	cudaEventElapsedTime(&milliseconds, start, stop);
	printf("vDistributePointsToBucketsGPU took %g s.\n", milliseconds/1000);
}

} // namespace

