/****************************************************************************** 
 * 
 *  file:  workspace.hpp
 * 
 *  Copyright (c) 2016, Grand R. Joldes.
 *  All rights reverved.
 * 
 *  
 *  THE SOFTWARE IS PROVIDED _AS IS_, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *  DEALINGS IN THE SOFTWARE.  
 *  
 *****************************************************************************/ 

#ifndef __WORKSPACE_HPP__
#define __WORKSPACE_HPP__

#include <vector>
#include <vtkPolyData.h>
#include <vtkSmartPointer.h>
#include <vtkPointData.h>
#include <vtkIdTypeArray.h>
#include <vtkSelection.h>
#include <vtkSelectionNode.h>
#include <vtkExtractSelection.h>

#include "options.hpp"
#include "discretisation.hpp"
#include "interactions.hpp"
#include "walls.hpp"
#include "GenericLoad.hpp"
#include "AnalysisStep.hpp"
#include "utils.hpp"
#include "catalyst.hpp"


namespace BioDEM {

class CWorkspace 
{
public:
	CWorkspace();
	~CWorkspace();

	bool boConfigure(program_options *pPO);
	unsigned int uiGetNumDimensions(void) {return uiDimensions;};
	CDiscretisation *pGetDiscretisation(void) {return &discretisation;};
	CWalls *pGetWalls(void) {return &walls;};
	CInteractions *pGetInteractions(void) {return &interactions;};
	unsigned int uiGetCurrentStep(void) {return uiCurrentStep;};
	CAnalysisStep *pGetAnalysisStep(unsigned int uiNum) {return CAnalysisStepArray[uiNum];};
	CatalystAdaptor *pGetCatalystAdaptor(void) {return &catalystAdaptor;};
	vtkPolyData *pGetOutputPolyData(void) {return pOutputPolyData;};
	vtkPolyData *pGetOutputWalls(void) {return pOutputWalls;};
	void vUpdateOutputWalls(void) {walls.vUpdateOutputWalls(pOutputWalls);};
	bool boRunAnalysis(void);
	void vOutputResults(int iTimeStep, double time);
	void vMoveWalls(float fRelativeStepTime, float *afWallMotion);
	void vUpdateWallLocation(float *afWallMotion);
	void vResetOutputGlobals(void);
	void vAddOutputVariable(const char *pcName);
	void vSetOutputVariable(const char *pcName, float fValues);
	template<typename T> void vAddOutputPointArray(const char *pcName, void *pvValues);
	void vRemoveOutputLines(void) {pOutputPolyData->GetLines()->Reset();};
	void vAddOutputLine(unsigned int uiPointIdx1, unsigned int uiPointIdx2);
	bool boGetUniqueInteractions(void) {return boUniqueInteractions;};
	bool boGrowthActive(void) {return boGrowthActive_;};
	// functions for managing BC
	void vInitBC(float *pfDisp, unsigned int uiNum);
	void vApplyBC(float *pfDisp, vtkIdType *pIdActivePoints, unsigned int uiNum, float fRelTime);
	void vInitGrowth(float *pfGrowthFactor, unsigned char *pucParticlesType, unsigned int uiNumParticles, unsigned int uiNumSteps);
	bool boPointIsFixed(unsigned int uiPointIdx);
	float fGetRadiusDispersion(void) {return fRadiusDispersion;};
	void vAddMembraneConnections(void);
	void vWriteOutputPoly(std::string fileName);

private:
	unsigned int uiDimensions;
	tenSolverType enSolverType;
	unsigned int uiNumberOfThreads;
	unsigned int uiGPU_id;
	float fRadiusDispersion;
	CDiscretisation discretisation;
	bool boUniqueInteractions;
	std::string sResultsFileName;
	CWalls walls;
	unsigned int uiNumSteps, uiCurrentStep;
	CMyArray<CAnalysisStep> CAnalysisStepArray;
	CMyArray<CGenericLoad> CLoadsArray;
	CMyArray<CGrowth> CGrowthArray;
	CInteractions interactions;
	CatalystAdaptor catalystAdaptor;
	bool boUseParallelOutput;
	bool boOutputAdditionalParticles;
	vtkPolyData *pOutputPolyData;
	vtkPolyData *pOutputPolyDataCopy;
	vtkPolyData *pOutputWalls;
	vtkPolyData *pOutputGlobals;
	vtkPolyData *pOutputGlobalsCopy;
	// data for selecting active particles for output
	vtkSmartPointer<vtkIdTypeArray> arrayIdsActiveParticles;
	vtkSmartPointer<vtkSelectionNode> selectionNode;
	vtkSmartPointer<vtkSelection> selection;
	vtkSmartPointer<vtkExtractSelection> extractSelectedIds;
	// arrays for managing BC
	unsigned char *pucBCMarkDOF;
	float *pfBCAppliedDisplacementValue;
	float *pfBCSavedDisplacements;
	// manage growth
	bool boGrowthActive_;
};

template<typename T> void CWorkspace::vAddOutputPointArray(const char *pcName, void *pvValues)
{
	vtkSmartPointer<T> arrayType = vtkSmartPointer<T>::New();
	arrayType->SetNumberOfValues(1);
	arrayType->SetName(pcName);
	arrayType->SetVoidArray(pvValues, discretisation.uiGetTotalNumberOfParticles(), 1);
	pOutputPolyData->GetPointData()->AddArray(arrayType);
}

} // namespace

#endif /* __WORKSPACE_HPP__ */