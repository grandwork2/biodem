/****************************************************************************** 
 * 
 *  file:  cells.hpp
 * 
 *  Copyright (c) 2016, Grand R. Joldes.
 *  All rights reverved.
 * 
 *  
 *  THE SOFTWARE IS PROVIDED _AS IS_, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *  DEALINGS IN THE SOFTWARE.  
 *  
 *****************************************************************************/ 

#ifndef __CELLS_HPP__
#define __CELLS_HPP__

namespace BioDEM {

class CCells 
{
public:
	typedef enum {
		CCells_enPassive,
		CCells_enGrowing,
		CCelles_enDividing
	} tenCellStatus;

	CCells(unsigned int uiMaxNumCells, unsigned int uiMaxNumParticles);
	~CCells();

	void vAddParticle(unsigned int uiCellIdx, unsigned int uiParticleIdx);

private:
	unsigned int uiMaxNumberOfCells;
	unsigned int uiNumberOfCells;
	tenCellStatus *penCellStatus;
	// particles associated to each cell
	unsigned int *puiNumParticlesPerCell;
	unsigned int *puiIdxOfFirstParticleInCell;
	unsigned int *puiIdxOfNextParticleInCell;
	// cell associated to each particle
	unsigned int *puiCellIdxForParticle;
	// manage variable number of cells
	unsigned int *puiActiveCells;
	// properties of cell parts
	unsigned char *pucCellsMembraneType;
	unsigned char *pucCellsCytoplasmType;
	unsigned char *pucCellsNucleusType;
	unsigned char *pucCellsNucleusMembraneType;
};

} // namespace

#endif /* __CELLS_HPP__ */