/****************************************************************************** 
 * 
 *  file:  discretisation.hpp
 * 
 *  Copyright (c) 2016, Grand R. Joldes.
 *  All rights reverved.
 * 
 *  
 *  THE SOFTWARE IS PROVIDED _AS IS_, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *  DEALINGS IN THE SOFTWARE.  
 *  
 *****************************************************************************/ 

#ifndef __DISCRETISATION_HPP__
#define __DISCRETISATION_HPP__

#include <vtkPolyData.h>
#include <vtkSmartPointer.h>

#include "options.hpp"

namespace BioDEM {

class CDiscretisation 
{
public:
	CDiscretisation();
	~CDiscretisation();

	bool boConfigure(program_options *pPO);
	unsigned int uiGetNumberOfParticleTypes(void) {return ucMaxParticleType+1;};
	unsigned int uiGetNumberOfActiveParticles(void) {return uiNumParticles;};
	unsigned int uiGetTotalNumberOfParticles(void) {return uiNumParticles+uiNumAdditionalParticles;};
	unsigned int uiGetNumberOfAdditionalParticles(void) {return uiNumAdditionalParticles;};
	float fGetMinInputParticleRadius(void) {return fMinParticleRadius;};
	float fGetMaxInputParticleRadius(void) {return fMaxParticleRadius;};
	vtkIdType idGetActiveParticleIndex(unsigned int uiIdx) {return pIdActiveParticlesIndexes[uiIdx];};
	vtkIdType *pIdGetActiveParticleIndexes(void) {return pIdActiveParticlesIndexes;};
	float *pfGetRadius(void) {return pfRadius;};
	float *pfGetDensity(void) {return pfDensity;};
	unsigned char *pucGetType(void) {return pucType;};
	bool boIsMembrane(unsigned int uiIdx) {return pucMembraneIdx[uiIdx]>0;};
	bool boHasMembrane(void) {return boHasMembraneParticles;};
	bool boCloseMembrane(void) {return boAddParticlesToCloseMembrane;};
	bool boGrowMembrane(void) {return boGrowMembrane_;};
	float *pfGetCoordinates(void) {return pfCoordinates;};
	float fGetXCoord(unsigned int idx) {return pfCoordinates[3*idx];};
	float fGetYCoord(unsigned int idx) {return pfCoordinates[3*idx+1];};
	float fGetZCoord(unsigned int idx) {return pfCoordinates[3*idx+2];};

	float *pfGetBoundingBox(void) {return afBoundingBox;};
	vtkSmartPointer<vtkPolyData> pGetInputPolyData(void) {return inputPolyData;};
	// particle removal/insertion. Returns the index in the data arrays corresponding to the particle.
	unsigned int uiDeactivateParticle(unsigned int uiIdx);
	unsigned int uiActivateParticle(void);
	unsigned int uiMarkMembraneParticles(const char *pcSetId, int idValue);
	unsigned int uiMembraneVolumeParticles(int idValue, unsigned char ucMembraneType);

	bool boAddUniqueMembraneNeighbour(unsigned int uiParticleId, unsigned int uiNeighbourId);
	
	// particle manipulation
	void vChangeParticleTypeInSphere(float *pfCenter, float fRadius, unsigned char ucOldType, unsigned char ucType);
	
public:
	unsigned int *puiMembraneNeigbours;
	unsigned char *pucMembraneNumNeighbours;
	unsigned char ucMaxMembraneNeighbours;
	unsigned char *pucMembraneIdx;
	unsigned char ucMaxMembraneIdx;

private:
	bool boCheckBoundingBox(float *pfBoundingBox, float fMaxRadius);
	vtkSmartPointer<vtkPolyData> inputPolyData;
	unsigned int uiNumParticles;
	unsigned int uiNumAdditionalParticles;
	// particles properties
	float *pfCoordinates;
	float *pfRadius; // radius
	unsigned char *pucType; 
	float *pfDensity;
	// membrane handling
	bool boHasMembraneParticles;
	bool boAddParticlesToCloseMembrane;
	bool boGrowMembrane_;
	unsigned char ucMaxParticleType;
	float fMaxParticleRadius;
	float fMinParticleRadius;
	float afBoundingBox[6];
	vtkIdType *pIdActiveParticlesIndexes;
	unsigned int uiDimensions;
};

} // namespace

#endif /* __DISCRETISATION_HPP__ */