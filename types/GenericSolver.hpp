/****************************************************************************** 
 * 
 *  file:  GenericSolver.hpp
 * 
 *  Copyright (c) 2016, Grand R. Joldes.
 *  All rights reverved.
 * 
 *  
 *  THE SOFTWARE IS PROVIDED _AS IS_, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *  DEALINGS IN THE SOFTWARE.  
 *  
 *****************************************************************************/ 

#ifndef __GENERICSOLVER_HPP__
#define __GENERICSOLVER_HPP__

#include <string>

#include "workspace.hpp"
#include "discretisation.hpp"

namespace BioDEM {

class CGenericSolver 
{
public:
	typedef enum
	{
		SOLVER_enInit = 0,
		SOLVER_enExplicit	
	} tenSolverType;

	typedef struct
	{
		float *pfOldDisplacements;
		float *pfCurrentDisplacements;
		float *pfNewDisplacements;
		float *pfOldVelocity;
		float *pfNewVelocity;
		float *pfReactionForces;
		float *pfParticlesRadius;
		float *pfParticlesDensity;
		float *pfParticlesCoordinates;
		float *pfParticlesInitialCoordinates;
		unsigned char *pucParticlesType;
		unsigned char *puiActiveParticles;
		unsigned char *pucMarkedForDeletion;
		float *pfMembraneParticleInterractionRadius;
		float *pfGrowthFactor;
		float *pfInterractionGrowthFactor;
		float *pfParticleIntegrity;
	} tstGlobalVars;

	CGenericSolver();
	~CGenericSolver() {};
	static CGenericSolver* pCCreateSolver(std::string config, CWorkspace *pW);
	virtual bool boPerformAnalysis(void) = 0;
	virtual bool boConfigure(std::string config) = 0;
	virtual bool boSetParameters(std::string config, tenSolverType enSolverType) = 0;
	bool boSetWallMotion(std::string config);
	tstGlobalVars *pstGetGlobalVars(void) {return pstGlobalVars;};
	void vSetGlobalVars(tstGlobalVars *pGlobals) {pstGlobalVars = pGlobals;};
	void vSetWorkspace(CWorkspace *pW);
	void vSetStartTime(float fT) {fStartTime = fT;};
	float fGetEndTime(void) {return fEndTime;};
	void vSetStartIteration(unsigned int it) {uiStartIteration = it;};
	unsigned int uiGetEndIteration(void) {return uiEndIteration;};
	tenSolverType enGetSolverType(void) {return enSolverType;};
	float fGetDampingParam(void) {return fDampingParam;};
	unsigned int uiDeactivateParticle(unsigned int uiIdx);
	void vMarkForDeletion(unsigned int uiIdx);
	unsigned int uiDeleteMarkedParticles(void);
	unsigned int uiActivateParticle(void);
	virtual void vCopyParticle(unsigned int fromIdx, unsigned int toIdx);
protected:
	tstGlobalVars *pstGlobalVars;
	CDiscretisation *pCDiscretisation;
	CWorkspace *pCWorkspace;
	unsigned int uiDimensions;
	unsigned int uiTotalNumParticles;
	unsigned int uiTotalNumberDOF;
	float fStartTime, fEndTime;
	unsigned int uiStartIteration, uiEndIteration;
	tenSolverType enSolverType;
	float afWallMotion[6];
	float afWallForces[6];
	float fDampingParam;
	bool boMarkedForDeletion;
};

class CInitSolver: public CGenericSolver
{
public:
	CInitSolver(void);
	~CInitSolver(void);
	bool boConfigure(std::string config) {return true;};
	bool boSetParameters(std::string config, tenSolverType enSolverType) {return true;};
	bool boPerformAnalysis(void);
	void vCloseMembrane(unsigned int uiParticleIdx, unsigned int uiNeighbourIndex);
	void vCleanMembrane(unsigned int uiParticleIdx, unsigned int uiNeighbourIndex);
};

} // namespace

#endif /* __GENERICSOLVER_HPP__ */