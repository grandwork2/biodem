/****************************************************************************** 
 * 
 *  file:  GenericLoad.cpp
 * 
 *  Copyright (c) 2016, Grand R. Joldes.
 *  All rights reverved.
 * 
 *  
 *  THE SOFTWARE IS PROVIDED _AS IS_, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *  DEALINGS IN THE SOFTWARE.  
 *  
 *****************************************************************************/ 
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <algorithm>
#include <iostream>

#include <vtkPointData.h>

#include "GenericLoad.hpp"

namespace BioDEM {

bool CGenericLoad::boConfigure(std::string config, vtkSmartPointer<vtkPolyData> inputPolyData, unsigned int uiNumSteps)
{
	aboActiveInStep.resize(uiNumSteps);
	config.erase(std::remove_if(config.begin(), config.end(), ::isspace), config.end());
	int iStepNo;
	char *token = strtok((char *)config.c_str(), ",");
	int n = sscanf(token, "%d", &iStepNo);
	if (n!=1) 
	{
		std::cout << "Error: Invalid load configuration: " << config << "! Step number not found. "<< std::endl; 
		return false;
	}
	if ((iStepNo > 0) && ((unsigned int)iStepNo <= uiNumSteps)) 
	{
		for (int i = 0; i < iStepNo-1; i++) aboActiveInStep[i] = false;
		for (int i = iStepNo-1; i < (int)uiNumSteps; i++) aboActiveInStep[i] = true;
	}
	else
	{
		std::cout << "Error: Invalid step number in load configuration: " << config << std::endl; 
		return false;
	}
	// get name
	token = strtok(NULL, ",");
	if (token != NULL)
	{
		sName = token;
		std::cout << "\t\tName: " << sName << std::endl;	
		std::cout << "\t\tStep: " << iStepNo << std::endl;	
	}
	else 
	{
		std::cout << "Error: Invalid load configuration: " << config << "! Name not found. "<< std::endl; 
		return false;
	}
	// get type
	token = strtok(NULL, ",");
	if (token != NULL)
	{
		if (_stricmp(token, "fix") == 0)
		{
			enType = LOAD_enEncastred;
		}
		else if (_stricmp(token, "move") == 0)
		{
			enType = LOAD_enDisplaced;
		}
		else
		{
			std::cout << "Error: Invalid load configuration: " << config << "! Unknown load type. "<< std::endl; 
			return false;
		}
		std::cout << "\t\tType: " << token << std::endl;
	}
	else 
	{
		std::cout << "Error: Invalid load configuration: " << config << "! Load type not found. "<< std::endl; 
		return false;
	}
	// get setId
	const char *pcSetId = strtok(NULL, ",");
	if (pcSetId != NULL)
	{
		std::cout << "\t\tSet Id: " << pcSetId << std::endl;
	}
	else
	{
		std::cout << "Error: Invalid load configuration: " << config << "! Set Id not found. "<< std::endl; 
		return false;
	}
	// get id value
	token = strtok(NULL, ",");
	int iIdValue;
	if (token != NULL)
	{
		n = sscanf(token, "%d", &iIdValue);
		if (n!=1) 
		{
			std::cout << "Error: Invalid load configuration: " << config << "! Id value not found. "<< std::endl; 
			return false;
		}
		else 
		{
			std::cout << "\t\tId value: " << iIdValue << std::endl;
		}
	}
	else 
	{
		std::cout << "Error: Invalid load configuration: " << config << "! Id value not found. "<< std::endl; 
		return false;
	}
	// get directions
	token = strtok(NULL, "");
	int ix, iy, iz;
	if (token != NULL)
	{
		if (enType == LOAD_enEncastred)
		{
			n = sscanf(token, "%d,%d,%d", &ix, &iy, &iz);
			if (n!=3) 
			{
				std::cout << "Error: Invalid load configuration: " << config << "! Directions not found. "<< std::endl; 
				return false;
			}
			else
			{
				boX = ix > 0; boY = iy > 0; boZ = iz > 0;
				std::cout << "\t\tDirections: " << boX << "," << boY << "," << boZ << std::endl;
			}
		}
		else
		{
			n = sscanf(token, "%d,%d,%d,%f,%f,%f", &ix, &iy, &iz, &fX, &fY, &fZ);
			if (n!=6) 
			{
				std::cout << "Error: Invalid load configuration: " << config << "! Directions and values not found. "<< std::endl; 
				return false;
			}
			else
			{
				boX = ix > 0; boY = iy > 0; boZ = iz > 0;
				std::cout << "\t\tDirections: " << boX << "," << boY << "," << boZ << std::endl;
				std::cout << "\t\tValues: " << fX << "," << fY << "," << fZ << std::endl;
			}
		}
	}
	else 
	{
		std::cout << "Error: Invalid load configuration: " << config << "! Directions not found. "<< std::endl; 
		return false;
	}

	// get the indexes of the loaded nodes
	vtkDataArray *pArray = inputPolyData->GetPointData()->GetScalars(pcSetId);
	for (unsigned int i = 0; i < inputPolyData->GetNumberOfPoints(); i++)
	{
		if (*pArray->GetTuple(i) == (double)iIdValue)
		{
			vectorLoadedIndexes.push_back(i);
		}
	}
	std::cout << "\t\tNumber of loaded points: " << vectorLoadedIndexes.size() << std::endl; 
	std::cout << std::endl;
	return true;
}

void CGenericLoad::vDisable(unsigned int uiStepNo)
{
	unsigned int uiNumSteps = (unsigned int)aboActiveInStep.size();
	for (unsigned int i = uiStepNo; i < uiNumSteps; i++)
	{
		aboActiveInStep[i] = false;
	}
}

bool CGrowth::boConfigure(std::string config)
{
	int n = sscanf(config.c_str(), "%d,%d,%f", &uiStepNumber, &uiParticleType, &fGrowthFactor);
	if (n != 3)
	{
		std::cout << "Warning: Invalid growth configuration: " << config << "! Configuration ignored. "<< std::endl; 
		return false;
	}
	if (fGrowthFactor <= 0)
	{
		std::cout << "Warning: Growth factor in " << config << " must be > 0! Configuration ignored. "<< std::endl; 
		return false;
	}
	std::cout << "\t\tStep number: " << uiStepNumber << std::endl;
	std::cout << "\t\tParticle type: " << uiParticleType << std::endl;
	std::cout << "\t\tGrowth factor: " << fGrowthFactor << std::endl;
	return true;
}

}