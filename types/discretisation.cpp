/****************************************************************************** 
 * 
 *  file:  discretisation.cpp
 * 
 *  Copyright (c) 2016, Grand R. Joldes.
 *  All rights reverved.
 * 
 *  
 *  THE SOFTWARE IS PROVIDED _AS IS_, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *  DEALINGS IN THE SOFTWARE.  
 *  
 *****************************************************************************/ 

// VTK includes
#include <vtkSmartPointer.h>
#include <vtkPoints.h>
#include <vtkFloatArray.h>
#include <vtkPointData.h>
#include <vtkPolyData.h>
#include <vtkGenericDataObjectReader.h>
#include <vtkSetGet.h>
#include <vtkIdTypeArray.h>
#include <vtkMergePoints.h>
#include <vtkDelaunay2D.h>
#include <vtkDelaunay3D.h>
#include <vtkFeatureEdges.h>

#include <stdio.h>
#include <string>

#include "vect.hpp"
#include "discretisation.hpp"

namespace BioDEM {

CDiscretisation::CDiscretisation():uiNumParticles(0), uiNumAdditionalParticles(0), pfCoordinates(NULL),
	pfRadius(NULL), pucType(NULL), pfDensity(NULL), pucMembraneIdx(NULL), ucMaxParticleType(0), 
	fMaxParticleRadius(0), fMinParticleRadius(0), boHasMembraneParticles(0), boAddParticlesToCloseMembrane(0), 
	ucMaxMembraneNeighbours(0), puiMembraneNeigbours(NULL), pucMembraneNumNeighbours(NULL), ucMaxMembraneIdx(0), boGrowMembrane_(0)
{
}

CDiscretisation::~CDiscretisation() 
{
	if (pfCoordinates != NULL) delete[] pfCoordinates;
	if (pfRadius != NULL) delete[] pfRadius;
	if (pucType != NULL) delete[] pucType;
	if (pfDensity != NULL) delete[] pfDensity;
	if (pucMembraneIdx != NULL) delete[] pucMembraneIdx;
	if (pIdActiveParticlesIndexes != NULL) delete[] pIdActiveParticlesIndexes;
	if (puiMembraneNeigbours != NULL) delete[] puiMembraneNeigbours;
	if (pucMembraneNumNeighbours != NULL) delete[] pucMembraneNumNeighbours;
	inputPolyData->Delete();
};

bool CDiscretisation::boConfigure(program_options *pPO)
{
	std::string inputFile = pPO->GetInputFile();
	std::cout << "\tInput file: " << inputFile << std::endl;

	uiDimensions = pPO->uiGetNumDimensions();

	// read input file
	vtkSmartPointer<vtkGenericDataObjectReader> reader = vtkSmartPointer<vtkGenericDataObjectReader>::New();
	reader->SetFileName(inputFile.c_str());
	reader->ReadAllScalarsOn();
	reader->Update();

	inputPolyData = inputPolyData->New();
	if(reader->IsFilePolyData()) inputPolyData->DeepCopy(reader->GetPolyDataOutput());
	else 
	{
		std::cout << "Error: Input file " << inputFile << " does not contain PolyData!" << std::endl;
		return false;
	}
	uiNumParticles = inputPolyData->GetNumberOfPoints();
	uiNumAdditionalParticles = pPO->uiGetNumAdditionalParticles();
	
	if (uiNumParticles == 0)
	{
		std::cout << "Error: No points found in input file " << inputFile << "!" << std::endl;
		return false;
	}
	else
	{
		std::cout << "\tNumber of particles: " << uiNumParticles << std::endl;
		pfCoordinates = new float[3*(uiNumParticles+uiNumAdditionalParticles)];
		pfRadius = new float[uiNumParticles+uiNumAdditionalParticles]; 
		pucType = new unsigned char[uiNumParticles+uiNumAdditionalParticles]; 
		pfDensity = new float[uiNumParticles+uiNumAdditionalParticles];
		pucMembraneIdx = new unsigned char[uiNumParticles+uiNumAdditionalParticles];
		
		for (unsigned int i = 0; i < uiNumParticles; i++)
		{
			double *pPoint = inputPolyData->GetPoints()->GetPoint(i);
			float *pfCoord = pfCoordinates + i*3;
			pfCoord[0] = (float)pPoint[0];
			pfCoord[1] = (float)pPoint[1];
			pfCoord[2] = (float)pPoint[2];
			pucMembraneIdx[i] = 0;
		}
		for (unsigned int i = uiNumParticles; i < uiNumParticles+uiNumAdditionalParticles; i++)
		{
			float *pfCoord = pfCoordinates + i*3;
			pfCoord[0] = 0.0f;
			pfCoord[1] = 0.0f;
			pfCoord[2] = 0.0f;
			pucMembraneIdx[i] = 0;
		}
	}

	// read particle radius
	float fRadiusRange[2];
	inputPolyData->GetPointData()->SetActiveScalars(pPO->GetRadiusId().c_str());
	vtkFloatArray *pafRadius = vtkFloatArray::SafeDownCast(inputPolyData->GetPointData()->GetScalars());
	if (pafRadius)
	{
		// read range
		pafRadius->ExportToVoidPointer(pfRadius);
		pafRadius->GetValueRange(fRadiusRange);
		fMaxParticleRadius = fRadiusRange[1];
		fMinParticleRadius = fRadiusRange[0];
		std::cout << "\tMinimum particle radius: " << fMinParticleRadius << std::endl;
		std::cout << "\tMaximum particle radius: " << fMaxParticleRadius << std::endl;
		for (unsigned int i = uiNumParticles; i < uiNumParticles+uiNumAdditionalParticles; i++)
		{
			pfRadius[i] = 0.0f;
		}
	}
	else
	{
		std::cout << "Error: Radius not found in input file " << inputFile << "!" << std::endl;
		return false;
	}

	if (!pPO->boGetBoundingBox(afBoundingBox))
	{
		// setting bounding box from points
		double adBoundingBox[6];
		inputPolyData->GetBounds(adBoundingBox);
		for (unsigned int i = 0; i < 6; i++) 
		{
			if (i % 2)	afBoundingBox[i] = adBoundingBox[i] + fMaxParticleRadius;
			else afBoundingBox[i] = adBoundingBox[i] - fMaxParticleRadius;
		}
	}
	// check bounding box
	if (!boCheckBoundingBox(afBoundingBox, fMaxParticleRadius)) return false;
	std::cout << "\tBounding box: " << std::endl;
	std::cout << "\t\tx: " << afBoundingBox[0] << " ... " << afBoundingBox[1] << std::endl;
	std::cout << "\t\ty: " << afBoundingBox[2] << " ... " << afBoundingBox[3] << std::endl;
	std::cout << "\t\tz: " << afBoundingBox[4] << " ... " << afBoundingBox[5] << std::endl;

	// read density 
	inputPolyData->GetPointData()->SetActiveScalars(pPO->GetDensityId().c_str());
	vtkFloatArray *pafParticleDensity = vtkFloatArray::SafeDownCast(inputPolyData->GetPointData()->GetScalars());
	if (!pafParticleDensity)
	{
		std::cout << "Warning: Density not found in input file " << inputFile << "! Will use density = 1." << std::endl;
		VECT_vInit(pfDensity, uiNumParticles+uiNumAdditionalParticles, 1.0f);
	}
	else 
	{
		pafParticleDensity->ExportToVoidPointer(pfDensity);
		for (unsigned int i = uiNumParticles; i < uiNumParticles+uiNumAdditionalParticles; i++)
		{
			pfDensity[i] = 1.0f;
		}
	}

	// read point type
	inputPolyData->GetPointData()->SetActiveScalars(pPO->GetTypeId().c_str());
	vtkDataArray *pafParticleType = inputPolyData->GetPointData()->GetScalars();
	double afTypeRange[2];
	if (pafParticleType)
	{
		// set particle type
		for (unsigned int i = 0; i < uiNumParticles; i++)
		{
			int t = (int)*(pafParticleType->GetTuple(i));
			if ((t < 0) || (t > 255))
			{
				std::cout << "Error: Particle type must be between 0 and 255!" << std::endl;
				return false;
			}
			pucType[i] = (unsigned char)t;
		}
		pafParticleType->GetRange(afTypeRange);
		std::cout << "\tParticle type range: " << afTypeRange[0] << " ... " << afTypeRange[1] << std::endl;
		ucMaxParticleType = (unsigned char)afTypeRange[1];
		for (unsigned int i = uiNumParticles; i < uiNumParticles+uiNumAdditionalParticles; i++)
		{
			pucType[i] = 0;
		}
	}
	else
	{
		std::cout << "Error: Particle type definition not found in input file " << inputFile << "!" << std::endl;
		return false;
	}

	pIdActiveParticlesIndexes = new vtkIdType[uiNumParticles+uiNumAdditionalParticles];
	for (unsigned int i = 0; i < uiNumParticles+uiNumAdditionalParticles; i++)
	{
		pIdActiveParticlesIndexes[i] = i;
	} 

	// Handle membranes
	// mark membrane particles
	boHasMembraneParticles = uiMarkMembraneParticles(pPO->GetMembraneId().c_str(), -1) > 0;
	
	if (pPO->uiGetNumDimensions() == 2) ucMaxMembraneNeighbours = 2;
	else ucMaxMembraneNeighbours = 6;

	pucMembraneNumNeighbours = new unsigned char[uiNumParticles+uiNumAdditionalParticles];
	VECT_vInit(pucMembraneNumNeighbours, uiNumParticles+uiNumAdditionalParticles, (unsigned char)0);

	puiMembraneNeigbours = new unsigned int[ucMaxMembraneNeighbours*(uiNumParticles+uiNumAdditionalParticles)];
	// read membrane neighbours from input file
	vtkIdType numCells = inputPolyData->GetNumberOfCells();
	for (vtkIdType cellId = 0; cellId < numCells; cellId++)
	{
		vtkCell *pCell = inputPolyData->GetCell(cellId);
		if (pCell->GetCellType() == VTK_LINE)
		{
			vtkIdType pointId1 = pCell->GetPointId(0);
			vtkIdType pointId2 = pCell->GetPointId(1);
			if (!boAddUniqueMembraneNeighbour(pointId1, pointId2))
			{
				std::cout << "Error: Particle " << pointId1 << " has more membrane neighbours than " << ucMaxMembraneNeighbours << "!" << std::endl;
				return false;
			}
			if (!boAddUniqueMembraneNeighbour(pointId2, pointId1))
			{
				std::cout << "Error: Particle " << pointId2 << " has more membrane neighbours than " << ucMaxMembraneNeighbours << "!" << std::endl;
				return false;
			}
		}
	}
	
	boAddParticlesToCloseMembrane = pPO->boCloseMembrane();
	boGrowMembrane_ = pPO->boGrowMembrane();
	return true;
}

bool CDiscretisation::boAddUniqueMembraneNeighbour(unsigned int uiParticleIdx, unsigned int uiNeighbourIdx)
{
	// check to see if neighbour not already present
	unsigned char ucNumNeighbours = pucMembraneNumNeighbours[uiParticleIdx];
	for (unsigned char i = 0; i < ucNumNeighbours; i++)
	{
		if (puiMembraneNeigbours[uiParticleIdx*ucMaxMembraneNeighbours + i] == uiNeighbourIdx) return true;
	}
	// add neighbour
	if (ucNumNeighbours < ucMaxMembraneNeighbours)
	{
		// add neighbour
		puiMembraneNeigbours[uiParticleIdx*ucMaxMembraneNeighbours+ucNumNeighbours] = uiNeighbourIdx;
		pucMembraneNumNeighbours[uiParticleIdx]++;
		return true;
	}
	return false;
}

unsigned int CDiscretisation::uiMarkMembraneParticles(const char *pcSetId, int idValue)
{
	inputPolyData->GetPointData()->SetActiveScalars(pcSetId);
	vtkDataArray *pafMembraneScalars = inputPolyData->GetPointData()->GetScalars();
	unsigned int uiNumMembraneParicles = 0;
	if (pafMembraneScalars)
	{
		if (idValue < 0)
		{
			// mark membrane particles
			for (unsigned int i = 0; i < uiNumParticles; i++)
			{
				int t = (int)*(pafMembraneScalars->GetTuple(i));
				if (t > 0)
				{
					// read membrane indexes from input array
					pucMembraneIdx[i] = (unsigned char)t;
					uiNumMembraneParicles++;
					pfRadius[i] = fMinParticleRadius;
					if (t > ucMaxMembraneIdx) ucMaxMembraneIdx = t;
				}
			}
		}
		else 
		{
			ucMaxMembraneIdx++;
			for (unsigned int i = 0; i < uiNumParticles; i++)
			{
				int t = (int)*(pafMembraneScalars->GetTuple(i));
				if (t == idValue)
				{
					// read membrane indexes from input array
					pucMembraneIdx[i] = ucMaxMembraneIdx;
					uiNumMembraneParicles++;
					pfRadius[i] = fMinParticleRadius;
				}
			}
			if (uiNumMembraneParicles == 0) ucMaxMembraneIdx--;
		}
	}
	else
	{
		std::cout << "Warning: Membrane type definition not found in input file! Configuration will be ignored." << std::endl;
	}
	if (uiNumMembraneParicles > 0) boHasMembraneParticles = true;
	return uiNumMembraneParicles;
}

unsigned int CDiscretisation::uiMembraneVolumeParticles(int idValue, unsigned char ucMembraneType)
{
	unsigned int uiNumMembraneParicles = 0;
	// find volume particles
	vtkSmartPointer<vtkIdTypeArray> originalPointId = vtkSmartPointer<vtkIdTypeArray>::New();
	originalPointId->SetNumberOfComponents(1);
	vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
	for (unsigned int i = 0; i < uiNumParticles; i++)
	{
		if (pucType[i] == idValue)
		{
			// create an array with the point indexes of particles in this volume
			originalPointId->InsertNextValue(i);
			points->InsertNextPoint(pfCoordinates+3*i);
		}
	}
	if (points->GetNumberOfPoints() == 0)
	{
		std::cout << "Warning: No particle meeting the criteria found in input file! Configuration will be ignored." << std::endl;
		return 0;
	}
	// mesh volume particles
	vtkSmartPointer<vtkPolyData> polyData = vtkSmartPointer<vtkPolyData>::New();
	polyData->SetPoints(points);
	if (uiDimensions == 2)
	{
		vtkSmartPointer<vtkDelaunay2D> delaunay2D = vtkSmartPointer<vtkDelaunay2D>::New();
		delaunay2D->SetAlpha(2.1*fMaxParticleRadius);
		delaunay2D->SetInputData(polyData);
		delaunay2D->Update();
		// find edges
		vtkSmartPointer<vtkFeatureEdges> featureEdges = vtkSmartPointer<vtkFeatureEdges>::New();
		featureEdges->SetInputConnection(delaunay2D->GetOutputPort());
		featureEdges->BoundaryEdgesOn();
		featureEdges->FeatureEdgesOff();
		featureEdges->ManifoldEdgesOff();
		featureEdges->NonManifoldEdgesOff();
		featureEdges->Update();
		// identify boundary particles and their neighbours
		vtkPolyData *pEdges = featureEdges->GetOutput();
		vtkIdType iNumParticles = pEdges->GetNumberOfPoints();
		vtkIdType uNumEdges = pEdges->GetNumberOfCells();
		// find point ids in the original mesh
		vtkSmartPointer<vtkIdTypeArray> aPointIds = vtkSmartPointer<vtkIdTypeArray>::New();
		aPointIds->SetNumberOfComponents(1);
		vtkSmartPointer<vtkMergePoints> pPointFinder = vtkSmartPointer<vtkMergePoints>::New();
		pPointFinder->SetDataSet(polyData);
		pPointFinder->BuildLocator();
		ucMaxMembraneIdx++;
		for (vtkIdType i = 0; i < iNumParticles; i++)
		{
			vtkIdType pointIdx = pPointFinder->FindClosestPoint(pEdges->GetPoint(i));
			aPointIds->InsertNextValue(pointIdx);
			vtkIdType originalPointIdx = originalPointId->GetValue(pointIdx);
			// mark membrane points
			pucMembraneIdx[originalPointIdx] = ucMaxMembraneIdx;
			uiNumMembraneParicles++;
			pfRadius[originalPointIdx] = fMinParticleRadius;
			pucType[originalPointIdx] = ucMembraneType;
		}
		if (uiNumMembraneParicles == 0) ucMaxMembraneIdx--;
		// set neighbours
		for (vtkIdType i = 0; i < uNumEdges; i++)
		{
			vtkIdType edgePointIdx1 = pEdges->GetCell(i)->GetPointId(0);
			vtkIdType edgePointIdx2 = pEdges->GetCell(i)->GetPointId(1);
			vtkIdType pointIdx1 = aPointIds->GetValue(edgePointIdx1);
			vtkIdType pointIdx2 = aPointIds->GetValue(edgePointIdx2);
			vtkIdType originalPointIdx1 = originalPointId->GetValue(pointIdx1);
			vtkIdType originalPointIdx2 = originalPointId->GetValue(pointIdx2);
			boAddUniqueMembraneNeighbour(originalPointIdx1, originalPointIdx2);
			boAddUniqueMembraneNeighbour(originalPointIdx2, originalPointIdx1);
		}
	}
	else
	{
		vtkSmartPointer<vtkDelaunay3D> delaunay3D = vtkSmartPointer<vtkDelaunay3D>::New();
		delaunay3D->SetAlpha(2.1*fMaxParticleRadius);
		delaunay3D->SetAlphaTris(0);
		delaunay3D->SetAlphaLines(0);
		delaunay3D->SetAlphaVerts(0);
		delaunay3D->SetInputData(polyData);
		delaunay3D->Update();
		std::cout << "Error: 3D membrane definition not yet implemented." << std::endl;
		exit(1);
	}	
	if (uiNumMembraneParicles > 0) 
	{
		boHasMembraneParticles = true;
		if (ucMaxParticleType < ucMembraneType) ucMaxParticleType = ucMembraneType;
	}
	return uiNumMembraneParicles;
}

bool CDiscretisation::boCheckBoundingBox(float *pfBoundingBox, float fMaxRadius)
{
	if(pfBoundingBox[0] > pfBoundingBox[1])
	{
		std::cout << "Error: BBox[0] must be smaller than BBox[1]!" << std::endl;
		return false;
	}
	if(pfBoundingBox[2] > pfBoundingBox[3])
	{
		std::cout << "Error: BBox[2] must be smaller than BBox[3]!" << std::endl;
		return false;
	}
	if(pfBoundingBox[4] > pfBoundingBox[5])
	{
		std::cout << "Error: BBox[4] must be smaller than BBox[5]!" << std::endl;
		return false;
	}
	float fLimitDelta = fMaxRadius/10;
	if ((pfBoundingBox[1] - pfBoundingBox[0]) < fLimitDelta)
	{
		pfBoundingBox[1]+=fMaxRadius;
		pfBoundingBox[0]-=fMaxRadius;
	}
	if ((pfBoundingBox[3] - pfBoundingBox[2]) < fLimitDelta)
	{
		pfBoundingBox[3]+=fMaxRadius;
		pfBoundingBox[2]-=fMaxRadius;
	}
	if ((pfBoundingBox[5] - pfBoundingBox[4]) < fLimitDelta)
	{
		pfBoundingBox[5]+=fMaxRadius;
		pfBoundingBox[4]-=fMaxRadius;
	}
	return true;
};

unsigned int CDiscretisation::uiDeactivateParticle(unsigned int uiIdx)
{
	uiNumParticles--;
	uiNumAdditionalParticles++;
	unsigned int idx = pIdActiveParticlesIndexes[uiNumParticles];
	pIdActiveParticlesIndexes[uiNumParticles] = pIdActiveParticlesIndexes[uiIdx];
	pIdActiveParticlesIndexes[uiIdx] = idx;
	return pIdActiveParticlesIndexes[uiNumParticles];
}

unsigned int CDiscretisation::uiActivateParticle(void)
{
	assert(uiNumAdditionalParticles > 0);
	unsigned int idx = pIdActiveParticlesIndexes[uiNumParticles];
	uiNumParticles++;
	uiNumAdditionalParticles--;
	return idx;
}

void CDiscretisation::vChangeParticleTypeInSphere(float *pfCenter, float fRadius, unsigned char ucOldType, unsigned char ucType)
{
	for (unsigned int i = 0; i < uiNumParticles; i++)
	{
		unsigned int idx = pIdActiveParticlesIndexes[i];
		float *pfCoord = pfCoordinates+3*idx;
		if (VECT_ComputeDistance(pfCoord, pfCenter, 3) <= fRadius)
		{
			if (pucType[idx] == ucOldType) pucType[idx] = ucType;
		}
	}
	if (ucMaxParticleType < ucType) ucMaxParticleType = ucType;
}

} // namespace

