/****************************************************************************** 
 * 
 *  file:  AnalysisStep.cpp
 * 
 *  Copyright (c) 2016, Grand R. Joldes.
 *  All rights reverved.
 * 
 *  
 *  THE SOFTWARE IS PROVIDED _AS IS_, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *  DEALINGS IN THE SOFTWARE.  
 *  
 *****************************************************************************/ 

#include <stdio.h>
#include <iostream>

#include "AnalysisStep.hpp"
#include "Workspace.hpp"
#include "GenericSolver.hpp"

namespace BioDEM {

bool CAnalysisStep::boConfigure(std::string config)
{
	config.erase(std::remove_if(config.begin(), config.end(), ::isspace), config.end());
	std::size_t pos1 = config.find(",");
	if (pos1!=std::string::npos)
	{
		sName = config.substr(0, pos1);
		if (config != "Init,Init,")	std::cout << "\t\tName: " << sName << std::endl;
	}
	else 
	{
		std::cout << "Error: Step name not found!" << std::endl;
		return false;
	}
	pCSolver = CGenericSolver::pCCreateSolver(config.substr(pos1+1, std::string::npos), pCWorkspace);
	if (pCSolver == NULL) return false;
	return true;
};

} // namespace

