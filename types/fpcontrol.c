#include "fpcontrol.h"

void FPCONTROL_vEnableInterrupt(unsigned int uiIntMask)
{
	unsigned int status;
	status = _controlfp(0, 0);
	status &= (~uiIntMask);
	_controlfp(status, _MCW_EM);
}

void FPCONTROL_vPrepareForComputations(void)
{
	_controlfp(_DN_FLUSH, _MCW_DN); // flush denormals
	FPCONTROL_uiChangePrecision(_PC_64);
	FPCONTROL_uiDisableInterrupts();
	FPCONTROL_vEnableInterrupt(EM_INVALID);
	FPCONTROL_vEnableInterrupt(EM_DENORMAL);
	FPCONTROL_uiClearStatus();
}

int FPCONTROL_iExceptionHandler( _FPIEEE_RECORD *pieee )
{
	// ieee trap handler routine:
   // there is one handler for all 
   // IEEE exceptions
#if 0
	unsigned int status;
	status = _statusfp();
   if (status & _SW_DENORMAL) //denormal value caused the exception
   {
        pieee->Result.Value.Fp64Value = 0.0F; //set the value to 0
        return EXCEPTION_CONTINUE_EXECUTION;
   }
   if(pieee->Cause.InvalidOperation == 1)
   {
	   pieee->Result.Value.Fp64Value = 0.0F; //set the value to 0
	   return EXCEPTION_CONTINUE_EXECUTION;
   }
   else
#endif
      return EXCEPTION_EXECUTE_HANDLER; // other exceptions
}

