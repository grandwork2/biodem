/****************************************************************************** 
 * 
 *  file:  walls.hpp
 * 
 *  Copyright (c) 2016, Grand R. Joldes.
 *  All rights reverved.
 * 
 *  
 *  THE SOFTWARE IS PROVIDED _AS IS_, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *  DEALINGS IN THE SOFTWARE.  
 *  
 *****************************************************************************/ 

#ifndef __WALLS_HPP__
#define __WALLS_HPP__

#include <string>
#include <vtkPolyData.h>

#include "options.hpp"

namespace BioDEM {

class CWalls 
{
public:
	CWalls() {};
	~CWalls() {};

	bool boConfigure(program_options *pPO);

	bool boSetWalls(const std::string &conf);
	float *pfGetLocation(void) {return afLocation;};
	float *pfGetInitialLocation(void) {return afInitialLocation;};
	bool *pboGetActive(void) {return aboActive;};
	void vInitOutputWalls(vtkPolyData *pOutputWalls);
	void vUpdateOutputWalls(vtkPolyData *pOutputWalls);
private:
	float afLocation[6]; // xmin, xmax, ymin, ymax, zmin, zmax
	float afInitialLocation[6];
	bool aboActive[6];
};

} // namespace

#endif /* __WALLS_HPP__ */