/****************************************************************************** 
 * 
 *  file:  walls.cpp
 * 
 *  Copyright (c) 2016, Grand R. Joldes.
 *  All rights reverved.
 * 
 *  
 *  THE SOFTWARE IS PROVIDED _AS IS_, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *  DEALINGS IN THE SOFTWARE.  
 *  
 *****************************************************************************/ 

#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <stdio.h>
#include <vtkPoints.h>
#include <vtkCellArray.h>
#include <vtkSmartPointer.h>

#include "walls.hpp"
#include "vect.hpp"

namespace BioDEM {

bool CWalls::boSetWalls(const std::string &conf)
{
	int aiActive[6];
	int n = sscanf(conf.c_str(), "%f,%f,%f,%f,%f,%f,%d,%d,%d,%d,%d,%d", 
		afLocation, afLocation+1, afLocation+2, afLocation+3, afLocation+4, afLocation+5,
		aiActive, aiActive+1, aiActive+2, aiActive+3, aiActive+4, aiActive+5);
	if (n!=12) return false;
	for (unsigned int i = 0; i < 6; i++)
	{
		aboActive[i] = (aiActive[i] > 0);
	};
	if (aboActive[0] && aboActive[1] && (afLocation[1] <= afLocation[0])) return false;  
	if (aboActive[2] && aboActive[3] && (afLocation[3] <= afLocation[2])) return false;  
	if (aboActive[4] && aboActive[5] && (afLocation[5] <= afLocation[4])) return false;  
	VECT_vCopy(afLocation, afInitialLocation, 6);
	return true;
};

bool CWalls::boConfigure(program_options *pPO)
{
	// get walls
	std::string sWallsDef = pPO->GetWalls();
	sWallsDef.erase(std::remove_if(sWallsDef.begin(), sWallsDef.end(), ::isspace), sWallsDef.end());
	if (!boSetWalls(sWallsDef))
	{
		std::cout << "Error: Incorrect walls definition! " << std::endl;
		return false;
	}
	else
	{
		std::cout << "\tWalls: " << sWallsDef << std::endl;
	}
	return true;
}

void CWalls::vInitOutputWalls(vtkPolyData *pOutputWalls)
{
	vtkSmartPointer<vtkPoints> pPoints = vtkSmartPointer<vtkPoints>::New();
	vtkSmartPointer<vtkCellArray> pFaces = vtkSmartPointer<vtkCellArray>::New();

	// face at xMin
	unsigned int uiNextPointIdx = 0;
	if (aboActive[0])
	{
		pPoints->InsertNextPoint(afInitialLocation[0], afInitialLocation[2], afInitialLocation[4]);
		pPoints->InsertNextPoint(afInitialLocation[0], afInitialLocation[2], afInitialLocation[5]);
		pPoints->InsertNextPoint(afInitialLocation[0], afInitialLocation[3], afInitialLocation[5]);
		pPoints->InsertNextPoint(afInitialLocation[0], afInitialLocation[3], afInitialLocation[4]);
		pFaces->InsertNextCell(4);
		for (unsigned int i = 0; i < 4; i++)
		{
			pFaces->InsertCellPoint(uiNextPointIdx);
			uiNextPointIdx++;
		}
	}

	// face at xMax
	if (aboActive[1])
	{
		pPoints->InsertNextPoint(afInitialLocation[1], afInitialLocation[2], afInitialLocation[4]);
		pPoints->InsertNextPoint(afInitialLocation[1], afInitialLocation[2], afInitialLocation[5]);
		pPoints->InsertNextPoint(afInitialLocation[1], afInitialLocation[3], afInitialLocation[5]);
		pPoints->InsertNextPoint(afInitialLocation[1], afInitialLocation[3], afInitialLocation[4]);
		pFaces->InsertNextCell(4);
		for (unsigned int i = 0; i < 4; i++)
		{
			pFaces->InsertCellPoint(uiNextPointIdx);
			uiNextPointIdx++;
		}
	}

	// face at yMin
	if (aboActive[2])
	{
		pPoints->InsertNextPoint(afInitialLocation[0], afInitialLocation[2], afInitialLocation[4]);
		pPoints->InsertNextPoint(afInitialLocation[0], afInitialLocation[2], afInitialLocation[5]);
		pPoints->InsertNextPoint(afInitialLocation[1], afInitialLocation[2], afInitialLocation[5]);
		pPoints->InsertNextPoint(afInitialLocation[1], afInitialLocation[2], afInitialLocation[4]);
		pFaces->InsertNextCell(4);
		for (unsigned int i = 0; i < 4; i++)
		{
			pFaces->InsertCellPoint(uiNextPointIdx);
			uiNextPointIdx++;
		}
	}
	// face at yMax
	if (aboActive[3])
	{
		pPoints->InsertNextPoint(afInitialLocation[0], afInitialLocation[3], afInitialLocation[4]);
		pPoints->InsertNextPoint(afInitialLocation[0], afInitialLocation[3], afInitialLocation[5]);
		pPoints->InsertNextPoint(afInitialLocation[1], afInitialLocation[3], afInitialLocation[5]);
		pPoints->InsertNextPoint(afInitialLocation[1], afInitialLocation[3], afInitialLocation[4]);
		pFaces->InsertNextCell(4);
		for (unsigned int i = 0; i < 4; i++)
		{
			pFaces->InsertCellPoint(uiNextPointIdx);
			uiNextPointIdx++;
		}
	}

	// face at zMin
	if (aboActive[4])
	{
		pPoints->InsertNextPoint(afInitialLocation[0], afInitialLocation[2], afInitialLocation[4]);
		pPoints->InsertNextPoint(afInitialLocation[0], afInitialLocation[3], afInitialLocation[4]);
		pPoints->InsertNextPoint(afInitialLocation[1], afInitialLocation[3], afInitialLocation[4]);
		pPoints->InsertNextPoint(afInitialLocation[1], afInitialLocation[2], afInitialLocation[4]);
		pFaces->InsertNextCell(4);
		for (unsigned int i = 0; i < 4; i++)
		{
			pFaces->InsertCellPoint(uiNextPointIdx);
			uiNextPointIdx++;
		}
	}

	// face at zMax
	if (aboActive[5])
	{
		pPoints->InsertNextPoint(afInitialLocation[0], afInitialLocation[2], afInitialLocation[5]);
		pPoints->InsertNextPoint(afInitialLocation[0], afInitialLocation[3], afInitialLocation[5]);
		pPoints->InsertNextPoint(afInitialLocation[1], afInitialLocation[3], afInitialLocation[5]);
		pPoints->InsertNextPoint(afInitialLocation[1], afInitialLocation[2], afInitialLocation[5]);
		pFaces->InsertNextCell(4);
		for (unsigned int i = 0; i < 4; i++)
		{
			pFaces->InsertCellPoint(uiNextPointIdx);
			uiNextPointIdx++;
		}
	}

	pOutputWalls->SetPoints(pPoints);
	pOutputWalls->SetPolys(pFaces);

}

void CWalls::vUpdateOutputWalls(vtkPolyData *pOutputWalls)
{
	vtkPoints *pPoints = pOutputWalls->GetPoints();
	
	// face at xMin
	unsigned int uiNextPointIdx = 0;
	if (aboActive[0])
	{
		for (unsigned int i = 0; i < 4; i++)
		{
			pPoints->GetData()->SetComponent(uiNextPointIdx, 0, afLocation[0]);
			uiNextPointIdx++;
		}
	}

	// face at xMax
	if (aboActive[1])
	{
		for (unsigned int i = 0; i < 4; i++)
		{
			pPoints->GetData()->SetComponent(uiNextPointIdx, 0, afLocation[1]);
			uiNextPointIdx++;
		}
	}

	// face at yMin
	if (aboActive[2])
	{
		for (unsigned int i = 0; i < 4; i++)
		{
			pPoints->GetData()->SetComponent(uiNextPointIdx, 1, afLocation[2]);
			uiNextPointIdx++;
		}
	}
	// face at yMax
	if (aboActive[3])
	{
		for (unsigned int i = 0; i < 4; i++)
		{
			pPoints->GetData()->SetComponent(uiNextPointIdx, 1, afLocation[3]);
			uiNextPointIdx++;
		}
	}

	// face at zMin
	if (aboActive[4])
	{
		for (unsigned int i = 0; i < 4; i++)
		{
			pPoints->GetData()->SetComponent(uiNextPointIdx, 2, afLocation[4]);
			uiNextPointIdx++;
		}
	}

	// face at zMax
	if (aboActive[5])
	{
		for (unsigned int i = 0; i < 4; i++)
		{
			pPoints->GetData()->SetComponent(uiNextPointIdx, 2, afLocation[5]);
			uiNextPointIdx++;
		}
	}
}


} // namespace

