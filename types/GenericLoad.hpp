/****************************************************************************** 
 * 
 *  file:  GenericLoad.hpp
 * 
 *  Copyright (c) 2016, Grand R. Joldes.
 *  All rights reverved.
 * 
 *  
 *  THE SOFTWARE IS PROVIDED _AS IS_, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *  DEALINGS IN THE SOFTWARE.  
 *  
 *****************************************************************************/ 

#ifndef __GENERICLOAD_HPP__
#define __GENERICLOAD_HPP__

#include <string>
#include <vector>

#include <vtkPolyData.h>
#include <vtkSmartPointer.h>

namespace BioDEM {

class CGenericLoad
{
public:
	typedef enum
	{
		LOAD_enEncastred,
		LOAD_enDisplaced,
		LOAD_enForce,
		LOAD_enAcceleration
	} tenLoadType;

	CGenericLoad(void):enType(LOAD_enEncastred), boX(false), boY(false), boZ(false), fX(0), fY(0), fZ(0) {};
	~CGenericLoad(void) {};
	void vSetType(tenLoadType t) {enType = t;};
	tenLoadType enGetType(void) {return enType;};
	bool boConfigure(std::string config, vtkSmartPointer<vtkPolyData> inputPolyData, unsigned int uiNumSteps);
	// member access functions
	void vSetName(const char *pcName) {sName = pcName;};
	const std::string &psGetName(void) {return sName;};
	bool boOnX(void) {return boX;};
	bool boOnY(void) {return boY;};
	bool boOnZ(void) {return boZ;};
	float fGetSizeX(void) {return fX;};
	float fGetSizeY(void) {return fY;};
	float fGetSizeZ(void) {return fZ;};
	void vSetSizeX(float fSize) {fX = fSize;};
	void vSetSizeY(float fSize) {fY = fSize;};
	void vSetSizeZ(float fSize) {fZ = fSize;};
	unsigned int uiGetNumberOfLoadedPoints(void) {return (unsigned int)vectorLoadedIndexes.size();};
	unsigned int uiGetLoadedPointIndex(unsigned int uiIdx) {return vectorLoadedIndexes[uiIdx];};
	bool boActiveInStep(unsigned int uiStepNo) {return aboActiveInStep[uiStepNo];};
	void vDisable(unsigned int uiStepNo);
protected:
	std::string sName;
	std::vector<unsigned int> vectorLoadedIndexes;
	std::vector<bool> aboActiveInStep;
	tenLoadType enType;
	// Direction of loads
	bool boX;
	bool boY;
	bool boZ;
	// Size of loads
	float fX;
	float fY;
	float fZ;
	// Amplitude
	unsigned int uiAmplitudeIdx;
};

class CGrowth
{
public:
	CGrowth(void) {};
	~CGrowth(void) {};
	bool boConfigure(std::string config);
	unsigned int uiStepNumber;
	unsigned int uiParticleType;
	float fGrowthFactor;
};

}
#endif // __GENERICLOAD_HPP__