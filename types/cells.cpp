/****************************************************************************** 
 * 
 *  file:  cells.cpp
 * 
 *  Copyright (c) 2016, Grand R. Joldes.
 *  All rights reverved.
 * 
 *  
 *  THE SOFTWARE IS PROVIDED _AS IS_, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *  DEALINGS IN THE SOFTWARE.  
 *  
 *****************************************************************************/ 

#include <iostream>

#include "cells.hpp"
#include "vect.hpp"

namespace BioDEM {

CCells::CCells(unsigned int uiMaxNumCells, unsigned int uiMaxNumParticles):uiMaxNumberOfCells(uiMaxNumCells), uiNumberOfCells(0)
{

	{
		std::cout << "Error: Incorrect cells definition! " << std::endl;
		exit(1);
	}
};

CCells::~CCells()
{

};

} // namespace

