/****************************************************************************** 
 * 
 *  file:  GenericSolver.cpp
 * 
 *  Copyright (c) 2016, Grand R. Joldes.
 *  All rights reverved.
 * 
 *  
 *  THE SOFTWARE IS PROVIDED _AS IS_, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *  DEALINGS IN THE SOFTWARE.  
 *  
 *****************************************************************************/ 
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <iostream>
#include <boost/algorithm/string.hpp>
#include <boost/bind.hpp>

#include <vtkSmartPointer.h>
#include <vtkPoints.h>
#include <vtkFloatArray.h>
#include <vtkUnsignedIntArray.h>
#include <vtkUnsignedCharArray.h>
#include <vtkPointData.h>
#include <vtkPolyData.h>
#include <vtkCellArray.h>
#include <vtkVertex.h>
#include <vtkUnsignedCharArray.h>

#include "GenericSolver.hpp"
#include "ExplicitSolver.hpp"
#include "ChangeRadiusSolver.hpp"
#include "vect.hpp"

namespace BioDEM {

CGenericSolver::CGenericSolver():pstGlobalVars(NULL), pCDiscretisation(NULL),pCWorkspace(NULL),
		uiDimensions(0),uiTotalNumParticles(0), uiTotalNumberDOF(0), fStartTime(0),
		fEndTime(0),uiStartIteration(0), uiEndIteration(0), fDampingParam(0.99), boMarkedForDeletion(false)
{
	VECT_vInit(afWallMotion, 6, 0.0f);
	VECT_vInit(afWallForces, 6, 0.0f);
};

void CGenericSolver::vSetWorkspace(CWorkspace *pW) 
{
	pCWorkspace = pW; 
	pCDiscretisation = pW->pGetDiscretisation(); 
	uiDimensions=pW->uiGetNumDimensions();
};

CGenericSolver* CGenericSolver::pCCreateSolver(std::string config, CWorkspace *pW)
{
	std::size_t pos = config.find(",");
	if (pos!=std::string::npos)
	{
		std::string sType = config.substr(0, pos);
		if (boost::iequals(sType, "Init"))
		{
			CInitSolver *pC = new CInitSolver();
			pC->vSetWorkspace(pW);
			return pC;
		}
		else
		{
			std::cout << "\t\tType: " << sType << std::endl;
			if (boost::iequals(sType, "Explicit"))
			{
				CExplicitSolver *pC = new CExplicitSolver();
				pC->vSetWorkspace(pW);
				if (!pC->boConfigure(config.substr(pos+1, std::string::npos))) return NULL;
				return pC;
			}
			else if (boost::iequals(sType, "ChangeRadius"))
			{
				CChangeRadiusSolver *pC = new CChangeRadiusSolver();
				pC->vSetWorkspace(pW);
				if (!pC->boConfigure(config.substr(pos+1, std::string::npos))) return NULL;
				return pC;
			}
			else return NULL;
		}
	}
	else 
	{
		std::cout << "Error: Step type not found!" << std::endl;
		return NULL;
	};
	return NULL;
};

void CGenericSolver::vCopyParticle(unsigned int fromIdx, unsigned int toIdx)
{
#define vCopyVector3(var) var[3*toIdx] = var[3*fromIdx];\
	var[3*toIdx+1] = var[3*fromIdx+1];\
	var[3*toIdx+2] = var[3*fromIdx+2]

#define vCopyScalar(var) var[toIdx] = var[fromIdx]

	vCopyVector3(pstGlobalVars->pfNewDisplacements);
	vCopyVector3(pstGlobalVars->pfReactionForces);
	vCopyVector3(pstGlobalVars->pfOldDisplacements);
	vCopyVector3(pstGlobalVars->pfCurrentDisplacements);
	vCopyVector3(pstGlobalVars->pfOldVelocity);
	vCopyVector3(pstGlobalVars->pfNewVelocity);

	vCopyVector3(pstGlobalVars->pfParticlesCoordinates);
	vCopyVector3(pstGlobalVars->pfParticlesInitialCoordinates);
	vCopyVector3(pstGlobalVars->pfNewVelocity);
	vCopyVector3(pstGlobalVars->pfNewVelocity);

	vCopyScalar(pstGlobalVars->pfParticlesRadius);
	vCopyScalar(pstGlobalVars->pfParticlesDensity);
	vCopyScalar(pstGlobalVars->pucParticlesType);
	vCopyScalar(pstGlobalVars->pfMembraneParticleInterractionRadius);
	vCopyScalar(pstGlobalVars->pucMarkedForDeletion);
	vCopyScalar(pstGlobalVars->pfGrowthFactor);
	vCopyScalar(pstGlobalVars->pfInterractionGrowthFactor);
	
	vCopyScalar(pstGlobalVars->pfParticleIntegrity);
}

bool CGenericSolver::boSetWallMotion(std::string conf)
{
	float afMotion[6];
	int n = sscanf(conf.c_str(), "%f,%f,%f,%f,%f,%f", 
		afMotion, afMotion+1, afMotion+2, afMotion+3, afMotion+4, afMotion+5);
	if (n!=6) 
	{
		std::cout << "Warning: invalid wall motion configuration: " << conf << std::endl; 
		std::cout << "! Configuration will be ignored." << std::endl;
		return false;
	}
	VECT_vCopy(afMotion, afWallMotion, 6);
	std::cout << "\t\tWall motion: " << conf << std::endl;
	return true;
}

void CGenericSolver::vMarkForDeletion(unsigned int uiIdx)
{
	pstGlobalVars->pucMarkedForDeletion[uiIdx] = 1;
	boMarkedForDeletion = true;
}

unsigned int CGenericSolver::uiDeleteMarkedParticles(void)
{
	unsigned int uiNumActiveParticles = pCDiscretisation->uiGetNumberOfActiveParticles();
	if (boMarkedForDeletion)
	{
		// delete marked particles
		for (unsigned int i = 0; i < uiNumActiveParticles; i++)
		{
			unsigned int particleIdx = pCDiscretisation->idGetActiveParticleIndex(i);
			if (pstGlobalVars->pucMarkedForDeletion[particleIdx])
			{
				uiDeactivateParticle(particleIdx);
			}
		}
		boMarkedForDeletion = false;
		uiNumActiveParticles = pCDiscretisation->uiGetNumberOfActiveParticles();
	}
	return uiNumActiveParticles;
}

CInitSolver::CInitSolver(void)
: CGenericSolver()
{
	enSolverType = SOLVER_enInit;
	pstGlobalVars = new tstGlobalVars;
	pstGlobalVars->pfNewDisplacements = NULL; 
	pstGlobalVars->pfReactionForces = NULL; 
	pstGlobalVars->pfOldDisplacements = NULL; 
	pstGlobalVars->pfCurrentDisplacements = NULL; 
	pstGlobalVars->pfOldVelocity = NULL;
	pstGlobalVars->pfNewVelocity = NULL;

	pstGlobalVars->pfParticlesRadius = NULL;
	pstGlobalVars->pfParticlesDensity = NULL;
	pstGlobalVars->pfParticlesCoordinates = NULL;
	pstGlobalVars->pfParticlesInitialCoordinates = NULL;
	pstGlobalVars->pucParticlesType = NULL;
	pstGlobalVars->puiActiveParticles = NULL;
	pstGlobalVars->pucMarkedForDeletion = NULL;
	
	pstGlobalVars->pfMembraneParticleInterractionRadius = NULL;
	pstGlobalVars->pfGrowthFactor = NULL;
	pstGlobalVars->pfInterractionGrowthFactor = NULL;
	pstGlobalVars->pfParticleIntegrity = NULL;
}

CInitSolver::~CInitSolver(void)
{
	if (pstGlobalVars->pfNewDisplacements != NULL) delete[] pstGlobalVars->pfNewDisplacements;
	if (pstGlobalVars->pfOldDisplacements != NULL) delete[] pstGlobalVars->pfOldDisplacements;
	if (pstGlobalVars->pfCurrentDisplacements != NULL) delete[] pstGlobalVars->pfCurrentDisplacements;
	if (pstGlobalVars->pfOldVelocity != NULL) delete[] pstGlobalVars->pfOldVelocity;
	if (pstGlobalVars->pfNewVelocity != NULL) delete[] pstGlobalVars->pfNewVelocity;
	if (pstGlobalVars->pfReactionForces != NULL) delete[] pstGlobalVars->pfReactionForces;

	if (pstGlobalVars->pfParticlesRadius != NULL) delete[] pstGlobalVars->pfParticlesRadius;
	if (pstGlobalVars->pfParticlesDensity != NULL) delete[] pstGlobalVars->pfParticlesDensity;
	if (pstGlobalVars->pfParticlesCoordinates != NULL) delete[] pstGlobalVars->pfParticlesCoordinates;
	if (pstGlobalVars->pfParticlesInitialCoordinates != NULL) delete[] pstGlobalVars->pfParticlesInitialCoordinates;
	if (pstGlobalVars->pucParticlesType != NULL) delete[] pstGlobalVars->pucParticlesType;
	if (pstGlobalVars->puiActiveParticles != NULL) delete[] pstGlobalVars->puiActiveParticles;
	if (pstGlobalVars->pucMarkedForDeletion != NULL) delete[] pstGlobalVars->pucMarkedForDeletion;
	if (pstGlobalVars->pfMembraneParticleInterractionRadius != NULL) delete[] pstGlobalVars->pfMembraneParticleInterractionRadius;
	if (pstGlobalVars->pfGrowthFactor != NULL) delete[] pstGlobalVars->pfGrowthFactor;
	if (pstGlobalVars->pfInterractionGrowthFactor != NULL) delete[] pstGlobalVars->pfInterractionGrowthFactor;
	if (pstGlobalVars->pfParticleIntegrity != NULL) delete[] pstGlobalVars->pfParticleIntegrity;
	delete pstGlobalVars;
}

bool CInitSolver::boPerformAnalysis(void)
{
	uiTotalNumParticles = pCDiscretisation->uiGetTotalNumberOfParticles();
	uiTotalNumberDOF = 3*uiTotalNumParticles;
	std::cout << "\tAllocating memory for global variables ..." << std::endl;
	pstGlobalVars->pfNewDisplacements = new float[uiTotalNumberDOF]; 
	pstGlobalVars->pfReactionForces = new float[uiTotalNumberDOF]; 
	pstGlobalVars->pfOldDisplacements = new float[uiTotalNumberDOF]; 
	pstGlobalVars->pfCurrentDisplacements = new float[uiTotalNumberDOF]; 
	pstGlobalVars->pfOldVelocity = new float[uiTotalNumberDOF];
	pstGlobalVars->pfNewVelocity = new float[uiTotalNumberDOF];

	pstGlobalVars->pfParticlesRadius = new float[uiTotalNumParticles];
	pstGlobalVars->pfParticlesDensity = new float[uiTotalNumParticles];
	pstGlobalVars->pfParticlesCoordinates = new float[uiTotalNumberDOF];
	pstGlobalVars->pfParticlesInitialCoordinates = new float[uiTotalNumberDOF];
	pstGlobalVars->pucParticlesType = new unsigned char[uiTotalNumParticles];
	pstGlobalVars->puiActiveParticles = new unsigned char[uiTotalNumParticles];
	pstGlobalVars->pucMarkedForDeletion = new unsigned char[uiTotalNumParticles];
	
	pstGlobalVars->pfMembraneParticleInterractionRadius = new float[uiTotalNumParticles];
	pstGlobalVars->pfGrowthFactor = new float[uiTotalNumParticles];
	pstGlobalVars->pfInterractionGrowthFactor = new float[uiTotalNumParticles];
	pstGlobalVars->pfParticleIntegrity = new float[uiTotalNumParticles];
		
	if ((!pstGlobalVars->pfNewDisplacements) || (!pstGlobalVars->pfReactionForces) || 
		(!pstGlobalVars->pfOldDisplacements) || (!pstGlobalVars->pfCurrentDisplacements) || 
		(!pstGlobalVars->pfOldVelocity) || (!pstGlobalVars->pfNewVelocity) ||
		(!pstGlobalVars->pfParticlesRadius) || (!pstGlobalVars->pfParticlesDensity) || 
		(!pstGlobalVars->pfMembraneParticleInterractionRadius) || (!pstGlobalVars->pfInterractionGrowthFactor) ||
		(!pstGlobalVars->pfParticlesCoordinates) || (!pstGlobalVars->pucParticlesType) || 
		(!pstGlobalVars->pfGrowthFactor) || (!pstGlobalVars->pfParticleIntegrity) ||
		(!pstGlobalVars->puiActiveParticles) || (!pstGlobalVars->pucMarkedForDeletion)) return false; 
	std::cout << "\tInitialising global variables ..." << std::endl;
	VECT_vInit(pstGlobalVars->pfNewDisplacements, uiTotalNumberDOF, (float)0.);
	VECT_vInit(pstGlobalVars->pfReactionForces, uiTotalNumberDOF, (float)0.);
	VECT_vInit(pstGlobalVars->pfOldDisplacements, uiTotalNumberDOF, (float)0.);
	VECT_vInit(pstGlobalVars->pfCurrentDisplacements, uiTotalNumberDOF, (float)0.);
	VECT_vInit(pstGlobalVars->pfOldVelocity, uiTotalNumberDOF, (float)0.);
	VECT_vInit(pstGlobalVars->pfNewVelocity, uiTotalNumberDOF, (float)0.);
	VECT_vInit(pstGlobalVars->pucMarkedForDeletion, uiTotalNumParticles, (unsigned char)0);
	VECT_vInit(pstGlobalVars->pfGrowthFactor, uiTotalNumParticles, 1.0f);
	VECT_vInit(pstGlobalVars->pfInterractionGrowthFactor, uiTotalNumParticles, 1.0f);
	VECT_vInit(pstGlobalVars->pfParticleIntegrity, uiTotalNumParticles, 1.0f);

	VECT_vCopy(pCDiscretisation->pfGetCoordinates(), pstGlobalVars->pfParticlesCoordinates, uiTotalNumberDOF);
	VECT_vCopy(pCDiscretisation->pfGetCoordinates(), pstGlobalVars->pfParticlesInitialCoordinates, uiTotalNumberDOF);
	VECT_vCopy(pCDiscretisation->pfGetRadius(), pstGlobalVars->pfParticlesRadius, uiTotalNumParticles);
	VECT_vCopy(pCDiscretisation->pfGetDensity(), pstGlobalVars->pfParticlesDensity, uiTotalNumParticles);
	VECT_vCopy(pCDiscretisation->pucGetType(), pstGlobalVars->pucParticlesType, uiTotalNumParticles);

	VECT_vCopy(pCDiscretisation->pfGetRadius(), pstGlobalVars->pfMembraneParticleInterractionRadius, uiTotalNumParticles);

	// check if membrane radius present in input file
	pCDiscretisation->pGetInputPolyData()->GetPointData()->SetActiveScalars("MR");
	vtkFloatArray *pafRadius = vtkFloatArray::SafeDownCast(pCDiscretisation->pGetInputPolyData()->GetPointData()->GetScalars());
	if (pafRadius)
	{
		pafRadius->ExportToVoidPointer(pstGlobalVars->pfMembraneParticleInterractionRadius);
	}
	
	unsigned int uiNumActiveParticles = pCDiscretisation->uiGetNumberOfActiveParticles();
	VECT_vInit(pstGlobalVars->puiActiveParticles, uiTotalNumParticles, (unsigned char)0);
	VECT_vAttribScalarToSubVector(pstGlobalVars->puiActiveParticles, pCDiscretisation->pIdGetActiveParticleIndexes(), uiNumActiveParticles, (unsigned char)1);

	// Prepare output data structure
	vtkPolyData *pPolyData = pCWorkspace->pGetOutputPolyData();

	// Create a float array which represents the points.
    vtkSmartPointer<vtkFloatArray> pcoords = vtkSmartPointer<vtkFloatArray>::New();
	pcoords->SetNumberOfComponents(3);
	pcoords->SetNumberOfTuples(uiTotalNumParticles);
	pcoords->SetVoidArray(pstGlobalVars->pfParticlesCoordinates, 3*uiTotalNumParticles, 1);
 
	// Create vtkPoints and assign pcoords as the internal data array.
	vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
	points->SetData(pcoords);
	pPolyData->SetPoints(points);

	// create point data
	vtkSmartPointer<vtkFloatArray> arrayRadius = vtkSmartPointer<vtkFloatArray>::New();
	arrayRadius->SetNumberOfComponents(1);
	arrayRadius->SetName("R");
	arrayRadius->SetVoidArray(pstGlobalVars->pfParticlesRadius, uiTotalNumParticles, 1);

	vtkSmartPointer<vtkUnsignedCharArray> arrayType = vtkSmartPointer<vtkUnsignedCharArray>::New();
	arrayType->SetNumberOfComponents(1);
	arrayType->SetName("Type");
	arrayType->SetVoidArray(pstGlobalVars->pucParticlesType, uiTotalNumParticles, 1);

	vtkSmartPointer<vtkFloatArray> arrayDens = vtkSmartPointer<vtkFloatArray>::New();
	arrayDens->SetNumberOfComponents(1);
	arrayDens->SetName("Ro");
	arrayDens->SetVoidArray(pstGlobalVars->pfParticlesDensity, uiTotalNumParticles, 1);

	vtkSmartPointer<vtkFloatArray> arrayMembRadius = vtkSmartPointer<vtkFloatArray>::New();
	arrayMembRadius->SetNumberOfComponents(1);
	arrayMembRadius->SetName("MR");
	arrayMembRadius->SetVoidArray(pstGlobalVars->pfMembraneParticleInterractionRadius, uiTotalNumParticles, 1);

	vtkSmartPointer<vtkFloatArray> arrayForce = vtkSmartPointer<vtkFloatArray>::New();
	arrayForce->SetNumberOfComponents(3);
	arrayForce->SetName("Force");
	arrayForce->SetVoidArray(pstGlobalVars->pfReactionForces, 3*uiTotalNumParticles, 1);

	vtkSmartPointer<vtkUnsignedCharArray> arrayActive = vtkSmartPointer<vtkUnsignedCharArray>::New();
	arrayActive->SetNumberOfComponents(1);
	arrayActive->SetName("Active");
	arrayActive->SetVoidArray(pstGlobalVars->puiActiveParticles, uiTotalNumParticles, 1);

	vtkSmartPointer<vtkUnsignedCharArray> arrayMembrane = vtkSmartPointer<vtkUnsignedCharArray>::New();
	arrayMembrane->SetNumberOfComponents(1);
	arrayMembrane->SetName("Membrane");
	arrayMembrane->SetVoidArray(pCDiscretisation->pucMembraneIdx, uiTotalNumParticles, 1);

	vtkSmartPointer<vtkFloatArray> arrayIntegrity = vtkSmartPointer<vtkFloatArray>::New();
	arrayIntegrity->SetNumberOfComponents(1);
	arrayIntegrity->SetName("Integrity");
	arrayIntegrity->SetVoidArray(pstGlobalVars->pfParticleIntegrity, uiTotalNumParticles, 1);

	pPolyData->GetPointData()->SetActiveScalars("R");
	pPolyData->GetPointData()->SetScalars(arrayRadius);
	pPolyData->GetPointData()->SetActiveScalars("Type");
	pPolyData->GetPointData()->SetScalars(arrayType);
	pPolyData->GetPointData()->SetActiveVectors("Force");
	pPolyData->GetPointData()->SetVectors(arrayForce);
	pPolyData->GetPointData()->SetActiveScalars("Ro");
	pPolyData->GetPointData()->SetScalars(arrayDens);
	pPolyData->GetPointData()->SetActiveScalars("MR");
	pPolyData->GetPointData()->SetScalars(arrayMembRadius);
	pPolyData->GetPointData()->SetActiveScalars("Active");
	pPolyData->GetPointData()->SetScalars(arrayActive);
	pPolyData->GetPointData()->SetActiveScalars("Membrane");
	pPolyData->GetPointData()->SetScalars(arrayMembrane);
	pPolyData->GetPointData()->SetActiveScalars("Integrity");
	pPolyData->GetPointData()->SetScalars(arrayIntegrity);

	// make sure we have enough membrane particles
	if (pCDiscretisation->boCloseMembrane())
	{
		std::cout << "\tClosing membrane ..." << std::endl;
		float fMaxParticleRadius = (float)VECT_f8MaxValue(pstGlobalVars->pfParticlesRadius, uiTotalNumParticles);
		float fBucketSize = 3*fMaxParticleRadius;
		float *afBoundingBox = pCDiscretisation->pfGetBoundingBox();
		unsigned int uiNumBucketsX = (unsigned int)((afBoundingBox[1]-afBoundingBox[0])/fBucketSize) + 1;
		unsigned int uiNumBucketsY = (unsigned int)((afBoundingBox[3]-afBoundingBox[2])/fBucketSize) + 1;
		unsigned int uiNumBucketsZ = (unsigned int)((afBoundingBox[5]-afBoundingBox[4])/fBucketSize) + 1;
		std::cout << "\tBuckets: " << uiNumBucketsX << " x " << uiNumBucketsY << " x " << uiNumBucketsZ << std::endl;

		if ((uiDimensions == 2) && (uiNumBucketsZ > 1))
		{
			std::cout << "Warning: particles are not in the same plane for the configured 2D analysis!" << std::endl;
		}

		BucketSearch::buckets *pCBuckets = new BucketSearch::buckets(afBoundingBox[0], afBoundingBox[2], afBoundingBox[4],
			fBucketSize, fBucketSize, fBucketSize, uiNumBucketsX, uiNumBucketsY, uiNumBucketsZ);

		BucketSearch::BucketSearch *pCBucketSearch = new BucketSearch::BucketSearch(pCBuckets, pstGlobalVars->pfParticlesCoordinates, uiTotalNumParticles);
		pCBucketSearch->vDistributeActivePointsToBuckets(pCDiscretisation->pIdGetActiveParticleIndexes(), uiNumActiveParticles);
		for (unsigned int i = 0; i < uiNumActiveParticles; i++)
		{
			unsigned int particleIdx = pCDiscretisation->idGetActiveParticleIndex(i);
			pCBucketSearch->vOperateOnNeighboursUsingMask(boost::bind(&BioDEM::CInitSolver::vCloseMembrane, this, _1, _2), uiDimensions, particleIdx);
		}
		
		// detect overclosures
		uiNumActiveParticles = pCDiscretisation->uiGetNumberOfActiveParticles();
		pCBucketSearch->vDistributeActivePointsToBuckets(pCDiscretisation->pIdGetActiveParticleIndexes(), uiNumActiveParticles);
		for (unsigned int i = 0; i < uiNumActiveParticles; i++)
		{
			unsigned int particleIdx = pCDiscretisation->idGetActiveParticleIndex(i);
			pCBucketSearch->vOperateOnNeighboursUsingMask(boost::bind(&BioDEM::CInitSolver::vCleanMembrane, this, _1, _2), uiDimensions, particleIdx);
		}
		// delete marked particles
		uiDeleteMarkedParticles();
		
		delete pCBuckets;
		delete pCBucketSearch;
	}

	return true;
}

void CInitSolver::vCloseMembrane(unsigned int uiParticleIdx, unsigned int uiNeighbourIndex)
{
	if (pCDiscretisation->boIsMembrane(uiParticleIdx))
	{
		unsigned char ucParticleType1 = pstGlobalVars->pucParticlesType[uiParticleIdx];
		unsigned char ucParticleType2 = pstGlobalVars->pucParticlesType[uiNeighbourIndex];
		if (ucParticleType1 == ucParticleType2)
		{
			// compute particle penetration
			float *pfCoord1 = pstGlobalVars->pfParticlesCoordinates + 3*uiParticleIdx;
			float *pfCoord2 = pstGlobalVars->pfParticlesCoordinates + 3*uiNeighbourIndex;
			float fRadius1 = pstGlobalVars->pfParticlesRadius[uiParticleIdx];
			float fRadius2 = pstGlobalVars->pfParticlesRadius[uiNeighbourIndex];
			float fd12 = VECT_ComputeDistance(pfCoord1, pfCoord2, uiDimensions);
			float fParticleDistance = fd12 - fRadius1 - fRadius2;
			if ((fParticleDistance > (fRadius1/2)) && (fParticleDistance < 1.5*fRadius1))
			{
				// insert a particle in between the 2 membrane particles
				unsigned int idx = uiActivateParticle();
				pstGlobalVars->pfParticlesInitialCoordinates[3*idx] = (pfCoord1[0] + pfCoord2[0])/2.0f;
				pstGlobalVars->pfParticlesInitialCoordinates[3*idx+1] = (pfCoord1[1] + pfCoord2[1])/2.0f;
				pstGlobalVars->pfParticlesInitialCoordinates[3*idx+2] = (pfCoord1[2] + pfCoord2[2])/2.0f;
				pstGlobalVars->pfParticlesCoordinates[3*idx] = pstGlobalVars->pfParticlesInitialCoordinates[3*idx];
				pstGlobalVars->pfParticlesCoordinates[3*idx+1] = pstGlobalVars->pfParticlesInitialCoordinates[3*idx+1];
				pstGlobalVars->pfParticlesCoordinates[3*idx+2] = pstGlobalVars->pfParticlesInitialCoordinates[3*idx+2];
				pstGlobalVars->pucParticlesType[idx] = ucParticleType1;
				pstGlobalVars->pfParticlesRadius[idx] = fRadius1;
				pstGlobalVars->pfParticlesDensity[idx] = pstGlobalVars->pfParticlesDensity[uiParticleIdx];
				pCDiscretisation->pucMembraneIdx[idx] = 1;
			}
		}
	}
}

void CInitSolver::vCleanMembrane(unsigned int uiParticleIdx, unsigned int uiNeighbourIndex)
{
	if (pCDiscretisation->boIsMembrane(uiParticleIdx))
	{
		unsigned char ucParticleType1 = pstGlobalVars->pucParticlesType[uiParticleIdx];
		unsigned char ucParticleType2 = pstGlobalVars->pucParticlesType[uiNeighbourIndex];
		if (ucParticleType1 == ucParticleType2)
		{
			// compute particle penetration
			float *pfCoord1 = pstGlobalVars->pfParticlesCoordinates + 3*uiParticleIdx;
			float *pfCoord2 = pstGlobalVars->pfParticlesCoordinates + 3*uiNeighbourIndex;
			float fRadius1 = pstGlobalVars->pfParticlesRadius[uiParticleIdx];
			float fd12 = VECT_ComputeDistance(pfCoord1, pfCoord2, uiDimensions);
			if (fd12 < fRadius1)
			{
				vMarkForDeletion(uiParticleIdx);
			}
		}
	}
}

unsigned int CGenericSolver::uiDeactivateParticle(unsigned int uiIdx)
{
	unsigned int idx = pCDiscretisation->uiDeactivateParticle(uiIdx);
	pstGlobalVars->puiActiveParticles[idx] = 0;
	pstGlobalVars->pucMarkedForDeletion[idx] = 0;
	return idx;
}

unsigned int CGenericSolver::uiActivateParticle(void)
{
	unsigned int idx = pCDiscretisation->uiActivateParticle();
	pstGlobalVars->puiActiveParticles[idx] = 1;
	return idx;
}


} // namespace

