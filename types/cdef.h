/****************************************************************************** 
 * 
 *  file:  cdef.h
 * 
 *  Copyright (c) 2016, Grand R. Joldes.
 *  All rights reverved.
 * 
 *  
 *  THE SOFTWARE IS PROVIDED _AS IS_, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *  DEALINGS IN THE SOFTWARE.  
 *  
 *****************************************************************************/ 

/* This file containes type definitions.
It is platform dependent */

#if !defined(_CDEF_H_)
#define _CDEF_H_

// signed integers:
typedef signed char int8;
typedef signed short int16;
typedef signed long int32;

// un-signed integers:
typedef unsigned char uint8;
typedef unsigned short uint16;
typedef unsigned long uint32;

// floating types
typedef float float4;
typedef double float8;

// definitions
//#define INLINE __inline
#define INLINE

#define ON 1
#define OFF 0

#define YES 1
#define NO 0

#define E_OK 0
#define E_ERROR 1

#endif
