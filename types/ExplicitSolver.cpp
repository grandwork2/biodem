/****************************************************************************** 
 * 
 *  file:  ExplicitSolver.cpp
 * 
 *  Copyright (c) 2016, Grand R. Joldes.
 *  All rights reverved.
 * 
 *  
 *  THE SOFTWARE IS PROVIDED _AS IS_, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *  DEALINGS IN THE SOFTWARE.  
 *  
 *****************************************************************************/ 
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <iostream>

#define _USE_MATH_DEFINES
#include <math.h>

#include <boost/bind.hpp>
#include <vtkUnsignedIntArray.h>

#include "ExplicitSolver.hpp"
#include "vect.hpp"
#include "fpcontrol.h"
#include "interactions.hpp"
#include "walls.hpp"

#define DR_NUM_DAMPING_PARAM_SAMPLES 10
#define DR_FORCE_DISP_UPDATE_INTERVAL 100
#define DR_DAMPING_PARAM_DEVIATION   0.01
#define DR_DAMPING_PARAM_DAMPING     0.8  //0.95
#define TERMINATION_DAMPING_PARAM_DEVIATION   0.01

namespace BioDEM {

CExplicitSolver::CExplicitSolver(void)
: CGenericSolver()
{
	enSolverType = SOLVER_enExplicit;
	pfLumpedMass = NULL;
	pCBuckets = NULL;
	pCBucketSearch = NULL;
	fInitialDampingParam = 0.99;
	boScaleMass = false;
	fConvergenceAbsoluteError = 0.01;
	uiConvergenceNumSteps = 50;
	pfSavedDisplacements = NULL;
	pfSavedForces = NULL;
	pfScaledMass = NULL;
	puiBucketIndex = NULL;
	boAdaptiveDamping = true;
	uiTimeStep = 0;
}

CExplicitSolver::~CExplicitSolver(void)
{
	if (pfScaledMass != NULL) delete[] pfScaledMass;
	if (pfLumpedMass != NULL) delete[] pfLumpedMass;
	if (pCBuckets != NULL) delete[] pCBuckets;
	if (pCBucketSearch != NULL) delete[] pCBucketSearch;
	if (pfSavedDisplacements != NULL) delete[] pfSavedDisplacements;
	if (pfSavedForces != NULL) delete[] pfSavedForces;
	if (puiBucketIndex != NULL) delete[] puiBucketIndex;

};

bool CExplicitSolver::boConfigure(std::string conf)
{
	int n = sscanf(conf.c_str(), "%f,%f", &fTimeStep, &fTotalTime);
	if (n!=2) 
	{
		std::cout << "Error: Invalid explicit solver configuration: " << conf << std::endl; 
		return false;
	}
	else
	{
		std::cout << "\t\tTime step: " << fTimeStep << std::endl;
		std::cout << "\t\tTotal time: " << fTotalTime << std::endl; 
	}
	uiTotalNumParticles = pCDiscretisation->uiGetTotalNumberOfParticles();
	uiTotalNumberDOF = 3*uiTotalNumParticles;
	pfLumpedMass = new float[uiTotalNumParticles];
	pfScaledMass = new float[uiTotalNumParticles];
	pfSavedDisplacements = new float[uiTotalNumberDOF];
	pfSavedForces = new float[uiTotalNumberDOF];
	return true;
};

void CExplicitSolver::vCopyParticle(unsigned int fromIdx, unsigned int toIdx)
{
	CGenericSolver::vCopyParticle(fromIdx, toIdx);

#define vCopyVector3(var) var[3*toIdx] = var[3*fromIdx];\
	var[3*toIdx+1] = var[3*fromIdx+1];\
	var[3*toIdx+2] = var[3*fromIdx+2]

#define vCopyScalar(var) var[toIdx] = var[fromIdx]
	
	vCopyScalar(pfLumpedMass);
	vCopyScalar(pfScaledMass);

	vCopyVector3(pfSavedDisplacements);
	vCopyVector3(pfSavedForces);
}

bool CExplicitSolver::boSetParameters(std::string conf, tenSolverType enSolverType)
{
	if (enSolverType != SOLVER_enExplicit)
	{
		std::cout << "Warning: invalid solver type! Configuration will be ignored." << fTimeStep << std::endl;
		return false;
	}
	float fDampingPar, fConvError;
	int iAdaptiveDamping, iScaleMass, iCheckConvergence, iConvNumSteps;

	int n = sscanf(conf.c_str(), "%f,%d,%d,%d,%f,%d", &fDampingPar, &iAdaptiveDamping, &iScaleMass, &iCheckConvergence, &fConvError, &iConvNumSteps);
	if (n!=6) 
	{
		std::cout << "Warning: invalid explicit solver configuration: " << conf << std::endl; 
		std::cout << "! Configuration will be ignored." << std::endl;
		return false;
	}
	if ((fDampingPar >= 0) && (fDampingPar <= 1))
	{
		fInitialDampingParam = fDampingPar;
	}
	else
	{
		std::cout << "Warning: convergence rate must be between 0 and 1! Using default." << std::endl;
	}
	std::cout << "\t\tConvergence rate: " << fInitialDampingParam << std::endl;
	boAdaptiveDamping = iAdaptiveDamping > 0;
	std::cout << "\t\tAdaptive convergence rate: ";
	if (boAdaptiveDamping) std::cout << "Yes" << std::endl;
	else std::cout << "No" << std::endl;
	boScaleMass = iScaleMass > 0;
	std::cout << "\t\tMass scaling: ";
	if (boScaleMass) std::cout << "Yes" << std::endl;
	else std::cout << "No" << std::endl;
	boCheckConvergence = iCheckConvergence > 0;
	std::cout << "\t\tCheck convergence: ";
	if (boCheckConvergence) std::cout << "Yes" << std::endl;
	else std::cout << "No" << std::endl;
	if (fConvError > 0)
	{
		fConvergenceAbsoluteError = fConvError;
	}
	else
	{
		std::cout << "Warning: convergence error must be > 0! Using default." << std::endl;
	}
	std::cout << "\t\tConvergence error: " << fConvergenceAbsoluteError << std::endl;
	if (iConvNumSteps > 0)
	{
		uiConvergenceNumSteps = (unsigned int)iConvNumSteps;
	}
	else
	{
		std::cout << "Warning: convergence number of steps must be > 0! Using default." << std::endl;
	}
	std::cout << "\t\tConvergence number of steps: " << iConvNumSteps << std::endl;
	return true;
}

void CExplicitSolver::vInitComputations(void)
{
	// Damping parameters
	unsigned int uiStepNo = pCWorkspace->uiGetCurrentStep()-1;
	if (fInitialDampingParam == 0) // get convergence rate from previous step
	{
		// try to get value from previous step
		fInitialDampingParam = pCWorkspace->pGetAnalysisStep(uiStepNo)->pGetSolver()->fGetDampingParam();
	}
	fOldDampingParam = fInitialDampingParam;
	fDampingParam = fInitialDampingParam;

	// Compute critical time step, as defined by the interractions and particle masses
	// This will be a conservative estimate but only valid for equal spheres.
	// For large difference in radius between particles it may not be conservative.
	CInteractions *pCInteractions = pCWorkspace->pGetInteractions();
	float fCriticalTimeStep = 0;
	unsigned int uiNumParticleTypes = pCWorkspace->pGetDiscretisation()->uiGetNumberOfParticleTypes();
	float fMaxMass = (float)VECT_f8MaxValue(pfLumpedMass, uiTotalNumParticles) +1;
	if (pCInteractions->boLinearForceActiveInStep(uiStepNo))
	{
		for (unsigned int ptype1 = 0; ptype1 < uiNumParticleTypes; ptype1++)
		{
			for (unsigned int ptype2 = ptype1; ptype2 < uiNumParticleTypes; ptype2++)
			{
				float k12 = pCInteractions->fLinearForceStiffness(uiStepNo, ptype1, ptype2); 
				if (k12 != 0)
				{
					// compute min. mass of particles of type 1 and 2
					float fMass1Min = fMaxMass; 
					float fMass2Min = fMaxMass; 
					for (unsigned int i = 0; i < uiTotalNumParticles; i++)
					{
						if (pstGlobalVars->pucParticlesType[i] == ptype1)
						{
							if (pfLumpedMass[i] < fMass1Min) fMass1Min = pfLumpedMass[i];
						}
						else if (pstGlobalVars->pucParticlesType[i] == ptype2)
						{
							if (pfLumpedMass[i] < fMass2Min) fMass2Min = pfLumpedMass[i];
						}
					}
					if (ptype1 == ptype2) fMass2Min = fMass1Min;
					if ((fMass1Min < fMaxMass) && (fMass2Min < fMaxMass))
					{
						float fMass = fMass1Min*fMass2Min/(fMass1Min+fMass2Min);
						float fTimeStep_ = sqrt(fMass/3.0f/k12);
						if (uiDimensions == 3) fTimeStep_ /= sqrt(2.0f);
						if (fCriticalTimeStep == 0) fCriticalTimeStep = fTimeStep_;
						else if (fCriticalTimeStep > fTimeStep_) fCriticalTimeStep = fTimeStep_;
					}
				}
			}
		}
	}
	if (fCriticalTimeStep == 0) fCriticalTimeStep = 1.1*fTimeStep;
	std::cout << "\tComputed critical time step: " << fCriticalTimeStep << std::endl;
	if (boScaleMass) fCriticalTimeStep = 1.1*fTimeStep;
	if (fCriticalTimeStep < fTimeStep)
	{
		fTimeStep = 0.9f*fCriticalTimeStep;
	}
	std::cout << "\tUsed time step: " << fTimeStep << std::endl;

	// init bucket search
	// configure the grid for bucket search
	float *pfDistance = pCInteractions->pfConstantForceDistance(uiStepNo);
	float fMaxAttractionDistance = (float)VECT_f8MaxValue(pfDistance, pCInteractions->uiGetInterractionMatrixSize());
	pfDistance = pCInteractions->pfLinearForceDistance(uiStepNo);
	float fMaxAttractionDistance1 = (float)VECT_f8MaxValue(pfDistance, pCInteractions->uiGetInterractionMatrixSize());
	if (fMaxAttractionDistance1 > fMaxAttractionDistance) fMaxAttractionDistance = fMaxAttractionDistance1;

	float fMaxParticleRadius = (float)VECT_f8MaxValue(pstGlobalVars->pfParticlesRadius, uiTotalNumParticles);
	fMaxParticleDisplacementBeforeBucketDistribution = 0; //fMaxParticleRadius;
	float fBucketSize = 2*fMaxParticleRadius + 2*fMaxParticleDisplacementBeforeBucketDistribution + fMaxAttractionDistance;
	float *afBoundingBox = pCDiscretisation->pfGetBoundingBox();
	uiNumBucketsX = (unsigned int)((afBoundingBox[1]-afBoundingBox[0])/fBucketSize) + 1;
	uiNumBucketsY = (unsigned int)((afBoundingBox[3]-afBoundingBox[2])/fBucketSize) + 1;
	uiNumBucketsZ = (unsigned int)((afBoundingBox[5]-afBoundingBox[4])/fBucketSize) + 1;
	std::cout << "\tBuckets: " << uiNumBucketsX << " x " << uiNumBucketsY << " x " << uiNumBucketsZ << std::endl;

	if ((uiDimensions == 2) && (uiNumBucketsZ > 1))
	{
		std::cout << "Warning: particles are not in the same plane for the configured 2D analysis!" << std::endl;
	}

	pCBuckets = new BucketSearch::buckets(afBoundingBox[0], afBoundingBox[2], afBoundingBox[4],
		fBucketSize, fBucketSize, fBucketSize, uiNumBucketsX, uiNumBucketsY, uiNumBucketsZ);

	pCBucketSearch = new BucketSearch::BucketSearch(pCBuckets, pstGlobalVars->pfParticlesCoordinates, uiTotalNumParticles);

	puiBucketIndex = new unsigned int[uiTotalNumParticles];
	VECT_vInit(puiBucketIndex, uiTotalNumParticles, (unsigned int)0);

	unsigned int uiNumActiveParticles = pCDiscretisation->uiGetNumberOfActiveParticles();
	pCBucketSearch->vDistributeActivePointsToBuckets(pCDiscretisation->pIdGetActiveParticleIndexes(), uiNumActiveParticles);

	if (pCDiscretisation->boHasMembrane()) 
	{
		// find membrane neighbours
		for (unsigned int i = 0; i < uiNumActiveParticles; i++)
		{
			unsigned int particleIdx = pCDiscretisation->idGetActiveParticleIndex(i);
			pCBucketSearch->vOperateOnNeighboursUsingMask(boost::bind(&BioDEM::CExplicitSolver::vFindMembraneNeighbours, this, _1, _2), uiDimensions, particleIdx);
		}

		// check that all membrane particles have at least 2 neighbours
		for (unsigned int i = 0; i < uiNumActiveParticles; i++)
		{
			unsigned int particleIdx = pCDiscretisation->idGetActiveParticleIndex(i);
			if (pCDiscretisation->boIsMembrane(particleIdx))
			{
				if (pCDiscretisation->pucMembraneNumNeighbours[particleIdx] < 2)
				{
					std::cout << "Warning: membrane seems not to be closed! Check your geometry initialisation." << std::endl;
				}
			}
		}

	}

	// growth
	pCWorkspace->vInitGrowth(pstGlobalVars->pfGrowthFactor, pstGlobalVars->pucParticlesType, uiTotalNumParticles, (unsigned int)(fTotalTime/fTimeStep));

	// Set up output variables
	pCWorkspace->vAddOutputVariable("DampingParamUsed");
	pCWorkspace->vAddOutputVariable("DampingParamCalculated");
	pCWorkspace->vAddOutputPointArray<vtkUnsignedIntArray>("Bucket", puiBucketIndex);
	pCWorkspace->vRemoveOutputLines();

	bool *boWallActive = pCWorkspace->pGetWalls()->pboGetActive();
	if (boWallActive[0]) pCWorkspace->vAddOutputVariable("FWXmin");
	if (boWallActive[1]) pCWorkspace->vAddOutputVariable("FWXmax");
	if (boWallActive[2]) pCWorkspace->vAddOutputVariable("FWYmin");
	if (boWallActive[3]) pCWorkspace->vAddOutputVariable("FWYmax");
	if (uiDimensions == 3)
	{
		if (boWallActive[4]) pCWorkspace->vAddOutputVariable("FWZmin");
		if (boWallActive[5]) pCWorkspace->vAddOutputVariable("FWZmax");
	}

	// termination
	uiTerminationCount = 0;
};

void CExplicitSolver::vFindMembraneNeighbours(unsigned int uiParticleIdx, unsigned int uiNeighbourIndex)
{
	if (pCDiscretisation->boIsMembrane(uiParticleIdx))
	{
		unsigned char ucParticleType1 = pstGlobalVars->pucParticlesType[uiParticleIdx];
		unsigned char ucParticleType2 = pstGlobalVars->pucParticlesType[uiNeighbourIndex];
		if (ucParticleType1 == ucParticleType2)
		{
			// compute particle penetration
			float *pfCoord1 = pstGlobalVars->pfParticlesCoordinates + 3*uiParticleIdx;
			float *pfCoord2 = pstGlobalVars->pfParticlesCoordinates + 3*uiNeighbourIndex;
			float fRadius1 = pstGlobalVars->pfMembraneParticleInterractionRadius[uiParticleIdx];
			float fRadius2 = pstGlobalVars->pfMembraneParticleInterractionRadius[uiNeighbourIndex];
			float fd12 = VECT_ComputeDistance(pfCoord1, pfCoord2, uiDimensions);
			float fParticleDistance = fd12 - fRadius1 - fRadius2;
			if (fParticleDistance < fRadius1/4)
			{
				// particles are neighbours
				pCDiscretisation->boAddUniqueMembraneNeighbour(uiParticleIdx, uiNeighbourIndex);
				pCDiscretisation->boAddUniqueMembraneNeighbour(uiNeighbourIndex, uiParticleIdx);
			}
		}
	}
}

void CExplicitSolver::vComputeInterractions(unsigned int uiParticleIdx, unsigned int uiNeighbourIndex)
{
	CInteractions *pCInteractions = pCWorkspace->pGetInteractions();
	unsigned int uiStepNo = pCWorkspace->uiGetCurrentStep()-1;
	unsigned char ucParticleType1 = pstGlobalVars->pucParticlesType[uiParticleIdx];
	unsigned char ucParticleType2 = pstGlobalVars->pucParticlesType[uiNeighbourIndex];
	// compute particle penetration
	float *pfCoord1 = pstGlobalVars->pfParticlesCoordinates + 3*uiParticleIdx;
	float *pfCoord2 = pstGlobalVars->pfParticlesCoordinates + 3*uiNeighbourIndex;
	float fRadius1 = pstGlobalVars->pfParticlesRadius[uiParticleIdx];
	float fRadius2 = pstGlobalVars->pfParticlesRadius[uiNeighbourIndex];
	float fd12 = VECT_ComputeDistance(pfCoord1, pfCoord2, uiDimensions);
	float fParticleDistance = fd12 - fRadius1 - fRadius2;
	float fNormalForces[3];
	fNormalForces[0] = 0; fNormalForces[1] = 0; fNormalForces[2] = 0;
	if ((!pCDiscretisation->boIsMembrane(uiParticleIdx)) || 
		(pCDiscretisation->pucMembraneIdx[uiParticleIdx] != pCDiscretisation->pucMembraneIdx[uiNeighbourIndex])) // membrane handled separately
	{
		if (pCInteractions->boConstantForceActiveInStep(uiStepNo))
		{
			// handle constant forces
			float fDist = pCInteractions->fConstantForceDistance(uiStepNo, ucParticleType1, ucParticleType2);
			if (fParticleDistance <= fDist)
			{
				float fForce = pCInteractions->fConstantForce(uiStepNo, ucParticleType1, ucParticleType2);
				if (fForce != 0)
				{
					// Compute components of the force
					float fRij = fRadius1*fRadius2/(fRadius1+fRadius2);
					for (unsigned int i = 0; i < uiDimensions; i++)
					{
						fNormalForces[i] += fForce*fRij*(pfCoord1[i] - pfCoord2[i])/fd12;
					}
				}
			}
		}
		if (pCInteractions->boLinearForceActiveInStep(uiStepNo))
		{
			// handle linear forces
			float fDist = pCInteractions->fLinearForceDistance(uiStepNo, ucParticleType1, ucParticleType2);
			if (fParticleDistance <= fDist)
			{
				float fK = pCInteractions->fLinearForceStiffness(uiStepNo, ucParticleType1, ucParticleType2)*
					pstGlobalVars->pfParticleIntegrity[uiParticleIdx]*pstGlobalVars->pfParticleIntegrity[uiNeighbourIndex];
				if (fK != 0)
				{
					// Compute components of the force
					float fRij = fRadius1*fRadius2/(fRadius1+fRadius2);
					for (unsigned int i = 0; i < uiDimensions; i++)
					{
						fNormalForces[i] += fK*fRij*fParticleDistance*(pfCoord1[i] - pfCoord2[i])/fd12;
					}
					if (boScaleMass) 
					{
						pfScaledMass[uiParticleIdx] += fK*fRij*fTimeStep*fTimeStep*1.21f/2.0f;
						if (pCWorkspace->boGetUniqueInteractions())
						{
							pfScaledMass[uiNeighbourIndex] += fK*fRij*fTimeStep*fTimeStep*1.21f/2.0f;
						}
					}
				}
			}
		}
	}
	// update global forces
	for (unsigned int i = 0; i < uiDimensions; i++)
	{
		pstGlobalVars->pfReactionForces[3*uiParticleIdx+i] += fNormalForces[i];
		if (pCWorkspace->boGetUniqueInteractions())
		{
			pstGlobalVars->pfReactionForces[3*uiNeighbourIndex+i] -= fNormalForces[i];
		}
	}


	// ***************************************************** //

	// Manage ECM erosion 
	if (fParticleDistance <= 0)
	{
		if (pCInteractions->pstGetParticleStiffnessDegradation()->boActive)
		{
			if (ucParticleType1 == pCInteractions->pstGetParticleStiffnessDegradation()->uiInitiator)
			{
				if (ucParticleType2 == pCInteractions->pstGetParticleStiffnessDegradation()->uiDegraded)
				{
					pstGlobalVars->pfParticleIntegrity[uiNeighbourIndex] += 
						pCInteractions->pstGetParticleStiffnessDegradation()->fDegradationFactor*fParticleDistance;
					if (pstGlobalVars->pfParticleIntegrity[uiNeighbourIndex] <= 0)
						vMarkForDeletion(uiNeighbourIndex);
				}
			}
			else if (pCWorkspace->boGetUniqueInteractions())
			{
				if (ucParticleType1 == pCInteractions->pstGetParticleStiffnessDegradation()->uiDegraded)
				{
					if (ucParticleType2 == pCInteractions->pstGetParticleStiffnessDegradation()->uiInitiator)
					{
						pstGlobalVars->pfParticleIntegrity[uiParticleIdx] += 
							pCInteractions->pstGetParticleStiffnessDegradation()->fDegradationFactor*fParticleDistance;
						if (pstGlobalVars->pfParticleIntegrity[uiParticleIdx] <= 0)
							vMarkForDeletion(uiParticleIdx);
					}
				}
			}
		}
		if (pCInteractions->pstGetParticleGrowth()->boActive)
		{
			// Manage particle generation
			if (ucParticleType1 == pCInteractions->pstGetParticleGrowth()->uiInitiator)
			{
				if (ucParticleType2 == pCInteractions->pstGetParticleGrowth()->uiGrown)
				{
					pstGlobalVars->pfInterractionGrowthFactor[uiNeighbourIndex] = pCInteractions->pstGetParticleGrowth()->fGrowFactor;
				}
			}
			else if (pCWorkspace->boGetUniqueInteractions())
			{
				if (ucParticleType1 == pCInteractions->pstGetParticleGrowth()->uiGrown)
				{
					if (ucParticleType2 == pCInteractions->pstGetParticleGrowth()->uiInitiator)
					{
						pstGlobalVars->pfInterractionGrowthFactor[uiParticleIdx] = pCInteractions->pstGetParticleGrowth()->fGrowFactor;
					}
				}
			}
		}
	}
}

void CExplicitSolver::vCheckWallsPenetration(void)
{
	unsigned int particleIdx;	
	VECT_vInit(afWallForces, 6, 0.0f);
	CWalls *pCWalls = pCWorkspace->pGetWalls();
	float *pfWallLocation = pCWalls->pfGetLocation();
	bool *boWallActive = pCWalls->pboGetActive();
	unsigned int uiNumActiveParticles = pCDiscretisation->uiGetNumberOfActiveParticles();
	for (unsigned int i = 0; i < uiNumActiveParticles; i++)
	{
		particleIdx = pCDiscretisation->idGetActiveParticleIndex(i);
		float *pfCoords = pstGlobalVars->pfParticlesInitialCoordinates + 3*particleIdx;
		float *pfDisp = pstGlobalVars->pfNewDisplacements + 3*particleIdx;
		float *pfReactionForces = pstGlobalVars->pfReactionForces + 3*particleIdx;
		float fParticleRadius = pstGlobalVars->pfParticlesRadius[particleIdx];
		float fPenetration;
		// handle wall
		if (boWallActive[0])
		{
			fPenetration = pfCoords[0] + pfDisp[0] - fParticleRadius -  pfWallLocation[0];
			if (fPenetration < 0) 
			{
				pfDisp[0] -= fPenetration;
				// compute wall force
				afWallForces[0] += pfReactionForces[0];
			}
		}
		if (boWallActive[1])
		{
			fPenetration = pfCoords[0] + pfDisp[0] + fParticleRadius -  pfWallLocation[1];
			if (fPenetration > 0) 
			{
				pfDisp[0] -= fPenetration;
				afWallForces[1] += pfReactionForces[0];
			}
		}
		if (boWallActive[2])
		{
			fPenetration = pfCoords[1] + pfDisp[1] - fParticleRadius -  pfWallLocation[2];
			if (fPenetration < 0) 
			{
				pfDisp[1] -= fPenetration;
				afWallForces[2] += pfReactionForces[1];
			}
		}
		if (boWallActive[3])
		{
			fPenetration = pfCoords[1] + pfDisp[1] + fParticleRadius -  pfWallLocation[3];
			if (fPenetration > 0) 
			{
				pfDisp[1] -= fPenetration;
				afWallForces[3] += pfReactionForces[1];
			}
		}
		if (uiDimensions == 3)
		{
			if (boWallActive[4])
			{
				fPenetration = pfCoords[2] + pfDisp[2] - fParticleRadius -  pfWallLocation[4];
				if (fPenetration < 0) 
				{
					pfDisp[2] -= fPenetration;
					afWallForces[4] += pfReactionForces[2];
				}
			}
			if (boWallActive[5])
			{
				fPenetration = pfCoords[2] + pfDisp[2] + fParticleRadius -  pfWallLocation[5];
				if (fPenetration > 0) 
				{
					pfDisp[2] -= fPenetration;
					afWallForces[5] += pfReactionForces[2];
				}
			}
		}
	}
	// Output wall forces
	if (boWallActive[0]) pCWorkspace->vSetOutputVariable("FWXmin", afWallForces[0]);
	if (boWallActive[1]) pCWorkspace->vSetOutputVariable("FWXmax", afWallForces[1]);
	if (boWallActive[2]) pCWorkspace->vSetOutputVariable("FWYmin", afWallForces[2]);
	if (boWallActive[3]) pCWorkspace->vSetOutputVariable("FWYmax", afWallForces[3]);
	if (uiDimensions == 3)
	{
		if (boWallActive[4]) pCWorkspace->vSetOutputVariable("FWZmin", afWallForces[4]);
		if (boWallActive[5]) pCWorkspace->vSetOutputVariable("FWZmax", afWallForces[5]);
	}
}

void CExplicitSolver::vComputeParticlesForces(void)
{
	unsigned int uiNumActiveParticles = pCDiscretisation->uiGetNumberOfActiveParticles();
	VECT_vAttribScalarToSubVector3(pstGlobalVars->pfReactionForces, pCDiscretisation->pIdGetActiveParticleIndexes(), uiNumActiveParticles, 0.0f);
	if (boScaleMass) 
	{
		VECT_vAttribScalarToSubVector(pfScaledMass, pCDiscretisation->pIdGetActiveParticleIndexes(), uiNumActiveParticles, 0.0f);
	}

	VECT_vAttribScalarToSubVector(pstGlobalVars->pfInterractionGrowthFactor, pCDiscretisation->pIdGetActiveParticleIndexes(), uiNumActiveParticles, 1.0f);

	unsigned int particleIdx;
	for (unsigned int i = 0; i < uiNumActiveParticles; i++)
	{
		particleIdx = pCDiscretisation->idGetActiveParticleIndex(i);
		float *pfCoords = pstGlobalVars->pfParticlesCoordinates + 3*particleIdx;
		// find bucket containing particle
		unsigned int uiBucketIdxX = pCBuckets->uiGetBucketIndexX(pfCoords[0]);
		unsigned int uiBucketIdxY = pCBuckets->uiGetBucketIndexY(pfCoords[1]);
		unsigned int uiBucketIdxZ = pCBuckets->uiGetBucketIndexZ(pfCoords[2]);
		puiBucketIndex[particleIdx] = (uiBucketIdxX+4*uiBucketIdxY+4*uiBucketIdxZ) % 7;
		if (pCWorkspace->boGetUniqueInteractions())
		{
			pCBucketSearch->vOperateOnNeighboursUsingMask(boost::bind(&BioDEM::CExplicitSolver::vComputeInterractions, this, _1, _2), uiDimensions, particleIdx);
		}
		else
		{
			pCBucketSearch->vOperateOnNeighbours(boost::bind(&BioDEM::CExplicitSolver::vComputeInterractions, this, _1, _2), uiDimensions, particleIdx);
		}
	}

	uiNumActiveParticles = uiDeleteMarkedParticles();

	if (pCDiscretisation->boHasMembrane()) 
	{
		// forces between walls and membranes
		CInteractions *pCInteractions = pCWorkspace->pGetInteractions();
		unsigned int uiStepNo = pCWorkspace->uiGetCurrentStep()-1;
#if (0)
		if (pCInteractions->boConstantForceActiveInStep(uiStepNo))
		{
			CWalls *pCWalls = pCWorkspace->pGetWalls();
			float *pfWallLocation = pCWalls->pfGetLocation();
			bool *boWallActive = pCWalls->pboGetActive();
			for (unsigned int i = 0; i < uiNumActiveParticles; i++)
			{
				particleIdx = pCDiscretisation->idGetActiveParticleIndex(i);
				if (pCDiscretisation->boIsMembrane(particleIdx))
				{
					unsigned char ucParticleType = pstGlobalVars->pucParticlesType[particleIdx];
					float fForce = pCInteractions->fConstantForce(uiStepNo, ucParticleType, 0);
					if (fForce != 0)
					{
						// check penetration
						float *pfCoords = pstGlobalVars->pfParticlesCoordinates + 3*particleIdx;
						float fParticleRadius = pstGlobalVars->pfParticlesRadius[particleIdx];
						float fPenetration;
						// I only consider the YMin wall for now
						if (boWallActive[2])
						{
							fPenetration = pfCoords[1] - fParticleRadius -  pfWallLocation[2];
							float fDist = pCInteractions->fConstantForceDistance(uiStepNo, ucParticleType, 0);
							if (fPenetration < fDist) 
							{
								// add constant force
								float *pfReactionForces = pstGlobalVars->pfReactionForces + 3*particleIdx;
								pfReactionForces[1] += fForce;
							}
						}
					}
				}
			}
		}
#endif
		// compute membrane forces
		for (unsigned int i = 0; i < uiNumActiveParticles; i++)
		{
			particleIdx = pCDiscretisation->idGetActiveParticleIndex(i);
			if (pCDiscretisation->boIsMembrane(particleIdx))
			{
				unsigned char ucParticleType = pstGlobalVars->pucParticlesType[particleIdx];
				float fForce = pCInteractions->fConstantForce(uiStepNo, ucParticleType, ucParticleType);
				float fK = pCInteractions->fLinearForceStiffness(uiStepNo, ucParticleType, ucParticleType);
				float fInteractionRadius1 = pstGlobalVars->pfMembraneParticleInterractionRadius[particleIdx];
				float *pfCoord1 = pstGlobalVars->pfParticlesCoordinates + 3*particleIdx;
				float fRadius1 = pstGlobalVars->pfParticlesRadius[particleIdx];
				unsigned char ucNumNeighbours = pCDiscretisation->pucMembraneNumNeighbours[particleIdx];
				float fNormalForces[3];
				fNormalForces[0] = 0; fNormalForces[1] = 0; fNormalForces[2] = 0;
				float fMembraneParticlePenetrationSum = 0;
				for (unsigned char n = 0; n < ucNumNeighbours; n++)
				{
					unsigned int uiNeighbourIndex = pCDiscretisation->puiMembraneNeigbours[particleIdx*pCDiscretisation->ucMaxMembraneNeighbours+n];
					//if (particleIdx >= uiNeighbourIndex) continue; // unique interractions
					float fInteractionRadius2 = pstGlobalVars->pfMembraneParticleInterractionRadius[uiNeighbourIndex];
					float *pfCoord2 = pstGlobalVars->pfParticlesCoordinates + 3*uiNeighbourIndex;
					float fd12 = VECT_ComputeDistance(pfCoord1, pfCoord2, uiDimensions);
					float fParticleDistance = fd12 - fInteractionRadius1 - fInteractionRadius2;
					if (fParticleDistance < 0) fParticleDistance = 0;
					// Compute components of the force
					for (unsigned int i = 0; i < uiDimensions; i++)
					{
						fNormalForces[i] += (fForce+fK*fParticleDistance)*fRadius1/2*(pfCoord1[i] - pfCoord2[i])/fd12;
					}
					// compute penetration sum
					fMembraneParticlePenetrationSum+=fParticleDistance;
					// scale mass
					if (boScaleMass) 
					{
						pfScaledMass[particleIdx] += fK*fRadius1/2*fTimeStep*fTimeStep*1.21f/2.0f;
					}
					//if (particleIdx < uiNeighbourIndex)
					{
					//	pstGlobalVars->pfMembraneParticleInterractionRadius[particleIdx] += (fInteractionRadius2-fInteractionRadius1)/2;
					//	pstGlobalVars->pfMembraneParticleInterractionRadius[uiNeighbourIndex] -= (fInteractionRadius2-fInteractionRadius1)/2;
					}
				}
				// update global forces
				for (unsigned int i = 0; i < uiDimensions; i++)
				{
					pstGlobalVars->pfReactionForces[3*particleIdx+i] += fNormalForces[i];
				}
				// change interraction radius
				float fCorrection;
				if (pCDiscretisation->boGrowMembrane())
				{
					fCorrection = fMembraneParticlePenetrationSum/2;
					float fMaxDisp = pCDiscretisation->fGetMinInputParticleRadius()/4;
					if (VECT_Abs(fCorrection) > fMaxDisp)
					{
						fCorrection = fMaxDisp*fCorrection/VECT_Abs(fCorrection);
					}
					fInteractionRadius1 += fCorrection;
					if (fInteractionRadius1 > pstGlobalVars->pfParticlesRadius[particleIdx]/2)
					{
						pstGlobalVars->pfMembraneParticleInterractionRadius[particleIdx] = fInteractionRadius1;
					}	
				}
			}
		}
		// insert membrane particles
		if (pCDiscretisation->boGrowMembrane())
		{
			for (unsigned int i = 0; i < uiNumActiveParticles; i++)
			{
				particleIdx = pCDiscretisation->idGetActiveParticleIndex(i);
				if (pCDiscretisation->boIsMembrane(particleIdx))
				{
					float *pfCoord1 = pstGlobalVars->pfParticlesCoordinates + 3*particleIdx;
					unsigned char ucNumNeighbours = pCDiscretisation->pucMembraneNumNeighbours[particleIdx];
					for (unsigned char n = 0; n < ucNumNeighbours; n++)
					{
						unsigned int uiNeighbourIndex = pCDiscretisation->puiMembraneNeigbours[particleIdx*pCDiscretisation->ucMaxMembraneNeighbours+n];
						if (particleIdx < uiNeighbourIndex) // unique insertions
						{
							float *pfCoord2 = pstGlobalVars->pfParticlesCoordinates + 3*uiNeighbourIndex;
							float fd12 = VECT_ComputeDistance(pfCoord1, pfCoord2, uiDimensions);
							// insert particles
							if (fd12 > 2*pstGlobalVars->pfParticlesRadius[particleIdx])
							{
								float pfSplittingDir[3];
								// set splitting direction
								VECT_vSubstract(pfCoord2, pfCoord1, pfSplittingDir, uiDimensions); 
								float fR = pstGlobalVars->pfMembraneParticleInterractionRadius[particleIdx]/2;
								pstGlobalVars->pfMembraneParticleInterractionRadius[particleIdx] = fR;
								// insert particle
								unsigned int idx = uiActivateParticle();
								vCopyParticle(particleIdx, idx);
								// set position
								float fNorm = sqrt(VECT_f8ScalarProduct(pfSplittingDir, pfSplittingDir, uiDimensions));
								float *pfInitialCoord1 = pstGlobalVars->pfParticlesInitialCoordinates + 3*particleIdx;
								float *pfInitialCoord3 = pstGlobalVars->pfParticlesInitialCoordinates + 3*idx;
								float *pfCoord3 = pstGlobalVars->pfParticlesCoordinates + 3*idx;
								for (unsigned int j = 0; j < uiDimensions; j++)
								{
									pfInitialCoord3[j] += fR*pfSplittingDir[j]/fNorm;
									pfCoord3[j] += fR*pfSplittingDir[j]/fNorm;
									pfInitialCoord1[j] -= fR*pfSplittingDir[j]/fNorm;
									pfCoord1[j] -= fR*pfSplittingDir[j]/fNorm;
								}
								
								// mark it as membrane
								pCDiscretisation->pucMembraneIdx[idx] = pCDiscretisation->pucMembraneIdx[particleIdx];
								// change neighbours
								pCDiscretisation->puiMembraneNeigbours[particleIdx*pCDiscretisation->ucMaxMembraneNeighbours+n] = idx;
								unsigned char ucNumNeighbours2 = pCDiscretisation->pucMembraneNumNeighbours[uiNeighbourIndex];
								for (unsigned char k = 0; k < ucNumNeighbours2; k++)
								{
									if (pCDiscretisation->puiMembraneNeigbours[uiNeighbourIndex*pCDiscretisation->ucMaxMembraneNeighbours+k] == particleIdx)
									{
										pCDiscretisation->puiMembraneNeigbours[uiNeighbourIndex*pCDiscretisation->ucMaxMembraneNeighbours+k] = idx;
										break;
									}
								}
								// set neighbours for the inserted particle
								if (uiDimensions == 2)
								{
									pCDiscretisation->pucMembraneNumNeighbours[idx] = 2; 
									pCDiscretisation->puiMembraneNeigbours[idx*pCDiscretisation->ucMaxMembraneNeighbours] = particleIdx;
									pCDiscretisation->puiMembraneNeigbours[idx*pCDiscretisation->ucMaxMembraneNeighbours+1] = uiNeighbourIndex;
								}
								else
								{
									//????????????????????? 3D ??????????
									std::cout << "Error: membrane not implemented in 3D!" << std::endl;
									exit(1);
								}
							}
						}
					}
				}
			}
		}
	}
		
	uiNumActiveParticles = pCDiscretisation->uiGetNumberOfActiveParticles();
	for (unsigned int i = 0; i < uiNumActiveParticles; i++)
	{
		particleIdx = pCDiscretisation->idGetActiveParticleIndex(i);
		// check mass
		if (boScaleMass)
		{
			if (pfScaledMass[particleIdx] == 0) pfScaledMass[particleIdx] = pfLumpedMass[particleIdx];
		}
		////////// Add gravity
		//pstGlobalVars->pfReactionForces[3*particleIdx+1] += 0.00001;
	}
} 

void CExplicitSolver::vChangeDampingParameters(void)
{
	/* Estimate the lower oscilation frequency */
	float *pfMass;
	float *pfDisp;
	float *pfSavedDisp;
	float *pfForces;
	float *pfSavedF;

	double dKsum, dMsum, dx, dKii;
	float fDampingPar = 0;
	dKsum = 0;
	dMsum = 0;
	unsigned int uiNumActiveParticles = pCDiscretisation->uiGetNumberOfActiveParticles();
	for (unsigned int i = 0; i < uiNumActiveParticles; i++)
	{
		unsigned int particleIdx = pCDiscretisation->idGetActiveParticleIndex(i);
		pfMass = pfScaledMass + particleIdx;
		pfDisp = pstGlobalVars->pfCurrentDisplacements + 3*particleIdx;
		pfSavedDisp = pfSavedDisplacements + 3*particleIdx;
		pfForces = pstGlobalVars->pfReactionForces + 3*particleIdx;
		pfSavedF = pfSavedForces + 3*particleIdx;
		for (unsigned int j = 0; j < 3; j++)
		{
			dKsum += (*pfDisp - *pfSavedDisp)*(*pfForces - *pfSavedF);
			dMsum += (*pfDisp - *pfSavedDisp)*(*pfDisp - *pfSavedDisp)*(*pfMass);
			pfDisp++; pfSavedDisp++; pfForces++; pfSavedF++;
		}
	}
	dKsum = VECT_Abs(dKsum);

	if ((dMsum > 10e-20) && (dKsum > 10e-20))
	{
		// minimum frequency
		dx = sqrt(dKsum/dMsum);
		// Kmat condition number (square root)
		dKii = 2.0/(dx*fTimeStep);
		// Convergence rate
		fDampingPar = (float)((dKii - 1.)/(dKii + 1.));
		if (VECT_Abs(fDampingPar - fOldDampingParam) < DR_DAMPING_PARAM_DEVIATION) // check if stabilized
		{
			uiNumDampParamSamples++;
			if (uiNumDampParamSamples >= DR_NUM_DAMPING_PARAM_SAMPLES)
			{
				if (boSavedForceDisplacementUpdated)
				{
					boSavedForceDisplacementUpdated = false;
					if (VECT_Abs(fDampingPar - fDampingParam) < TERMINATION_DAMPING_PARAM_DEVIATION)
						boDampingParamStabilized = true;
				}
				fDampingParam = DR_DAMPING_PARAM_DAMPING*fDampingParam + (1-DR_DAMPING_PARAM_DAMPING)*fDampingPar;
			}
		}
		else uiNumDampParamSamples = 0;
		fOldDampingParam = fDampingPar;
	}
	else boDampingParamStabilized = true;
	if ((uiTimeStep % DR_FORCE_DISP_UPDATE_INTERVAL) == 0) 
	{
		VECT_vCopySubVector3(pstGlobalVars->pfCurrentDisplacements, pfSavedDisplacements, 
			pCDiscretisation->pIdGetActiveParticleIndexes(), uiNumActiveParticles);
		VECT_vCopySubVector3(pstGlobalVars->pfReactionForces, pfSavedForces, 
			pCDiscretisation->pIdGetActiveParticleIndexes(), uiNumActiveParticles);
		boSavedForceDisplacementUpdated = true;
	}
	pCWorkspace->vSetOutputVariable("DampingParamUsed", fDampingParam);
	pCWorkspace->vSetOutputVariable("DampingParamCalculated", fDampingPar);
}

void CExplicitSolver::vDoTimeStepping(void)
{
	uiTimeStep = 0;
	float fTime = 0;
	unsigned int uiTerminationCount = 0;
	unsigned int uiNumActiveParticles;
	// Output initial geometry
	pCWorkspace->vOutputResults(0,0);
	// Initialize displacements for 0 velocity
	//vInitDisplacements();
	if (boAdaptiveDamping)
	{
		uiNumDampParamSamples = 0;
		boDampingParamStabilized = false;
		boSavedForceDisplacementUpdated = false;
		uiNumActiveParticles = pCDiscretisation->uiGetNumberOfActiveParticles();
		VECT_vCopySubVector3(pstGlobalVars->pfCurrentDisplacements, pfSavedDisplacements, pCDiscretisation->pIdGetActiveParticleIndexes(), uiNumActiveParticles);
		VECT_vCopySubVector3(pstGlobalVars->pfReactionForces, pfSavedForces, pCDiscretisation->pIdGetActiveParticleIndexes(), uiNumActiveParticles);
	}
	pCWorkspace->vInitBC(pstGlobalVars->pfCurrentDisplacements, uiTotalNumberDOF);
	while (fTime < fTotalTime)
	{
		uiTimeStep++;
		//if (uiTimeStep > 20) break;
		fTime += fTimeStep;
		if (fTime > fTotalTime) fTime = fTotalTime;
		// Compute reaction forces
		vComputeParticlesForces();
		/* Compute new displacements based on forces */
        vComputeNewDisplacements();
		/* Apply boundary conditions for next time step */
		pCWorkspace->vMoveWalls(fTime/fTotalTime, afWallMotion);
		// Manage walls
		vCheckWallsPenetration();
		// Apply BC
		uiNumActiveParticles = pCDiscretisation->uiGetNumberOfActiveParticles();
		pCWorkspace->vApplyBC(pstGlobalVars->pfNewDisplacements, pCDiscretisation->pIdGetActiveParticleIndexes(), uiNumActiveParticles, fTime/fTotalTime); 

		// Adaptive convergence rate
		if (boAdaptiveDamping) vChangeDampingParameters();

		/* Update displacements */
        VECT_vCopySubVector3(pstGlobalVars->pfCurrentDisplacements, pstGlobalVars->pfOldDisplacements,  
			pCDiscretisation->pIdGetActiveParticleIndexes(), uiNumActiveParticles);
        VECT_vCopySubVector3(pstGlobalVars->pfNewDisplacements, pstGlobalVars->pfCurrentDisplacements,  
			pCDiscretisation->pIdGetActiveParticleIndexes(), uiNumActiveParticles);

		// Update particle positions
		VECT_vAddSubVector3(pstGlobalVars->pfParticlesInitialCoordinates, pstGlobalVars->pfNewDisplacements, 
			pstGlobalVars->pfParticlesCoordinates, pCDiscretisation->pIdGetActiveParticleIndexes(), uiNumActiveParticles);

		// Manage growth
		vGrowParticles();
		uiNumActiveParticles = pCDiscretisation->uiGetNumberOfActiveParticles();

		// output results
		pCWorkspace->vOutputResults(uiStartIteration + uiTimeStep, fStartTime+fTime);

		// Update buckets
		pCBucketSearch->vDistributeActivePointsToBuckets(pCDiscretisation->pIdGetActiveParticleIndexes(), uiNumActiveParticles);
		if (boFinishAnalysis()) break;
	}
	pCWorkspace->vUpdateWallLocation(afWallMotion);
	fEndTime = fStartTime + fTime;
	uiEndIteration = uiStartIteration + uiTimeStep;
	std::cout << "\tFinished in " << uiTimeStep << " iterations." << std::endl;
};

bool CExplicitSolver::boFinishAnalysis(void)
{
	// check termination criteria
	if (boAdaptiveDamping && boCheckConvergence)
	{
		unsigned int uiNumActiveParticles = pCDiscretisation->uiGetNumberOfActiveParticles();
		// compute displacement variation norm
		VECT_vSubstractSubVector3(pstGlobalVars->pfNewDisplacements, pstGlobalVars->pfOldDisplacements, 
			pstGlobalVars->pfNewDisplacements, pCDiscretisation->pIdGetActiveParticleIndexes(), uiNumActiveParticles);
		float fMaxDispVar = VECT_fNormInfSubVector3(pstGlobalVars->pfNewDisplacements, 
			pCDiscretisation->pIdGetActiveParticleIndexes(), uiNumActiveParticles);
		float fEstimatedError = fMaxDispVar*fDampingParam/(1-fDampingParam);
		if (boDampingParamStabilized)
		{
			if (fEstimatedError < fConvergenceAbsoluteError)
			{
				uiTerminationCount += 1;
				if (uiTerminationCount >= uiConvergenceNumSteps) return true;
			}
			else
				uiTerminationCount = 0;
		}
	}
	return false;
}

void CExplicitSolver::vComputeNewDisplacements(void)
{
	float fTimeStep2 = fTimeStep*fTimeStep;
    float *pfF, *pfNewU, *pfCurrentU, *pfOldU, *pfMass;
	unsigned int uiNumActiveParticles = pCDiscretisation->uiGetNumberOfActiveParticles();
	float b = 1+fDampingParam;
	for (unsigned int i = 0; i < uiNumActiveParticles; i++)
	{
		unsigned int particleIdx = pCDiscretisation->idGetActiveParticleIndex(i);
	  	pfF = pstGlobalVars->pfReactionForces + 3*particleIdx;
		pfNewU = pstGlobalVars->pfNewDisplacements + 3*particleIdx;
		pfCurrentU = pstGlobalVars->pfCurrentDisplacements + 3*particleIdx;
		pfOldU = pstGlobalVars->pfOldDisplacements + 3*particleIdx;
		pfMass = pfScaledMass + particleIdx;
		*pfNewU = b*(*pfCurrentU)- (b-1)*(*pfOldU) -b/2*fTimeStep2*(*pfF)/(*pfMass);
		pfNewU[1] = b*(pfCurrentU[1])- (b-1)*pfOldU[1] -b/2*fTimeStep2*(pfF[1])/(*pfMass);
		if (uiDimensions > 2)
		{
			pfNewU[2] = b*(pfCurrentU[2])- (b-1)*pfOldU[2] -b/2*fTimeStep2*(pfF[2])/(*pfMass);
		}
		// limit displacement for membrane particles
		//if (pCDiscretisation->boIsMembrane(particleIdx))
		{
			float fMaxDisp = pCDiscretisation->fGetMinInputParticleRadius()/10;
			float fu = pfNewU[0];
			if (VECT_Abs(fu - pfCurrentU[0]) > fMaxDisp) pfNewU[0] = pfCurrentU[0] + fMaxDisp*(fu - pfCurrentU[0])/VECT_Abs(fu - pfCurrentU[0]);
			fu = pfNewU[1];
			if (VECT_Abs(fu - pfCurrentU[1]) > fMaxDisp) pfNewU[1] = pfCurrentU[1] + fMaxDisp*(fu - pfCurrentU[1])/VECT_Abs(fu - pfCurrentU[1]);
			fu = pfNewU[2];
			if (VECT_Abs(fu - pfCurrentU[2]) > fMaxDisp) pfNewU[2] = pfCurrentU[2] + fMaxDisp*(fu - pfCurrentU[2])/VECT_Abs(fu - pfCurrentU[2]);
		}
	}
}

void CExplicitSolver::vInitDisplacements(void)
{
	// Compute reaction forces
	vComputeParticlesForces();
	float fTimeStep2 = fTimeStep*fTimeStep;
    float *pfF, *pfCurrentU, *pfOldU, *pfMass;
	unsigned int uiNumActiveParticles = pCDiscretisation->uiGetNumberOfActiveParticles();
	for (unsigned int i = 0; i < uiNumActiveParticles; i++)
	{
		unsigned int particleIdx = pCDiscretisation->idGetActiveParticleIndex(i);
		pfF = pstGlobalVars->pfReactionForces + 3*particleIdx;
		pfCurrentU = pstGlobalVars->pfCurrentDisplacements + 3*particleIdx;
		pfOldU = pstGlobalVars->pfOldDisplacements + 3*particleIdx;
		pfMass = pfScaledMass + particleIdx;
		*pfOldU = *pfCurrentU + fTimeStep2*(*pfF)/(*pfMass)/2;
		pfF++; pfCurrentU++; pfOldU++;
		*pfOldU = *pfCurrentU + fTimeStep2*(*pfF)/(*pfMass)/2;
		if (uiDimensions > 2)
		{
			pfF++; pfCurrentU++; pfOldU++;
			*pfOldU = *pfCurrentU + fTimeStep2*(*pfF)/(*pfMass)/2;
		}
	}
}

bool CExplicitSolver::boPerformAnalysis(void)
{
	std::cout << "\tPerforming explicit analysis ..." << std::endl;
	//FPCONTROL_vPrepareForComputations();
	__try
	{
		vComputeLumpedMassMatrix();
		vInitComputations();
		vDoTimeStepping();
	}
	__except ( _fpieee_flt( GetExceptionCode(),
                GetExceptionInformation(),
                FPCONTROL_iExceptionHandler ) )
	{
		// code that gets control 
		// if fpieee_handler returns
		// EXCEPTION_EXECUTE_HANDLER goes here
		// Release RAM
		std::cout << "Solution became unstable! Try reducing the step size." << std::endl;
		return false;
	}
	return true;
};

void CExplicitSolver::vComputeLumpedMassMatrix(void)
{
	VECT_vMultiply(pstGlobalVars->pfParticlesDensity, pstGlobalVars->pfParticlesRadius, pfLumpedMass, uiTotalNumParticles);
	VECT_vMultiply(pstGlobalVars->pfParticlesRadius, pfLumpedMass, pfLumpedMass, uiTotalNumParticles);
	VECT_vMultiplyScalar(pfLumpedMass, (float)M_PI, pfLumpedMass, uiTotalNumParticles);
	if (pCWorkspace->uiGetNumDimensions() == 3)
	{
		VECT_vMultiplyScalar(pfLumpedMass, (float)(4.0/3.0), pfLumpedMass, uiTotalNumParticles);
		VECT_vMultiply(pstGlobalVars->pfParticlesRadius, pfLumpedMass, pfLumpedMass, uiTotalNumParticles);
	}
	VECT_vCopy(pfLumpedMass, pfScaledMass, uiTotalNumParticles);
};

void CExplicitSolver::vGrowParticles(void)
{
	if (pCWorkspace->boGrowthActive() || pCWorkspace->pGetInteractions()->pstGetParticleGrowth()->boActive)
	{
		unsigned int uiNumActiveParticles = pCDiscretisation->uiGetNumberOfActiveParticles();
		for (unsigned int i = 0; i < uiNumActiveParticles; i++)
		{
			unsigned int particleIdx = pCDiscretisation->idGetActiveParticleIndex(i);
			float fGrowthFactor = pstGlobalVars->pfGrowthFactor[particleIdx]*pstGlobalVars->pfInterractionGrowthFactor[particleIdx];
			if (fGrowthFactor > 1)
			{
				pstGlobalVars->pfParticlesRadius[particleIdx] *= fGrowthFactor;
				if (pstGlobalVars->pfParticlesRadius[particleIdx] > pCDiscretisation->fGetMaxInputParticleRadius())
				{
					// split particle into 3(2D)/4(3D)
					float fR;
					unsigned int idx, p;
					float *pfInitialCoord;
					float *pfCoord;
					if (uiDimensions == 2)
					{
						// 2D
						fR = pstGlobalVars->pfParticlesRadius[particleIdx]/sqrt(3.0f);
						const float afDirections[3][2] = {{1, 0}, {-0.5, 0.866025404}, {-0.5, -0.866025404}};
						// split particle into 3
						for (p = 0; p < 3; p++)
						{
							idx = uiActivateParticle();
							vCopyParticle(particleIdx, idx);
							pstGlobalVars->pfParticlesRadius[idx] = fR;
							// correct position
							pfInitialCoord = pstGlobalVars->pfParticlesInitialCoordinates + 3*idx;
							pfCoord = pstGlobalVars->pfParticlesCoordinates + 3*idx;
							for (unsigned int j = 0; j < uiDimensions; j++)
							{
								pfInitialCoord[j] += fR*afDirections[p][j];
								pfCoord[j] += fR*afDirections[p][j];
							}
						}
					}
					else
					{
						// 3D
						fR = pstGlobalVars->pfParticlesRadius[particleIdx]/pow(4.0f, 1.0f/3.0f);
						const float afDirections[4][3] = {{0.942809042, 0, -0.33333333}, 
							{-0.471404521, 0.816496581, -0.33333333}, 
							{-0.471404521, -0.816496581, -0.33333333},
							{0, 0, 1}};
						// split particle into 4
						for (p = 0; p < 4; p++)
						{
							idx = uiActivateParticle();
							vCopyParticle(particleIdx, idx);
							pstGlobalVars->pfParticlesRadius[idx] = fR;
							// correct position
							pfInitialCoord = pstGlobalVars->pfParticlesInitialCoordinates + 3*idx;
							pfCoord = pstGlobalVars->pfParticlesCoordinates + 3*idx;
							for (unsigned int j = 0; j < uiDimensions; j++)
							{
								pfInitialCoord[j] += fR*afDirections[p][j];
								pfCoord[j] += fR*afDirections[p][j];
							}
						}
					}
					uiDeactivateParticle(i);
				}
			}
		}
	}
}

} // namespace

