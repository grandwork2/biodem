/****************************************************************************** 
 * 
 *  file:  workspace.cpp
 * 
 *  Copyright (c) 2016, Grand R. Joldes.
 *  All rights reverved.
 * 
 *  
 *  THE SOFTWARE IS PROVIDED _AS IS_, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *  DEALINGS IN THE SOFTWARE.  
 *  
 *****************************************************************************/ 
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <iostream>
#include <sstream>
#include <algorithm>
#include <boost/timer/timer.hpp>

#include <vtkSmartPointer.h>
#include <vtkFloatArray.h>
#include <vtkPolyData.h>
#include <vtkPointData.h>
#include <vtkPoints.h>
#include <vtkLine.h>
#include <vtkPolyDataWriter.h>
#include <vtkGeometryFilter.h>
#include <vtkInformation.h>

#include "workspace.hpp"
#include "GenericSolver.hpp"

namespace BioDEM {

CWorkspace::CWorkspace() 
{
	pOutputPolyData = pOutputPolyData->New();
	pOutputWalls = pOutputWalls->New();
	pOutputGlobals = pOutputGlobals->New();
	pOutputPolyDataCopy = NULL;
	pOutputGlobalsCopy = NULL;
	// prepare globals for output - a point and a vertex
	vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
	points->SetNumberOfPoints(1);
	points->SetPoint(0, 0, 0, 0);
	pOutputGlobals->SetPoints(points);
	boUniqueInteractions = true;
	pucBCMarkDOF = NULL;
	pfBCAppliedDisplacementValue = NULL;
	pfBCSavedDisplacements = NULL;
	boGrowthActive_ = false;
}

CWorkspace::~CWorkspace() 
{
	pOutputPolyData->Delete();
	pOutputWalls->Delete();
	if (pOutputPolyDataCopy != NULL) pOutputPolyDataCopy->Delete();
	if (pOutputGlobalsCopy != NULL) pOutputGlobalsCopy->Delete();
	pOutputGlobals->Delete();
	if (pucBCMarkDOF != NULL) delete[] pucBCMarkDOF;
	if (pfBCAppliedDisplacementValue != NULL) delete[] pfBCAppliedDisplacementValue;
	if (pfBCSavedDisplacements != NULL) delete[] pfBCSavedDisplacements;
}

void CWorkspace::vResetOutputGlobals(void)
{
	pOutputGlobals->GetPointData()->Initialize();
}

void CWorkspace::vAddOutputVariable(const char *pcName)
{
	vtkSmartPointer<vtkFloatArray> floatArray = vtkSmartPointer<vtkFloatArray>::New();
	floatArray->SetNumberOfComponents(1);
	floatArray->SetNumberOfTuples(1);
	floatArray->SetName(pcName);
	floatArray->SetValue(0, 0.0f);
	pOutputGlobals->GetPointData()->AddArray(floatArray);
}

void CWorkspace::vSetOutputVariable(const char *pcName, float fValue)
{
	vtkFloatArray* floatArray = vtkFloatArray::SafeDownCast(pOutputGlobals->GetPointData()->GetArray(pcName));
	floatArray->SetValue(0, fValue);
}

bool CWorkspace::boConfigure(program_options *pPO)
{
	uiDimensions = pPO->uiGetNumDimensions();
	tenSolverType enSolverType = pPO->enGetSolverType();
	std::cout << "\tDimensions: " << uiDimensions << std::endl;
	std::cout << "\tSolver: " << pPO->GetSolverName(enSolverType) << std::endl;
	uiNumberOfThreads = pPO->uiGetNumberOfThreads();
	uiGPU_id = pPO->uiGetGPU();
	if (enSolverType == enMT) std::cout << "\t\tNumber of threads: " << uiNumberOfThreads << std::endl;
	else if (enSolverType == enGPU) std::cout << "\t\tGPU used: " << uiGPU_id << std::endl;
	fRadiusDispersion = pPO->fGetRadiusDispersion();
	if ((fRadiusDispersion < 0) || (fRadiusDispersion > 1))
	{
		std::cout << "Warning: Radius dispersion must be between 0 and 1! Will use default." << std::endl;
		fRadiusDispersion = 0.2;
	}
	std::cout << "\t\tRadius dispersion: " << fRadiusDispersion << std::endl;
	if (!discretisation.boConfigure(pPO)) return false;
	if (!walls.boConfigure(pPO)) return false;
	walls.vInitOutputWalls(pOutputWalls);
	boUniqueInteractions = pPO->boUniqueInteractions();

	// geometry changes
	const std::vector<std::string> *aChangeParticleTypeConfigs = pPO->GetChangeParticleTypeInSphere();
	if (aChangeParticleTypeConfigs != NULL)
	{
		std::cout << "\tChanges of partycle type inside sphere:" << std::endl;
		for (unsigned int i = 0; i < aChangeParticleTypeConfigs->size(); i++)
		{
			std::string config = aChangeParticleTypeConfigs->at(i);
			float afCenter[3];
			int iType;
			int iOldType;
			float fRadius;
			int n = sscanf(config.c_str(), "%f,%f,%f,%f,%d,%d", &afCenter[0], &afCenter[1], &afCenter[2], &fRadius, &iOldType, &iType);
			if (n!=6) 
			{
				std::cout << "Error: Invalid change particle type in sphere configuration: " << config << "!"<< std::endl; 
				return false;
			}
			else if ((iType < 0) || (iType > 255))
			{
				std::cout << "Error: Invalid particle type value. Must be greater than 0 and lower than 256!" << std::endl; 
				return false;
			}
			else
			{
				std::cout << "\t\tCenter: " << afCenter[0] << ", " << afCenter[1] << ", " << afCenter[2] << std::endl;
				std::cout << "\t\tRadius: " << fRadius << std::endl;
				std::cout << "\t\tOld particle type: " << iOldType << std::endl;
				std::cout << "\t\tNew particle type: " << iType << std::endl;
			}
			discretisation.vChangeParticleTypeInSphere(afCenter, fRadius,(unsigned char)iOldType,(unsigned char)iType);
		}
	}

	const std::vector<std::string> *paSteps = pPO->GetSteps();
	uiNumSteps = (unsigned int)paSteps->size() + 1;
	uiCurrentStep = 0;
	CAnalysisStepArray.vSetSize(uiNumSteps);
	// set up the init step
	CAnalysisStep *pCInitStep = new CAnalysisStep(this);
	if (!pCInitStep->boConfigure("Init,Init,")) return false;
	CAnalysisStepArray.vAdd(pCInitStep);
	for (unsigned int s = 1; s < uiNumSteps; s++)
	{
		std::cout << "\tStep " << s << ":" << std::endl;
		CAnalysisStep *pCAnalysisStep = new CAnalysisStep(this);
		if (!pCAnalysisStep->boConfigure(paSteps->at(s-1))) return false;
		// set up globals
		pCAnalysisStep->pGetSolver()->vSetGlobalVars(pCInitStep->pGetSolver()->pstGetGlobalVars());
		CAnalysisStepArray.vAdd(pCAnalysisStep);
	}
	
	// solver options
	const std::vector<std::string> *paExplicitParams = pPO->GetExplicitParams();
	if (paExplicitParams != NULL)
	{
		for (unsigned int i = 0; i < paExplicitParams->size(); i++)
		{
			std::string sExplicitParams = paExplicitParams->at(i);
			// check step no.
			sExplicitParams.erase(std::remove_if(sExplicitParams.begin(), sExplicitParams.end(), ::isspace), sExplicitParams.end());
			std::size_t pos1 = sExplicitParams.find(",");
			if (pos1!=std::string::npos)
			{
				std::string sStepNo = sExplicitParams.substr(0, pos1);
				unsigned int uiStepNo;
				if (sscanf(sStepNo.c_str(), "%d", &uiStepNo) != 1)
				{
					std::cout << "Warning: Step number could not be read in explicit parameters: " << sExplicitParams;
					std::cout << "! Configuration will be ignored." << std::endl;
				}
				else
				{
					if ((uiStepNo > 0) && (uiStepNo < uiNumSteps))
					{
						std::cout << "\tConfiguring solver for step " << uiStepNo << ":" << std::endl;
						CAnalysisStepArray.ElementAt(uiStepNo)->pGetSolver()->boSetParameters(sExplicitParams.substr(pos1+1, std::string::npos), CGenericSolver::SOLVER_enExplicit);
					}
					else
					{
						std::cout << "Warning: Invalid step number in explicit parameters: " << sExplicitParams;
						std::cout << "! Configuration will be ignored." << std::endl;
					}
				}
			}
			else 
			{
				std::cout << "Warning: Step number not found in explicit parameters: " << sExplicitParams;
				std::cout << "! Configuration will be ignored." << std::endl;
			}			
		}
	}

	// Wall motions
	const std::vector<std::string> *paWallMotion = pPO->GetWallMotion();
	if (paWallMotion != NULL)
	{
		for (unsigned int i = 0; i < paWallMotion->size(); i++)
		{
			std::string sWallMotion = paWallMotion->at(i);
			// check step no.
			sWallMotion.erase(std::remove_if(sWallMotion.begin(), sWallMotion.end(), ::isspace), sWallMotion.end());
			std::size_t pos1 = sWallMotion.find(",");
			if (pos1!=std::string::npos)
			{
				std::string sStepNo = sWallMotion.substr(0, pos1);
				unsigned int uiStepNo;
				if (sscanf(sStepNo.c_str(), "%d", &uiStepNo) != 1)
				{
					std::cout << "Warning: Step number could not be read in wall motion: " << sWallMotion;
					std::cout << "! Configuration will be ignored." << std::endl;
				}
				else
				{
					if ((uiStepNo > 0) && (uiStepNo < uiNumSteps))
					{
						std::cout << "\tConfiguring wall motion for step " << uiStepNo << std::endl;
						CAnalysisStepArray.ElementAt(uiStepNo)->pGetSolver()->boSetWallMotion(sWallMotion.substr(pos1+1, std::string::npos));
					}
					else
					{
						std::cout << "Warning: Invalid step number in wall motion: " << sWallMotion;
						std::cout << "! Configuration will be ignored." << std::endl;
					}
				}
			}
			else 
			{
				std::cout << "Warning: Step number not found in wall motion: " << sWallMotion;
				std::cout << "! Configuration will be ignored." << std::endl;
			}			
		}
	}

	// Interactions
	if (!interactions.boConfigure(uiNumSteps-1, 256, pPO)) return false;
	
	// Loads
	const std::vector<std::string> *paLoads = pPO->GetLoads();
	if (paLoads != NULL)
	{
		for (unsigned int i = 0; i < paLoads->size(); i++)
		{
			std::string sLoadConf = paLoads->at(i);
			CGenericLoad *pLoad = new CGenericLoad();
			std::cout << "\tLoad" << i+1 << ":" << std::endl;
			if (pLoad->boConfigure(sLoadConf, discretisation.pGetInputPolyData(), uiNumSteps-1))
			{
				CLoadsArray.vAdd(pLoad);
			}
			else delete pLoad;
		}
	}
	const std::vector<std::string> *paDisabledLoads = pPO->GetDisableLoads();
	if (paDisabledLoads != NULL)
	{
		unsigned int uiNumLoads = CLoadsArray.u32GetCount();
		for (unsigned int i = 0; i < paDisabledLoads->size(); i++)
		{
			std::string sLoadDisable = paDisabledLoads->at(i);
			sLoadDisable.erase(std::remove_if(sLoadDisable.begin(), sLoadDisable.end(), ::isspace), sLoadDisable.end());
			std::size_t pos1 = sLoadDisable.find(",");
			if (pos1!=std::string::npos)
			{
				std::string sStepNo = sLoadDisable.substr(0, pos1);
				unsigned int uiStepNo;
				if (sscanf(sStepNo.c_str(), "%d", &uiStepNo) != 1)
				{
					std::cout << "Warning: Step number could not be read in disable loads: " << sLoadDisable;
					std::cout << "! Configuration will be ignored." << std::endl;
				}
				else
				{
					if ((uiStepNo > 0) && (uiStepNo < uiNumSteps))
					{
						// find load with the given name
						bool boFound = false;
						for (unsigned int l = 0; l < uiNumLoads; l++)
						{
							if (CLoadsArray[l]->psGetName() == sLoadDisable.substr(pos1+1, std::string::npos))
							{
								CLoadsArray[l]->vDisable(uiStepNo-1);
								boFound = true;
								std::cout << "\tDisabled load " << CLoadsArray[l]->psGetName() << " from step " << uiStepNo << std::endl;
								break;
							}
						}
						if (!boFound) 
						{
							std::cout << "Warning: Could not find load name in disable loads: " << sLoadDisable;
							std::cout << "! Configuration will be ignored." << std::endl;
						}
					}
					else
					{
						std::cout << "Warning: Invalid step number in disable loads: " << sLoadDisable;
						std::cout << "! Configuration will be ignored." << std::endl;
					}
				}
			}
			else 
			{
				std::cout << "Warning: Step number not found in disable loads: " << sLoadDisable;
				std::cout << "! Configuration will be ignored." << std::endl;
			}
		}
	}

	// BC
	unsigned int uiNumDOF = 3*discretisation.uiGetTotalNumberOfParticles();
	pucBCMarkDOF = new unsigned char[uiNumDOF];
	pfBCAppliedDisplacementValue = new float[uiNumDOF];
	pfBCSavedDisplacements = new float[uiNumDOF];

	// Membrane particles
	// get membrane particles
	const std::vector<std::string> *aMembraneConfigs = pPO->GetMembrane();
	if (aMembraneConfigs != NULL)
	{
		std::cout << "\tMembranes:" << std::endl;
		for (unsigned int i = 0; i < aMembraneConfigs->size(); i++)
		{
			std::string config = aMembraneConfigs->at(i);
			int iIdValue;
			float fTension, fStiffness;
			int n = sscanf(config.c_str(), "%d,%f,%f", &iIdValue, &fTension, &fStiffness);
			if (n!=3) 
			{
				std::cout << "Error: Invalid membrane configuration: " << config << "!"<< std::endl; 
				return false;
			}
			else 
			{
				std::cout << "\t\tParticle type: " << iIdValue << std::endl;
				std::cout << "\t\tTension: " << fTension << std::endl;
				std::cout << "\t\tStiffness: " << fStiffness << std::endl;
			}
			unsigned int uiNumMembraneParticles = discretisation.uiMarkMembraneParticles(pPO->GetTypeId().c_str(), iIdValue);
			std::cout << "\t\tNumber of particles: " << uiNumMembraneParticles << std::endl;
			// update interractions
			std::string sInteractionConfig;
			std::stringstream sStream(sInteractionConfig);
			sStream << "C,1," << iIdValue << "," << iIdValue << "," << fTension << ",0";
			interactions.boAddInterraction(sStream.str());
			sStream.str("");
			sStream.clear();
			sStream << "L,1," << iIdValue << "," << iIdValue << "," << fStiffness << ",0";
			interactions.boAddInterraction(sStream.str());
		}
	}

	// get membrane particles around volumes
	const std::vector<std::string> *aMembraneVolumeConfigs = pPO->GetMembraneVolume();
	if (aMembraneVolumeConfigs != NULL)
	{
		std::cout << "\tMembranes around volumes:" << std::endl;
		for (unsigned int i = 0; i < aMembraneVolumeConfigs->size(); i++)
		{
			std::string config = aMembraneVolumeConfigs->at(i);
			int iIdValue, iIdMembraneType;
			float fTension, fStiffness;
			int n = sscanf(config.c_str(), "%d,%f,%f,%d", &iIdValue, &fTension, &fStiffness,&iIdMembraneType);
			if (n!=4) 
			{
				std::cout << "Error: Invalid volume membrane configuration: " << config << "!"<< std::endl; 
				return false;
			}
			else if ((iIdMembraneType < 0) || (iIdMembraneType > 255))
			{
				std::cout << "Error: Invalid membrane particle type value. Must be greater than 0 and lower than 256!" << std::endl; 
				return false;
			}
			else 
			{
				std::cout << "\t\tVolume particle type: " << iIdValue << std::endl;
				std::cout << "\t\tTension: " << fTension << std::endl;
				std::cout << "\t\tStiffness: " << fStiffness << std::endl;
				std::cout << "\t\tMembrane particle type: " << iIdMembraneType << std::endl;
			}
			unsigned int uiNumMembraneParticles = discretisation.uiMembraneVolumeParticles(iIdValue, (unsigned char)iIdMembraneType);
			std::cout << "\t\tNumber of particles: " << uiNumMembraneParticles << std::endl;
			// update interractions
			std::string sInteractionConfig;
			std::stringstream sStream(sInteractionConfig);
			sStream << "C,1," << iIdMembraneType << "," << iIdMembraneType << "," << fTension << ",0";
			interactions.boAddInterraction(sStream.str());
			sStream.str("");
			sStream.clear();
			sStream << "L,1," << iIdMembraneType << "," << iIdMembraneType << "," << fStiffness << ",0";
			interactions.boAddInterraction(sStream.str());
		}
	}

	// particles having same interactions
	const std::vector<std::string> *aInteractionCopyConfig = pPO->GetInteractionCopy();
	if (aInteractionCopyConfig != NULL)
	{
		std::cout << "\tInteractions copy:" << std::endl;
		for (unsigned int i = 0; i < aInteractionCopyConfig->size(); i++)
		{
			std::string config = aInteractionCopyConfig->at(i);
			int iTypeFrom, iTypeTo;
			int n = sscanf(config.c_str(), "%d,%d", &iTypeFrom, &iTypeTo);
			if (n!=2) 
			{
				std::cout << "Error: Invalid interactions copy configuration: " << config << "!"<< std::endl; 
				return false;
			}
			else if ((iTypeFrom < 0) || (iTypeFrom > 255) || (iTypeTo < 0) || (iTypeTo > 255))
			{
				std::cout << "Error: Invalid type value. Must be greater than 0 and lower than 256!" << std::endl; 
				return false;
			}
			else 
			{
				std::cout << "\t\tFrom particle type " << iTypeFrom << "to " << iTypeTo << std::endl;
			}
			interactions.vCopyInterractions((unsigned char)iTypeFrom, (unsigned char)iTypeTo);
		}
	}

	std::cout << "\tInteractions:" << std::endl << interactions;

	// Growth
	const std::vector<std::string> *aGrowthConfigs = pPO->GetGrowth();
	if (aGrowthConfigs != NULL)
	{
		std::cout << "\tGrowth:" << std::endl;
		for (unsigned int i = 0; i < aGrowthConfigs->size(); i++)
		{
			std::string config = aGrowthConfigs->at(i);
			CGrowth *pGrow = new CGrowth();
			std::cout << "\tGrowth " << i+1 << ":" << std::endl;
			if (pGrow->boConfigure(config))
			{
				CGrowthArray.vAdd(pGrow);
			}
			else delete pGrow;
		}
	}

	// configure Catalyst
	std::string PythonScriptName = pPO->GetPythonScript();
	catalystAdaptor.boAddPythonScript(PythonScriptName.c_str());
	catalystAdaptor.vSetSendInterval(pPO->uiGetOutputInterval());
	boUseParallelOutput = pPO->boUseParallelOutput();
	catalystAdaptor.vSetParralelDataTransfer(boUseParallelOutput);
	boOutputAdditionalParticles = pPO->boOutputAdditionalParticles();
	if (boUseParallelOutput) 
	{
		pOutputGlobalsCopy = pOutputGlobalsCopy->New();
	}
	pOutputPolyDataCopy = pOutputPolyDataCopy->New();
	
	std::cout << "\tCatalyst output script: " << PythonScriptName << std::endl;
	std::cout << "\t\tOutput interval (iterations): " << pPO->uiGetOutputInterval() << std::endl;
	std::cout << "\t\tParallel output: " << boUseParallelOutput << std::endl;
	std::cout << "\t\tOutput additional particles: " << boOutputAdditionalParticles << std::endl;
	sResultsFileName = pPO->GetResultsFileName();
	std::cout << "\t\tResults file: " << sResultsFileName << std::endl;
	if (boOutputAdditionalParticles == false)
	{
		// filter out additional particles
		arrayIdsActiveParticles = vtkSmartPointer<vtkIdTypeArray>::New();
		arrayIdsActiveParticles->SetNumberOfComponents(1);
		
		selectionNode = vtkSmartPointer<vtkSelectionNode>::New();
		selectionNode->SetFieldType(vtkSelectionNode::POINT);
		selectionNode->SetContentType(vtkSelectionNode::INDICES);

		selection = vtkSmartPointer<vtkSelection>::New();
		extractSelectedIds = vtkSmartPointer<vtkExtractSelection>::New();
	}
	return true;
}

bool CWorkspace::boRunAnalysis(void)
{
	float fStartTime = 0;
	unsigned int uiStartIteration = 0;
	for (unsigned int i = 0; i < uiNumSteps; i++)
	{
		boost::timer::auto_cpu_timer *pBoostTimer = new boost::timer::auto_cpu_timer();
		vResetOutputGlobals();
		uiCurrentStep = i;
		CAnalysisStep *pCAnalysisStep = CAnalysisStepArray[i];
		std::cout << "Analysing step " << pCAnalysisStep->GetName() << " ..." << std::endl;
		pCAnalysisStep->pGetSolver()->vSetStartTime(fStartTime);
		pCAnalysisStep->pGetSolver()->vSetStartIteration(uiStartIteration);
		if (!pCAnalysisStep->pGetSolver()->boPerformAnalysis()) return false;
		fStartTime = pCAnalysisStep->pGetSolver()->fGetEndTime();
		uiStartIteration = pCAnalysisStep->pGetSolver()->uiGetEndIteration();
		std::cout << "\tCompleted in: ";
		delete pBoostTimer;
	}
	std::cout << "Writing results to " << sResultsFileName << " ..." << std::endl;
	vWriteOutputPoly(sResultsFileName);
	return true;
}

void CWorkspace::vMoveWalls(float fRelativeStepTime, float *afWallMotion)
{
	float *pfInitialLocation = walls.pfGetInitialLocation();
	float *pfCurrentLocation = walls.pfGetLocation();
	float afWallDelta[6];
	VECT_vMultiplyScalar(afWallMotion, fRelativeStepTime, afWallDelta, 6);
	VECT_vAdd(pfInitialLocation, afWallDelta, pfCurrentLocation, 6);
}

void CWorkspace::vUpdateWallLocation(float *afWallMotion)
{
	float *pfInitialLocation = walls.pfGetInitialLocation();
	VECT_vAdd(pfInitialLocation, afWallMotion, pfInitialLocation, 6);
}

void CWorkspace::vInitGrowth(float *pfGrowthFactor, unsigned char *pucParticlesType, unsigned int uiNumParticles, unsigned int uiNumSteps)
{
	for (unsigned int i = 0; i < uiNumParticles; i++)
	{
		pfGrowthFactor[i] = 1;
		unsigned int uiNumGrowth = CGrowthArray.u32GetCount();
		for (unsigned int g = 0; g < uiNumGrowth; g++)
		{
			CGrowth *pGrow = CGrowthArray[g];
			if (pGrow->uiStepNumber == uiCurrentStep)
			{
				if (pGrow->uiParticleType == pucParticlesType[i])
				{
					pfGrowthFactor[i] = pow((double)(1+pGrow->fGrowthFactor), (double)1.0/(uiDimensions*uiNumSteps));
					boGrowthActive_ = true;
				}
			}
		}
	}	
}

void CWorkspace::vInitBC(float *pfDisp, unsigned int uiNum)
{
	// init the displacements at the beginning of a step
	// save current displacements
	VECT_vCopy(pfDisp, pfBCSavedDisplacements, uiNum);
	VECT_vInit(pucBCMarkDOF, uiNum, (unsigned char)0);
	VECT_vInit(pfBCAppliedDisplacementValue, uiNum, 0.0f);
	// mark DOFs
	unsigned int uiNumLoads = CLoadsArray.u32GetCount();
	for (unsigned int i = 0; i < uiNumLoads; i++)
	{
		CGenericLoad *pLoad = CLoadsArray[i];
		if (pLoad->boActiveInStep(uiCurrentStep-1))
		{
			unsigned int uiNumLoadedPoints = pLoad->uiGetNumberOfLoadedPoints();
			if (pLoad->enGetType() == CGenericLoad::LOAD_enEncastred)
			{
				// apply encastred
				for (unsigned k = 0; k < uiNumLoadedPoints; k++)
				{
					unsigned int uiPointIdx = pLoad->uiGetLoadedPointIndex(k);
					if (pLoad->boOnX()) pucBCMarkDOF[3*uiPointIdx] = 1;
					if (pLoad->boOnY()) pucBCMarkDOF[3*uiPointIdx+1] = 1;
					if (pLoad->boOnZ()) pucBCMarkDOF[3*uiPointIdx+2] = 1;
				}
			}
			else if (pLoad->enGetType() == CGenericLoad::LOAD_enDisplaced)
			{
				// check if this is the firts step where active
				if ((uiCurrentStep == 1) || pLoad->boActiveInStep(uiCurrentStep-2))
				{
					// apply as displacement
					for (unsigned k = 0; k < uiNumLoadedPoints; k++)
					{
						unsigned int uiPointIdx = pLoad->uiGetLoadedPointIndex(k);
						if (pLoad->boOnX()) 
						{
							pucBCMarkDOF[3*uiPointIdx] = 1;
							pfBCAppliedDisplacementValue[3*uiPointIdx] = pLoad->fGetSizeX();
						}
						if (pLoad->boOnY()) 
						{
							pucBCMarkDOF[3*uiPointIdx+1] = 1;
							pfBCAppliedDisplacementValue[3*uiPointIdx+1] = pLoad->fGetSizeY();
						}
						if (pLoad->boOnZ()) 
						{
							pucBCMarkDOF[3*uiPointIdx+2] = 1;
							pfBCAppliedDisplacementValue[3*uiPointIdx+2] = pLoad->fGetSizeZ();
						}
					}
				}
				else
				{
					// apply as encastred
					for (unsigned k = 0; k < uiNumLoadedPoints; k++)
					{
						unsigned int uiPointIdx = pLoad->uiGetLoadedPointIndex(k);
						if (pLoad->boOnX()) pucBCMarkDOF[3*uiPointIdx] = 1;
						if (pLoad->boOnY()) pucBCMarkDOF[3*uiPointIdx+1] = 1;
						if (pLoad->boOnZ()) pucBCMarkDOF[3*uiPointIdx+2] = 1;
					}
				}
			}
		}
	}
}

void CWorkspace::vApplyBC(float *pfDisp, vtkIdType *pIdActivePoints, unsigned int uiNum, float fRelTime)
{
	for (unsigned int j = 0; j < uiNum; j++)
	{
		unsigned int dofIdx = 3*pIdActivePoints[j];
		for (unsigned int k = 0; k < 3; k++)
		{
			unsigned int i = dofIdx + k;
			if (pucBCMarkDOF[i])
			{
				pfDisp[i] = pfBCSavedDisplacements[i] + pfBCAppliedDisplacementValue[i]*fRelTime;
			}
		}
	}
}

bool CWorkspace::boPointIsFixed(unsigned int uiPointIdx)
{
	bool boFixed = false;
	if (pucBCMarkDOF[3*uiPointIdx] && pucBCMarkDOF[3*uiPointIdx+1] && pucBCMarkDOF[3*uiPointIdx+2])
		boFixed = true;
	return boFixed;
}

void CWorkspace::vAddOutputLine(unsigned int uiPointIdx1, unsigned int uiPointIdx2)
{
	// create vertices
	vtkSmartPointer<vtkLine> line = vtkSmartPointer<vtkLine>::New();
	line->GetPointIds()->SetId(0, uiPointIdx1);
	line->GetPointIds()->SetId(1, uiPointIdx2);
	pOutputPolyData->GetLines()->InsertNextCell(line);
}

void CWorkspace::vOutputResults(int iTimeStep, double time)
{
	if (catalystAdaptor.boReadyToTransfer(iTimeStep))
	{
		vUpdateOutputWalls();
		//vAddMembraneConnections();
		if (boUseParallelOutput) 
		{
			if (boOutputAdditionalParticles)
			{
				pOutputPolyDataCopy->DeepCopy(pOutputPolyData);
			}
			else
			{
				arrayIdsActiveParticles->SetVoidArray(discretisation.pIdGetActiveParticleIndexes(), discretisation.uiGetNumberOfActiveParticles(), 1); 
				selectionNode->SetSelectionList(arrayIdsActiveParticles);
				selection->RemoveAllNodes();
				selection->AddNode(selectionNode);

				extractSelectedIds->SetInputData(0, pOutputPolyData);
				extractSelectedIds->SetInputData(1, selection);
				extractSelectedIds->Update();
				pOutputPolyDataCopy->DeepCopy(extractSelectedIds->GetOutput());
			}
			catalystAdaptor.vSetOutputBlock(0, pOutputPolyDataCopy);
			pOutputGlobalsCopy->DeepCopy(pOutputGlobals);
			catalystAdaptor.vSetOutputBlock(2, pOutputGlobalsCopy);
		}
		else
		{
			if (boOutputAdditionalParticles)
			{
				catalystAdaptor.vSetOutputBlock(0, pOutputPolyData);
			}
			else
			{
				arrayIdsActiveParticles->SetVoidArray(discretisation.pIdGetActiveParticleIndexes(), discretisation.uiGetNumberOfActiveParticles(), 1); 
				selectionNode->SetSelectionList(arrayIdsActiveParticles);
				selection->RemoveAllNodes();
				selection->AddNode(selectionNode);

				extractSelectedIds->SetInputData(0, pOutputPolyData);
				extractSelectedIds->SetInputData(1, selection);
				extractSelectedIds->Update();
				pOutputPolyDataCopy->ShallowCopy(extractSelectedIds->GetOutput());
				catalystAdaptor.vSetOutputBlock(0, pOutputPolyDataCopy);
			}
			catalystAdaptor.vSetOutputBlock(2, pOutputGlobals);
		}
		catalystAdaptor.vSetOutputBlock(1, pOutputWalls);
		catalystAdaptor.vTransferData(iTimeStep, time);
	}
}

void CWorkspace::vAddMembraneConnections(void)
{
	unsigned int uiNumParticles = discretisation.uiGetTotalNumberOfParticles();
	vtkSmartPointer<vtkCellArray> lines = vtkSmartPointer<vtkCellArray>::New();
	for (unsigned int i = 0; i < uiNumParticles; i++)
	{
		if (discretisation.pucMembraneIdx[i])
		{
			unsigned char ucNumNeighbours = discretisation.pucMembraneNumNeighbours[i];
			for (unsigned char j = 0; j < ucNumNeighbours; j++)
			{
				unsigned int uiNeighbourIdx = discretisation.puiMembraneNeigbours[i*discretisation.ucMaxMembraneNeighbours + j];
				if (i < uiNeighbourIdx) 
				{
					vtkSmartPointer<vtkLine> line = vtkSmartPointer<vtkLine>::New();
					line->GetPointIds()->SetId(0, i);
					line->GetPointIds()->SetId(1, uiNeighbourIdx);
					lines->InsertNextCell(line);
				}
			}
		}
	}
	//std::cout << "Number of lines: " << lines->GetNumberOfCells() << std::endl;
	//pOutputPolyData->DeleteCells();
	pOutputPolyData->SetLines(lines);
}

void CWorkspace::vWriteOutputPoly(std::string fileName)
{
	vtkSmartPointer<vtkPolyDataWriter> writer = vtkSmartPointer<vtkPolyDataWriter>::New();
	//vtkSmartPointer<vtkUnstructuredGridWriter> writer = vtkSmartPointer<vtkUnstructuredGridWriter>::New();
	
	writer->SetFileTypeToBinary();
	vAddMembraneConnections();
	if (boOutputAdditionalParticles)
	{
		writer->SetInputData(pOutputPolyData);
	}
	else
	{
		// filter out additional particles
		arrayIdsActiveParticles->SetVoidArray(discretisation.pIdGetActiveParticleIndexes(), discretisation.uiGetNumberOfActiveParticles(), 1); 
		selectionNode->SetSelectionList(arrayIdsActiveParticles);
		selectionNode->GetProperties()->Set(vtkSelectionNode::CONTAINING_CELLS(), 1);

		selection->RemoveAllNodes();
		selection->AddNode(selectionNode);

		extractSelectedIds->RemoveAllInputs();
		extractSelectedIds->SetInputData(0, pOutputPolyData);
		extractSelectedIds->SetInputData(1, selection);
		extractSelectedIds->Update();

		vtkSmartPointer<vtkGeometryFilter> geometryFilter = vtkSmartPointer<vtkGeometryFilter>::New();
		geometryFilter->SetInputData(extractSelectedIds->GetOutput());
		geometryFilter->Update();
		pOutputPolyData->ShallowCopy(geometryFilter->GetOutput());
		writer->SetInputData(pOutputPolyData);
	}
	writer->SetFileName(fileName.c_str());
	writer->Write();
}

} // namespace

