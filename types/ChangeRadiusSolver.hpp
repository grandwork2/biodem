/****************************************************************************** 
 * 
 *  file:  ChangeRadiusSolver.hpp
 * 
 *  Copyright (c) 2016, Grand R. Joldes.
 *  All rights reverved.
 * 
 *  
 *  THE SOFTWARE IS PROVIDED _AS IS_, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *  DEALINGS IN THE SOFTWARE.  
 *  
 *****************************************************************************/ 

#ifndef __CHANGERADIUSSOLVER_HPP__
#define __CHANGERADIUSSOLVER_HPP__

#include "ExplicitSolver.hpp"

namespace BioDEM {

class CChangeRadiusSolver: public CExplicitSolver
{
public:
	CChangeRadiusSolver(void);
	~CChangeRadiusSolver(void);
	void vInitComputations(void);
	void vComputeLumpedMassMatrix(void);
	void vComputeParticlesForces(void);
	void vComputeInterractions(unsigned int uiParticleIdx, unsigned int uiNeighbourIndex);
	bool boFinishAnalysis(void);
protected:
	float *pfAverageNeighbourDistance;
	float *pfMaxPenetration;
	float fAverageRadius;
	float fRadiusDispersion;
	bool boAnalysisFinished;
};


} // namespace

#endif /* __EXPLICITSOLVER_HPP__ */