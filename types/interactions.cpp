/****************************************************************************** 
 * 
 *  file:  interactions.cpp
 * 
 *  Copyright (c) 2016, Grand R. Joldes.
 *  All rights reverved.
 * 
 *  
 *  THE SOFTWARE IS PROVIDED _AS IS_, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *  DEALINGS IN THE SOFTWARE.  
 *  
 *****************************************************************************/ 
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <ctype.h>

#include "interactions.hpp"

namespace BioDEM {

CStepInteractions::CStepInteractions(unsigned int uiNumParticleTypes_):boConstantForceActive(false), boLinearForceActive(false)
{
	uiNumParticleTypes = uiNumParticleTypes_;
	afConstantForce = new float[uiNumParticleTypes*uiNumParticleTypes];
	afConstantForceDistance = new float[uiNumParticleTypes*uiNumParticleTypes];
	afLinearForceStiffness = new float[uiNumParticleTypes*uiNumParticleTypes];
	afLinearForceDistance = new float[uiNumParticleTypes*uiNumParticleTypes];
	
	for (unsigned int p1 = 0; p1 < uiNumParticleTypes; p1++)
	{
		for (unsigned int p2 = 0; p2 < uiNumParticleTypes; p2++)
		{
			afConstantForce[p1*uiNumParticleTypes + p2] = 0;
			afConstantForceDistance[p1*uiNumParticleTypes + p2] = 0;
			afLinearForceStiffness[p1*uiNumParticleTypes + p2] = 0;
			afLinearForceDistance[p1*uiNumParticleTypes + p2] = 0;
		}
	}
};

CStepInteractions::~CStepInteractions()
{
	delete[] afConstantForce;
	delete[] afConstantForceDistance;
	delete[] afLinearForceStiffness;
	delete[] afLinearForceDistance;
};

CInteractions::CInteractions():boConfigured_(false),uiNumLoadSteps(0),uiNumParticleTypes(0)
{
	stParticleGrowth.boActive = false;
	stParticleStiffnessDegradation.boActive = false; 
};

CInteractions::~CInteractions()
{
	for (unsigned int i = 0; i < uiNumLoadSteps; i++)
	{
		CStepInteractions *pCStepInteractions = pCStepInteractionsVector.at(i);
		delete pCStepInteractions;
	}
};

bool CInteractions::boConfigure(unsigned int uiNumLoadSteps_, unsigned int uiNumParticleTypes_, program_options *pPO)
{
	const std::vector<std::string> *paInteractionsDefinition = pPO->GetInteractions();
	if (paInteractionsDefinition) {
		for (unsigned int i = 0; i < uiNumLoadSteps; i++)
		{
			CStepInteractions *pCStepInteractions = pCStepInteractionsVector.at(i);
			delete pCStepInteractions;
		}
		uiNumLoadSteps = uiNumLoadSteps_;
		uiNumParticleTypes = uiNumParticleTypes_;
		pCStepInteractionsVector.clear();
		pCStepInteractionsVector.reserve(uiNumLoadSteps);
		for (unsigned int i = 0; i < uiNumLoadSteps; i++)
		{
			CStepInteractions *pCStepInteractions = new CStepInteractions(uiNumParticleTypes);
			pCStepInteractionsVector.push_back(pCStepInteractions);
		}
		
		for (unsigned int i = 0; i < paInteractionsDefinition->size(); i++)
		{
			if (!boAddInterraction(paInteractionsDefinition->at(i)))
			{
				return false;
			}
		}
	}

	std::string growConfig = pPO->GetParticleGrowthConfig();
	int iTypeInitiator, iTypeAffected;
	float fFactor;
	if (!growConfig.empty())
	{
		// read particle growth configuration
		int n = sscanf(growConfig.c_str(), "%d,%d,%f", &iTypeInitiator, &iTypeAffected, &fFactor);
		if (n!=3) 
		{
			std::cout << "Error: Invalid particle growth configuration: " << growConfig << "!"<< std::endl; 
			return false;
		}
		else 
		{
			std::cout << "\tParticle growth:" << std::endl;
			std::cout << "\t\tInitiator type: " << iTypeInitiator << std::endl;
			std::cout << "\t\tGrown type: " << iTypeAffected << std::endl;
			std::cout << "\t\tGrowth factor: " << fFactor << std::endl;
			stParticleGrowth.boActive = true;
			stParticleGrowth.fGrowFactor = fFactor;
			stParticleGrowth.uiGrown = iTypeAffected;
			stParticleGrowth.uiInitiator = iTypeInitiator;
		}
	}
	
	std::string stiffnessDegradationConfig = pPO->GetStiffnessDegradationConfig();
	if (!stiffnessDegradationConfig.empty())
	{
		// read particle stiffness degradation configuration
		int n = sscanf(stiffnessDegradationConfig.c_str(), "%d,%d,%f", &iTypeInitiator, &iTypeAffected, &fFactor);
		if (n!=3) 
		{
			std::cout << "Error: Invalid particle stiffness degradation configuration: " << growConfig << "!"<< std::endl; 
			return false;
		}
		else 
		{
			std::cout << "\tParticle stiffness degradation:" << std::endl;
			std::cout << "\t\tInitiator type: " << iTypeInitiator << std::endl;
			std::cout << "\t\tDegraded type: " << iTypeAffected << std::endl;
			std::cout << "\t\tDegradation factor: " << fFactor << std::endl;
			stParticleStiffnessDegradation.boActive = true;
			stParticleStiffnessDegradation.fDegradationFactor = fFactor;
			stParticleStiffnessDegradation.uiDegraded = iTypeAffected;
			stParticleStiffnessDegradation.uiInitiator = iTypeInitiator;
		}
	}
	boConfigured_ = true;
	return true;
}

bool CInteractions::boAddInterraction(std::string s)
{
	char cType;
	int iStep, iP1Type, iP2Type;
	float fForce, fDist;
	if (sscanf(s.c_str(), "%1c,%d,%d,%d,%f,%f", &cType, &iStep, &iP1Type, &iP2Type, &fForce, &fDist) != 6)
	{
		std::cout << "Error in defining Interraction " << s << "!" << std::endl;
		return false;
	}
	else
	{
		if ((toupper(cType) != 'C') && (toupper(cType) != 'L'))
		{
			std::cout << "Error in defining Interraction " << s << " - unknown interraction type (must be C or L)!" << std::endl;
			return false;
		}
		else
		{
			if ((iStep <= 0) || (iStep > (int)uiNumLoadSteps))
			{
				std::cout << "Error in defining Interraction " << s << " - unknown time step (must be between 1 and " << uiNumLoadSteps << ")!" << std::endl;
				return false;
			}
			else
			{
				if ((iP1Type < 0) || (iP1Type >= (int)uiNumParticleTypes))
				{
					std::cout << "Error in defining Interraction " << s << " - unknown first particle type (must be between 0 and " << uiNumParticleTypes-1 << ")!" << std::endl;
					return false;
				}
				else
				{
					if ((iP2Type < 0) || (iP2Type >= (int)uiNumParticleTypes))
					{
						std::cout << "Error in defining Interraction " << s << " - unknown second particle type (must be between 0 and " << uiNumParticleTypes-1 << ")!" << std::endl;
						return false;
					}
					else
					{
						if (toupper(cType) == 'C')
						{
							// define a constant force
							for (unsigned int i = iStep-1; i < uiNumLoadSteps; i++)
							{
								pCStepInteractionsVector[i]->boConstantForceActive = true;
								pCStepInteractionsVector[i]->afConstantForce[iP1Type*uiNumParticleTypes + iP2Type] = fForce;
								pCStepInteractionsVector[i]->afConstantForceDistance[iP1Type*uiNumParticleTypes + iP2Type] = fDist;
								pCStepInteractionsVector[i]->afConstantForce[iP2Type*uiNumParticleTypes + iP1Type] = fForce;
								pCStepInteractionsVector[i]->afConstantForceDistance[iP2Type*uiNumParticleTypes + iP1Type] = fDist;
							}
						}
						else
						{
							// define a linear force
							for (unsigned int i = iStep-1; i < uiNumLoadSteps; i++)
							{
								pCStepInteractionsVector[i]->boLinearForceActive = true;
								pCStepInteractionsVector[i]->afLinearForceStiffness[iP1Type*uiNumParticleTypes + iP2Type] = fForce;
								pCStepInteractionsVector[i]->afLinearForceDistance[iP1Type*uiNumParticleTypes + iP2Type] = fDist;
								pCStepInteractionsVector[i]->afLinearForceStiffness[iP2Type*uiNumParticleTypes + iP1Type] = fForce;
								pCStepInteractionsVector[i]->afLinearForceDistance[iP2Type*uiNumParticleTypes + iP1Type] = fDist;
							}
						}
					}
				}
			}
		}
	}
	return true;
};

void CInteractions::vCopyInterractions(unsigned char ucTypeFrom, unsigned char ucTypeTo)
{
	for (unsigned int i = 0; i < uiNumLoadSteps; i++)
	{
		pCStepInteractionsVector[i]->afConstantForce[ucTypeFrom*uiNumParticleTypes + ucTypeTo] = pCStepInteractionsVector[i]->afConstantForce[ucTypeFrom*uiNumParticleTypes + ucTypeFrom];
		pCStepInteractionsVector[i]->afConstantForceDistance[ucTypeFrom*uiNumParticleTypes + ucTypeTo] = pCStepInteractionsVector[i]->afConstantForceDistance[ucTypeFrom*uiNumParticleTypes + ucTypeFrom];
		pCStepInteractionsVector[i]->afLinearForceStiffness[ucTypeFrom*uiNumParticleTypes + ucTypeTo] = pCStepInteractionsVector[i]->afLinearForceStiffness[ucTypeFrom*uiNumParticleTypes + ucTypeFrom];
		pCStepInteractionsVector[i]->afLinearForceDistance[ucTypeFrom*uiNumParticleTypes + ucTypeTo] = pCStepInteractionsVector[i]->afLinearForceDistance[ucTypeFrom*uiNumParticleTypes + ucTypeFrom];
			
		for (unsigned int iP2Type = 0; iP2Type < uiNumParticleTypes; iP2Type++)
		{
			pCStepInteractionsVector[i]->afConstantForce[ucTypeTo*uiNumParticleTypes + iP2Type] = pCStepInteractionsVector[i]->afConstantForce[ucTypeFrom*uiNumParticleTypes + iP2Type];
			pCStepInteractionsVector[i]->afConstantForceDistance[ucTypeTo*uiNumParticleTypes + iP2Type] = pCStepInteractionsVector[i]->afConstantForceDistance[ucTypeFrom*uiNumParticleTypes + iP2Type];
			pCStepInteractionsVector[i]->afLinearForceStiffness[ucTypeTo*uiNumParticleTypes + iP2Type] = pCStepInteractionsVector[i]->afLinearForceStiffness[ucTypeFrom*uiNumParticleTypes + iP2Type];
			pCStepInteractionsVector[i]->afLinearForceDistance[ucTypeTo*uiNumParticleTypes + iP2Type] = pCStepInteractionsVector[i]->afLinearForceDistance[ucTypeFrom*uiNumParticleTypes + iP2Type];
		}
		for (unsigned int iP2Type = 0; iP2Type < uiNumParticleTypes; iP2Type++)
		{
			pCStepInteractionsVector[i]->afConstantForce[iP2Type*uiNumParticleTypes + ucTypeTo] = pCStepInteractionsVector[i]->afConstantForce[iP2Type*uiNumParticleTypes + ucTypeFrom];
			pCStepInteractionsVector[i]->afConstantForceDistance[iP2Type*uiNumParticleTypes + ucTypeTo] = pCStepInteractionsVector[i]->afConstantForceDistance[iP2Type*uiNumParticleTypes + ucTypeFrom];
			pCStepInteractionsVector[i]->afLinearForceStiffness[iP2Type*uiNumParticleTypes + ucTypeTo] = pCStepInteractionsVector[i]->afLinearForceStiffness[iP2Type*uiNumParticleTypes + ucTypeFrom];
			pCStepInteractionsVector[i]->afLinearForceDistance[iP2Type*uiNumParticleTypes + ucTypeTo] = pCStepInteractionsVector[i]->afLinearForceDistance[iP2Type*uiNumParticleTypes + ucTypeFrom];
		}
	}

}

std::ostream& operator<<(std::ostream& out, const CStepInteractions& inter)
{
	out << "\t\tConstant force: ";
	if (inter.boConstantForceActive) out << "Active" << std::endl;
	else out << "Not Active" << std::endl;

	for (unsigned int p1 = 0; p1 < inter.uiNumParticleTypes; p1++)
	{
		for (unsigned int p2 = p1; p2 < inter.uiNumParticleTypes; p2++)
		{
			if (inter.afConstantForce[p1*inter.uiNumParticleTypes + p2] != 0)
			{
				out << "\t\t(" << p1 << "," << p2 << "): ";
				out << inter.afConstantForce[p1*inter.uiNumParticleTypes + p2];
				out << ", " << inter.afConstantForceDistance[p1*inter.uiNumParticleTypes + p2] << std::endl;
			}
		}
	}

	out << "\t\tLinear force: ";
	if (inter.boLinearForceActive) out << "Active" << std::endl;
	else out << "Not Active" << std::endl;
	
	for (unsigned int p1 = 0; p1 < inter.uiNumParticleTypes; p1++)
	{
		for (unsigned int p2 = p1; p2 < inter.uiNumParticleTypes; p2++)
		{
			if (inter.afLinearForceStiffness[p1*inter.uiNumParticleTypes + p2] != 0)
			{
				out << "\t\t(" << p1 << "," << p2 << "): ";
				out << inter.afLinearForceStiffness[p1*inter.uiNumParticleTypes + p2];
				out << ", " << inter.afLinearForceDistance[p1*inter.uiNumParticleTypes + p2] << std::endl;
			}
		}
	}

	return out;
};

std::ostream& operator<<(std::ostream& out, const CInteractions& inter)
{
	for (unsigned int i = 0; i < inter.uiNumLoadSteps; i++)
	{
		out << "\tStep " << i+1 << ":" << std::endl;
		out << *(inter.pCStepInteractionsVector[i]);
	}
	return out;
};


} // namespace

