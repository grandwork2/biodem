/****************************************************************************** 
 * 
 *  file:  ExplicitSolver.hpp
 * 
 *  Copyright (c) 2016, Grand R. Joldes.
 *  All rights reverved.
 * 
 *  
 *  THE SOFTWARE IS PROVIDED _AS IS_, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *  DEALINGS IN THE SOFTWARE.  
 *  
 *****************************************************************************/ 

#ifndef __EXPLICITSOLVER_HPP__
#define __EXPLICITSOLVER_HPP__

#include <string>

#include "GenericSolver.hpp"
#include "bucketsearch.hpp"

namespace BioDEM {

class CExplicitSolver: public CGenericSolver
{
public:
	CExplicitSolver(void);
	~CExplicitSolver(void);
	bool boConfigure(std::string config);
	bool boSetParameters(std::string config, tenSolverType enSolverType);
	bool boPerformAnalysis(void);
	void vDoTimeStepping(void);
	virtual void vInitComputations(void);
	virtual void vComputeLumpedMassMatrix(void);
	virtual void vComputeParticlesForces(void);
	virtual void vComputeInterractions(unsigned int uiParticleIdx, unsigned int uiNeighbourIndex);
	virtual void vFindMembraneNeighbours(unsigned int uiParticleIdx, unsigned int uiNeighbourIndex);
	void vComputeNewDisplacements(void);
	void vInitDisplacements(void);
	void vCheckWallsPenetration(void);
	void vChangeDampingParameters(void);
	virtual bool boFinishAnalysis(void);
	void vCopyParticle(unsigned int fromIdx, unsigned int toIdx);
	void vGrowParticles(void);
protected:
	// time step for integration
	float fTimeStep;
	// total time
	float fTotalTime;
	unsigned int uiTimeStep;
	// Lumped mass matrix
	float *pfLumpedMass;
	float *pfScaledMass;
	// Bucket search
	BucketSearch::buckets *pCBuckets;
	BucketSearch::BucketSearch *pCBucketSearch;
	float fMaxParticleDisplacementBeforeBucketDistribution;
	unsigned int uiNumBucketsX, uiNumBucketsY, uiNumBucketsZ;
	// configuration
	float fInitialDampingParam;
	bool boAdaptiveDamping;
	bool boScaleMass;
	bool boCheckConvergence;
	float fConvergenceAbsoluteError;
	unsigned int uiConvergenceNumSteps;
	unsigned int uiTerminationCount;
	// adaptive convergence rate
	float fOldDampingParam;
	unsigned int uiNumDampParamSamples;
	bool boDampingParamStabilized;
	bool boSavedForceDisplacementUpdated;
	float *pfSavedDisplacements;
	float *pfSavedForces;
	// debug arrays
	unsigned int *puiBucketIndex;
};


} // namespace

#endif /* __EXPLICITSOLVER_HPP__ */