/****************************************************************************** 
 * 
 *  file:  ChangeRadiusSolver.cpp
 * 
 *  Copyright (c) 2016, Grand R. Joldes.
 *  All rights reverved.
 * 
 *  
 *  THE SOFTWARE IS PROVIDED _AS IS_, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *  DEALINGS IN THE SOFTWARE.  
 *  
 *****************************************************************************/ 
#include <vtkUnsignedIntArray.h>

#include "ChangeRadiusSolver.hpp"

namespace BioDEM {

CChangeRadiusSolver::CChangeRadiusSolver(void)
: CExplicitSolver()
{
	pfAverageNeighbourDistance = NULL;
	pfMaxPenetration = NULL;
}

CChangeRadiusSolver::~CChangeRadiusSolver(void)
{
	if (pfAverageNeighbourDistance != NULL) delete[] pfAverageNeighbourDistance;
	if (pfMaxPenetration != NULL) delete[] pfMaxPenetration;
};

void CChangeRadiusSolver::vComputeLumpedMassMatrix(void)
{
	VECT_vInit(pfLumpedMass, uiTotalNumParticles, 1.0f);
}

void CChangeRadiusSolver::vInitComputations(void)
{
	// Damping parameters
	if (fInitialDampingParam == 0) // get convergence rate from previous step
	{
		fInitialDampingParam = 0.85;
	}
	fOldDampingParam = fInitialDampingParam;
	fDampingParam = fInitialDampingParam;

	boAdaptiveDamping = false;
	boScaleMass = true;

	std::cout << "\t\tConvergence rate: " << fInitialDampingParam << std::endl;
	std::cout << "\t\tAdaptive convergence rate: ";
	if (boAdaptiveDamping) std::cout << "Yes" << std::endl;
	else std::cout << "No" << std::endl;
	std::cout << "\t\tMass scaling: ";
	if (boScaleMass) std::cout << "Yes" << std::endl;
	else std::cout << "No" << std::endl;

	// Compute critical time step, as defined by the interractions and particle masses
	// The particles masses and stifness are set to 1
	float fMass = 0.5f;
	float fTimeStep_ = sqrt(fMass/3.0f);
	if (uiDimensions == 3) fTimeStep_ /= sqrt(2.0f);
	float fCriticalTimeStep = fTimeStep_;

	std::cout << "\tComputed critical time step: " << fCriticalTimeStep << std::endl;
	if (boScaleMass) fCriticalTimeStep = 1.1*fTimeStep;
	if (fCriticalTimeStep < fTimeStep)
	{
		fTimeStep = 0.9f*fCriticalTimeStep;
	}
	std::cout << "\tUsed time step: " << fTimeStep << std::endl;

	// init bucket search
	// configure the grid for bucket search
	fRadiusDispersion = pCWorkspace->fGetRadiusDispersion();

	unsigned int uiNumActiveParticles = pCDiscretisation->uiGetNumberOfActiveParticles();
	fAverageRadius = 0;
	for (unsigned int i = 0; i < uiNumActiveParticles; i++)
	{
		unsigned int uiParticleIdx = pCDiscretisation->idGetActiveParticleIndex(i);
		fAverageRadius += pstGlobalVars->pfParticlesRadius[uiParticleIdx];
	}
	fAverageRadius /= uiNumActiveParticles;
	float fMaxParticleRadius = fAverageRadius*(1+fRadiusDispersion);

	float fBucketSize = 2*fMaxParticleRadius;
	float *afBoundingBox = pCDiscretisation->pfGetBoundingBox();
	uiNumBucketsX = (unsigned int)((afBoundingBox[1]-afBoundingBox[0])/fBucketSize) + 1;
	uiNumBucketsY = (unsigned int)((afBoundingBox[3]-afBoundingBox[2])/fBucketSize) + 1;
	uiNumBucketsZ = (unsigned int)((afBoundingBox[5]-afBoundingBox[4])/fBucketSize) + 1;
	std::cout << "\tBuckets: " << uiNumBucketsX << " x " << uiNumBucketsY << " x " << uiNumBucketsZ << std::endl;

	if ((uiDimensions == 2) && (uiNumBucketsZ > 1))
	{
		std::cout << "Warning: particles are not in the same plane for the configured 2D analysis!" << std::endl;
	}

	pCBuckets = new BucketSearch::buckets(afBoundingBox[0], afBoundingBox[2], afBoundingBox[4],
		fBucketSize, fBucketSize, fBucketSize, uiNumBucketsX, uiNumBucketsY, uiNumBucketsZ);

	pCBucketSearch = new BucketSearch::BucketSearch(pCBuckets, pstGlobalVars->pfParticlesCoordinates, uiTotalNumParticles);

	puiBucketIndex = new unsigned int[uiTotalNumParticles];
	VECT_vInit(puiBucketIndex, uiTotalNumParticles, (unsigned int)0);

	pCBucketSearch->vDistributeActivePointsToBuckets(pCDiscretisation->pIdGetActiveParticleIndexes(), uiNumActiveParticles);

	// Set up output variables
	pCWorkspace->vAddOutputVariable("DampingParamUsed");
	pCWorkspace->vAddOutputVariable("DampingParamCalculated");
	pCWorkspace->vAddOutputVariable("MaxPenetration");
	pCWorkspace->vAddOutputPointArray<vtkUnsignedIntArray>("Bucket", puiBucketIndex);
	pCWorkspace->vRemoveOutputLines();

	// Adjust radius
	for (unsigned int i = 0; i < uiNumActiveParticles; i++)
	{
		unsigned int particleIdx = pCDiscretisation->idGetActiveParticleIndex(i);
		float fRadius = pstGlobalVars->pfParticlesRadius[particleIdx];
		if (fRadius < (1-fRadiusDispersion/3)*fAverageRadius) pstGlobalVars->pfParticlesRadius[particleIdx] = (1-fRadiusDispersion/3)*fAverageRadius;
		if (fRadius > (1+fRadiusDispersion/3)*fAverageRadius) pstGlobalVars->pfParticlesRadius[particleIdx] = (1+fRadiusDispersion/3)*fAverageRadius;
	}

	pfAverageNeighbourDistance = new float[uiTotalNumParticles];
	pfMaxPenetration = new float[uiTotalNumParticles];
};

void CChangeRadiusSolver::vComputeInterractions(unsigned int uiParticleIdx, unsigned int uiNeighbourIndex)
{
	// compute particle penetration
	float *pfCoord1 = pstGlobalVars->pfParticlesCoordinates + 3*uiParticleIdx;
	float *pfCoord2 = pstGlobalVars->pfParticlesCoordinates + 3*uiNeighbourIndex;
	float fRadius1 = pstGlobalVars->pfParticlesRadius[uiParticleIdx];
	float fRadius2 = pstGlobalVars->pfParticlesRadius[uiNeighbourIndex];
	float fd12 = VECT_ComputeDistance(pfCoord1, pfCoord2, uiDimensions);
	float fParticleDistance = fd12 - fRadius1 - fRadius2;
	float fNormalForces[3];
	fNormalForces[0] = 0; fNormalForces[1] = 0; fNormalForces[2] = 0;
	if (fParticleDistance <= 0)
	{
		pfAverageNeighbourDistance[uiParticleIdx] += fParticleDistance;
		if (-fParticleDistance > pfMaxPenetration[uiParticleIdx]) pfMaxPenetration[uiParticleIdx] = -fParticleDistance;
		if (pCWorkspace->boGetUniqueInteractions())
		{
			pfAverageNeighbourDistance[uiNeighbourIndex] += fParticleDistance;
			if (-fParticleDistance > pfMaxPenetration[uiNeighbourIndex]) pfMaxPenetration[uiNeighbourIndex] = -fParticleDistance;
		}
		// Compute components of the force using unit stiffness
		for (unsigned int i = 0; i < uiDimensions; i++)
		{
			fNormalForces[i] += fParticleDistance*(pfCoord1[i] - pfCoord2[i])/fd12;
		}
		// manage membrane particles
		if (pCDiscretisation->boIsMembrane(uiParticleIdx))
		{
			if (pstGlobalVars->pucParticlesType[uiParticleIdx] == pstGlobalVars->pucParticlesType[uiNeighbourIndex])
			{
				// add atractive force
				for (unsigned int i = 0; i < uiDimensions; i++)
				{
					fNormalForces[i] += pstGlobalVars->pfParticlesRadius[uiParticleIdx]/4*(pfCoord1[i] - pfCoord2[i])/fd12;
				}
			}
		}
		if (boScaleMass) 
		{
			pfScaledMass[uiParticleIdx] += fTimeStep*fTimeStep*1.21f/2.0f;
			if (pCWorkspace->boGetUniqueInteractions())
			{
				pfScaledMass[uiNeighbourIndex] += fTimeStep*fTimeStep*1.21f/2.0f;
			}
		}
	}
	// update global forces
	for (unsigned int i = 0; i < uiDimensions; i++)
	{
		pstGlobalVars->pfReactionForces[3*uiParticleIdx+i] += fNormalForces[i];
		if (pCWorkspace->boGetUniqueInteractions())
		{
			pstGlobalVars->pfReactionForces[3*uiNeighbourIndex+i] -= fNormalForces[i];
		}
	}
}

void CChangeRadiusSolver::vComputeParticlesForces(void)
{
	unsigned int uiNumActiveParticles = pCDiscretisation->uiGetNumberOfActiveParticles();
	VECT_vAttribScalarToSubVector(pfAverageNeighbourDistance, pCDiscretisation->pIdGetActiveParticleIndexes(), uiNumActiveParticles, 0.0f);
	VECT_vAttribScalarToSubVector(pfMaxPenetration, pCDiscretisation->pIdGetActiveParticleIndexes(), uiNumActiveParticles, 0.0f);
	
	CExplicitSolver::vComputeParticlesForces();

	// Change particle radius
	float fMaxPenetrationAll = 0;
	boAnalysisFinished = true;
	for (unsigned int i = 0; i < uiNumActiveParticles; i++)
	{
		unsigned int particleIdx = pCDiscretisation->idGetActiveParticleIndex(i);
		if (!pCDiscretisation->boIsMembrane(particleIdx))
		{
			float fRadius = pstGlobalVars->pfParticlesRadius[particleIdx];
			float fAvDist = pfAverageNeighbourDistance[particleIdx];
			float fMaxPenetration = pfMaxPenetration[particleIdx];
			float *pfForces = pstGlobalVars->pfReactionForces + 3*particleIdx;
			float fForceNorm = (float)VECT_f8NormInf(pfForces, 3);

			float fMaxPenetrationAllowed = 0.001*fAverageRadius;

			if (!pCWorkspace->boPointIsFixed(particleIdx)) 
			{
				if (fMaxPenetration > fMaxPenetrationAll) fMaxPenetrationAll = fMaxPenetration;
				if (fMaxPenetration < fMaxPenetrationAllowed/10) boAnalysisFinished = false; // prevent rattling particles
				if (fForceNorm > fMaxPenetrationAllowed) boAnalysisFinished = false; // moving particles
			}

			if (fForceNorm < fMaxPenetrationAllowed)
			{
				if (fMaxPenetration < fMaxPenetrationAllowed)
				{
					if (fRadius < (1+fRadiusDispersion)*fAverageRadius) pstGlobalVars->pfParticlesRadius[particleIdx] += fMaxPenetrationAllowed;
				}
				else 
				{
					if (fMaxPenetration > 5*fMaxPenetrationAllowed) 
					{
						if (fRadius > (1-fRadiusDispersion)*fAverageRadius) 
							pstGlobalVars->pfParticlesRadius[particleIdx] -= fMaxPenetrationAllowed;
					}
				}
			}
		}
	}
	pCWorkspace->vSetOutputVariable("MaxPenetration", fMaxPenetrationAll);
}

bool CChangeRadiusSolver::boFinishAnalysis(void)
{
	return boAnalysisFinished;
}

} // namespace

