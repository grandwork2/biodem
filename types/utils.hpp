/****************************************************************************** 
 * 
 *  file:  utils.hpp
 * 
 *  Copyright (c) 2016, Grand R. Joldes.
 *  All rights reverved.
 * 
 *  
 *  THE SOFTWARE IS PROVIDED _AS IS_, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *  DEALINGS IN THE SOFTWARE.  
 *  
 *****************************************************************************/ 

#if !defined(_UTILS_HPP_)
#define _UTILS_HPP_

#include <vector>

#include "cdef.h"
#include "vect.hpp"

template <class T> class CMyArray
{
public:
	CMyArray(void) {};
	virtual ~CMyArray(void) {vRemoveAll();};
	T* &ElementAt(uint32 u32Idx) {return apTArray.at(u32Idx);};
	void vRemoveAll(void);
	void vSetSize(uint32 u32Size) {apTArray.reserve(u32Size);};
	void vRemoveAt(uint32 u32Idx);
	void vAdd(T *pT) {apTArray.push_back(pT);};
	void vInsertAt(uint32 u32Idx, T *pT) {apTArray.insert(u32Idx, pT);};
	uint32 u32GetCount(void) {return (uint32)apTArray.size();};
	T* &operator[](const uint32 i) {return apTArray.at(i);};
private:
	std::vector<T*> apTArray;
};


template <class T>
void CMyArray<T>::vRemoveAll(void)
{
	uint32 n = (uint32)apTArray.size();
	T* pT;
	for(uint32 i = 0; i < n; i++)
	{
		pT = apTArray.at(i);
		if (pT != NULL) delete pT;
	}
	apTArray.clear();
}

template <class T>
void CMyArray<T>::vRemoveAt(uint32 u32Idx)
{
	T* pT = apTArray.at(u32Idx);
	if (pT != NULL) delete pT;
	apTArray.erase(u32Idx);
}

#endif