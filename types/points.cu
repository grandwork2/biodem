/****************************************************************************** 
 * 
 *  file:  points.cu
 * 
 *  Copyright (c) 2016, Grand R. Joldes.
 *  All rights reverved.
 * 
 *  
 *  THE SOFTWARE IS PROVIDED _AS IS_, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *  DEALINGS IN THE SOFTWARE.  
 *  
 *****************************************************************************/ 
#include <stdlib.h>
#include <cuda_runtime.h>
#include "helper_cuda.h"

#include "points_gpu.hpp"

namespace BioDEM {

points_GPU::points_GPU(float *pfPointsCoord_, unsigned int uiNumPoints_):pfPointsCoord(pfPointsCoord_), uiNumPoints(uiNumPoints_)
{
	// allocate memory on device 
	tstDevicePoints points;
	points.uiCount = uiNumPoints;
	void *pvDevicePtr;
	checkCudaErrors(cudaMalloc(&pvDevicePtr, uiNumPoints*sizeof(float)));
	points.afX = (float *)pvDevicePtr;
	checkCudaErrors(cudaMalloc(&pvDevicePtr, uiNumPoints*sizeof(float)));
	points.afY = (float *)pvDevicePtr;
	checkCudaErrors(cudaMalloc(&pvDevicePtr, uiNumPoints*sizeof(float)));
	points.afZ = (float *)pvDevicePtr;
	checkCudaErrors(cudaMalloc(&pDevicePoints, sizeof(tstDevicePoints)));
	// copy data to device memory
	checkCudaErrors(cudaMemcpy(pDevicePoints, &points, 
			sizeof(tstDevicePoints), cudaMemcpyHostToDevice)); 
	float *pfCoord = (float *)malloc(uiNumPoints*sizeof(float));
	for (unsigned int i = 0; i < uiNumPoints; i++) pfCoord[i] = pfPointsCoord_[3*i];
	checkCudaErrors(cudaMemcpy(points.afX, pfCoord, 
			uiNumPoints*sizeof(float), cudaMemcpyHostToDevice));
	for (unsigned int i = 0; i < uiNumPoints; i++) pfCoord[i] = pfPointsCoord_[3*i+1];
	checkCudaErrors(cudaMemcpy(points.afY, pfCoord, 
			uiNumPoints*sizeof(float), cudaMemcpyHostToDevice));
	for (unsigned int i = 0; i < uiNumPoints; i++) pfCoord[i] = pfPointsCoord_[3*i+2];
	checkCudaErrors(cudaMemcpy(points.afZ, pfCoord, 
			uiNumPoints*sizeof(float), cudaMemcpyHostToDevice));
	free(pfCoord);
}

points_GPU::~points_GPU() 
{
	tstDevicePoints points;
	checkCudaErrors(cudaMemcpy(&points, pDevicePoints,
			sizeof(tstDevicePoints), cudaMemcpyDeviceToHost));
	checkCudaErrors(cudaFree(points.afX));
	checkCudaErrors(cudaFree(points.afY));
	checkCudaErrors(cudaFree(points.afZ));
	checkCudaErrors(cudaFree(pDevicePoints));
}

}; // namespace