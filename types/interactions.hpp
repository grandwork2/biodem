/****************************************************************************** 
 * 
 *  file:  interactions.hpp
 * 
 *  Copyright (c) 2016, Grand R. Joldes.
 *  All rights reverved.
 * 
 *  
 *  THE SOFTWARE IS PROVIDED _AS IS_, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *  DEALINGS IN THE SOFTWARE.  
 *  
 *****************************************************************************/ 

#ifndef __INTERACTIONS_HPP__
#define __INTERACTIONS_HPP__

#include <string>
#include <iostream>
#include <vector>

#include "options.hpp"

namespace BioDEM {

class CStepInteractions
{
public:
	CStepInteractions(unsigned int uiNumParticleTypes);
	~CStepInteractions();

	friend std::ostream& operator<<(std::ostream& out, const CStepInteractions& i);

	unsigned int uiNumParticleTypes;
	bool boConstantForceActive;
	bool boLinearForceActive;
	float *afConstantForce;
	float *afConstantForceDistance;
	float *afLinearForceStiffness;
	float *afLinearForceDistance;
};

class CInteractions 
{
public:
	typedef struct {
		unsigned int uiInitiator;
		unsigned int uiGrown;
		float fGrowFactor;
		bool boActive;
	} tstParticleGrowth;

	typedef struct {
		unsigned int uiInitiator;
		unsigned int uiDegraded;
		float fDegradationFactor;
		bool boActive;
	} tstParticleStiffnessDegradation;

	CInteractions();
	~CInteractions();

	bool boConfigure(unsigned int uiNumLoadSteps, unsigned int uiNumParticleTypes, program_options *pPO);
	bool boAddInterraction(std::string s);
	void vCopyInterractions(unsigned char ucTypeFrom, unsigned char ucTypeTo);
	friend std::ostream& operator<<(std::ostream& out, const CInteractions& i);

	tstParticleGrowth *pstGetParticleGrowth(void) {return &stParticleGrowth;};
	tstParticleStiffnessDegradation *pstGetParticleStiffnessDegradation(void) {return &stParticleStiffnessDegradation;};

	bool boConfigured(void) {return boConfigured_;};
	unsigned int uiGetInterractionMatrixSize(void) {return uiNumParticleTypes*uiNumParticleTypes;};
	bool boConstantForceActiveInStep(unsigned int uiStep) {return boConfigured_?pCStepInteractionsVector[uiStep]->boConstantForceActive:false;};
	bool boLinearForceActiveInStep(unsigned int uiStep) {return boConfigured_?pCStepInteractionsVector[uiStep]->boLinearForceActive:false;};
	float *pfConstantForce(unsigned int uiStep) {return boConfigured_?pCStepInteractionsVector[uiStep]->afConstantForce:NULL;};
	float *pfConstantForceDistance(unsigned int uiStep) {return boConfigured_?pCStepInteractionsVector[uiStep]->afConstantForceDistance:NULL;};
	float *pfLinearForceStiffness(unsigned int uiStep) {return boConfigured_?pCStepInteractionsVector[uiStep]->afLinearForceStiffness:NULL;};
	float *pfLinearForceDistance(unsigned int uiStep) {return boConfigured_?pCStepInteractionsVector[uiStep]->afLinearForceDistance:NULL;};
	float fConstantForce(unsigned int uiStep, unsigned int uiType1,  unsigned int uiType2) {return pCStepInteractionsVector[uiStep]->afConstantForce[uiType1*uiNumParticleTypes +uiType2];};
	float fConstantForceDistance(unsigned int uiStep, unsigned int uiType1,  unsigned int uiType2) {return pCStepInteractionsVector[uiStep]->afConstantForceDistance[uiType1*uiNumParticleTypes +uiType2];};
	float fLinearForceStiffness(unsigned int uiStep, unsigned int uiType1,  unsigned int uiType2) {return pCStepInteractionsVector[uiStep]->afLinearForceStiffness[uiType1*uiNumParticleTypes +uiType2];};
	float fLinearForceDistance(unsigned int uiStep, unsigned int uiType1,  unsigned int uiType2) {return pCStepInteractionsVector[uiStep]->afLinearForceDistance[uiType1*uiNumParticleTypes +uiType2];};
private:
	bool boConfigured_;
	unsigned int uiNumLoadSteps;
	unsigned int uiNumParticleTypes;
	std::vector<CStepInteractions*> pCStepInteractionsVector;
	tstParticleGrowth stParticleGrowth;
	tstParticleStiffnessDegradation stParticleStiffnessDegradation;
};

std::ostream& operator<<(std::ostream& out, const CStepInteractions& i);
std::ostream& operator<<(std::ostream& out, const CInteractions& i);

} // namespace

#endif /* __INTERACTIONS_HPP__ */