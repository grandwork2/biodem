/****************************************************************************** 
 * 
 *  file:  AnalysisStep.hpp
 * 
 *  Copyright (c) 2016, Grand R. Joldes.
 *  All rights reverved.
 * 
 *  
 *  THE SOFTWARE IS PROVIDED _AS IS_, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *  DEALINGS IN THE SOFTWARE.  
 *  
 *****************************************************************************/ 

#ifndef __ANALYSISSTEP_HPP__
#define __ANALYSISSTEP_HPP__

#include <string>

#include "options.hpp"

namespace BioDEM {

class CWorkspace;
class CGenericSolver;

class CAnalysisStep 
{
public:
	CAnalysisStep(CWorkspace *pW):pCWorkspace(pW) {};
	~CAnalysisStep() {};

	bool boConfigure(std::string config);
	CGenericSolver *pGetSolver(void) {return pCSolver;};
	std::string GetName(void) {return sName;};
	
private:
	std::string sName;
	// Solver for this step
	CGenericSolver *pCSolver;
	CWorkspace *pCWorkspace;
};

} // namespace

#endif /* __ANALYSISSTEP_HPP__ */