#if !defined(_FPCONTROL_H_)
#define _FPCONTROL_H_

#define _CRT_SECURE_NO_WARNINGS
#include <float.h>
#include <fpieee.h>
#include <excpt.h>

#ifdef __cplusplus
extern "C" {
#endif

#define FPCONTROL_uiClearStatus() _clearfp()
#define FPCONTROL_uiChangePrecision(uiNewPrec) _controlfp(uiNewPrec, _MCW_PC);
#define FPCONTROL_uiDisableInterrupts() _controlfp(_MCW_EM, _MCW_EM);

void FPCONTROL_vEnableInterrupt(unsigned int uiIntMask);

void FPCONTROL_vPrepareForComputations(void);

int FPCONTROL_iExceptionHandler( _FPIEEE_RECORD * );

#ifdef __cplusplus
}
#endif

#endif