/****************************************************************************** 
 * 
 *  file:  points_gpu.hpp
 * 
 *  Copyright (c) 2016, Grand R. Joldes.
 *  All rights reverved.
 * 
 *  
 *  THE SOFTWARE IS PROVIDED _AS IS_, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *  DEALINGS IN THE SOFTWARE.  
 *  
 *****************************************************************************/ 

#ifndef __POINTS_GPU_HPP__
#define __POINTS_GPU_HPP__

namespace BioDEM {

class points_GPU 
{
public:
	typedef struct stDevicePoints {
		unsigned int uiCount;
		float *afX, *afY, *afZ; // arrays of coordinates in device memory
	} tstDevicePoints;

	points_GPU(float *pfPointsCoord, unsigned int uiNumPoints);
	~points_GPU();

	tstDevicePoints *pstGetDevicePoints(void) {return pDevicePoints;};
	unsigned int uiGetNumPoints(void) {return uiNumPoints;};

protected:
	tstDevicePoints *pDevicePoints;
	float *pfPointsCoord;
	unsigned int uiNumPoints;
};

} // namespace

#endif /* __POINTS_GPU_HPP__ */