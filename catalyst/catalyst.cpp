/****************************************************************************** 
 * 
 *  file:  catalyst.cpp
 * 
 *  Copyright (c) 2016, Grand R. Joldes.
 *  All rights reverved.
 * 
 *  
 *  THE SOFTWARE IS PROVIDED _AS IS_, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *  DEALINGS IN THE SOFTWARE.  
 *  
 *****************************************************************************/ 
#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <string>
#include "vtkCPPythonScriptPipeline.h"
#include "vtkCPInputDataDescription.h"
#include "catalyst.hpp"

namespace BioDEM {

void vTransferThreadFunction(CatalystAdaptor *pCA)
{
	while (1)
	{
		pCA->pSemaphoreSendData->wait();
		if (pCA->boTerminateTransferThread) return;
		// send data
		pCA->coProcessorData->SetTimeData(pCA->dTime, pCA->iTimeStep);
		pCA->coProcessorData->ForceOutputOn();
		if(pCA->coProcessor->RequestDataDescription(pCA->coProcessorData) != 0)
		{
			// Catalyst needs to output data
			// Create data
			pCA->coProcessorData->GetInputDescriptionByName("input")->SetGrid(pCA->pMultiBlockDataSet);
			if (pCA->coProcessor->CoProcess(pCA->coProcessorData))
			{
				// save time step and time
				pCA->vectorTimeStep.push_back(pCA->iTimeStep);
				pCA->vectorTime.push_back(pCA->dTime);
			}
		}
		pCA->boReadyForTransfer = true;
	}
}

CatalystAdaptor::CatalystAdaptor() 
{
	boParallelDataTransfer = false;
	iLastTimeStep = -1;
	uiSendInterval = 0;
	coProcessor = vtkCPProcessor::New();
    coProcessor->Initialize();
	coProcessorData = vtkCPDataDescription::New();
    coProcessorData->AddInput("input");
	pSemaphoreSendData = new boost::interprocess::interprocess_semaphore(0);
	boReadyForTransfer = true;
	boTerminateTransferThread = false;
	pTransferThread = new boost::thread(vTransferThreadFunction, this);
	pMultiBlockDataSet = vtkMultiBlockDataSet::New();
}

bool CatalystAdaptor::boAddPythonScript(const char *pcScriptName)
{
	vtkCPPythonScriptPipeline* pipeline = vtkCPPythonScriptPipeline::New();
	bool boRet = pipeline->Initialize(pcScriptName) > 0;
	if (boRet) 
	{
		coProcessor->AddPipeline(pipeline);
		// get output file name
		std::string line;
		std::ifstream myfile(pcScriptName);
		std::string desiredString = "filename='";
		if (myfile.is_open())
		{
			while ( getline (myfile,line) )
			{
				std::size_t pos = line.find(desiredString);
				if (pos!=std::string::npos)
				{
					std::size_t pos1 = line.find("'", pos+desiredString.length()+1);
					if (pos1!=std::string::npos)
					{
						outputFileName = line.substr(pos + desiredString.length(), pos1-pos-desiredString.length());
						break;
					}
				}
			}
			myfile.close();
		 }
	}
	pipeline->Delete();
	return boRet;
}

CatalystAdaptor::~CatalystAdaptor()
{
	boTerminateTransferThread = true;
	// wake up the transfer thread
	pSemaphoreSendData->post();
	pTransferThread->join();
	// create output file with time steps
	if (outputFileName.length() > 0)
	{
		std::size_t pos = outputFileName.find("%t");
		if (pos!=std::string::npos)
		{
			std::size_t pos1 = outputFileName.find_last_of('/');
			std::string pvdFile;
			if (pos1 == std::string::npos) pvdFile = outputFileName.substr(0, pos - 1).append(".pvd");
			else pvdFile = outputFileName.substr(pos1 + 1, pos - pos1 - 2).append(".pvd");
			outputFileName.replace(outputFileName.find("%t"), 2, "%d");
			FILE *pf;
			fopen_s(&pf, pvdFile.c_str(), "w");
			if (pf)
			{
				char file_name[256];
				fprintf(pf, "<?xml version=\"1.0\"?>\n<VTKFile type=\"Collection\" version=\"0.1\"\n\t\tbyte_order=\"LittleEndian\"\n\t\tcompressor=\"vtkZLibDataCompressor\">\n\t<Collection>\n");
				for (unsigned int step = 0; step < vectorTimeStep.size(); step++)
				{
					sprintf(file_name, outputFileName.c_str(), vectorTimeStep[step]);
					fprintf(pf, "\t\t<DataSet timestep=\"%g\" group=\"\" part=\"0\"\n\t\t\tfile=\"%s\"/>\n", vectorTime[step], file_name);
				}
				fprintf(pf, "\t</Collection>\n</VTKFile>\n");
			}
			fclose(pf);
			std::cout << "Results written to " << pvdFile << std::endl;
		}
		else
		{
			std::cout << "Error writing results pvd file! Check the output file name in the python script contains %t!"<< std::endl;
		}
	}
	coProcessorData->Delete();
	coProcessor->Finalize();
    coProcessor->Delete();
	delete pTransferThread;
	delete pSemaphoreSendData;
	pMultiBlockDataSet->Delete();
}

bool CatalystAdaptor::boReadyToTransfer(unsigned int timeStep)
{
	bool boPrepare = false;
	if ((timeStep >= (iLastTimeStep + uiSendInterval)) || (timeStep == 0))
	{
		boPrepare = true;
		iLastTimeStep = timeStep;
	}
	if (boParallelDataTransfer && boPrepare)
	{
		boPrepare = boReadyForTransfer;
	}
	return boPrepare;
}

void CatalystAdaptor::vTransferData(int timeStep, double time)
{
	if (boParallelDataTransfer)
	{
		boReadyForTransfer = false;
		iTimeStep = timeStep;
		dTime = time;
		// wake up the transfer thread
		pSemaphoreSendData->post();
	} 
	else
	{
		coProcessorData->SetTimeData(time, timeStep);
		coProcessorData->ForceOutputOn();
		if(coProcessor->RequestDataDescription(coProcessorData) != 0)
		{
			// Catalyst needs to output data
			// Create data
			coProcessorData->GetInputDescriptionByName("input")->SetGrid(pMultiBlockDataSet);
			if (coProcessor->CoProcess(coProcessorData))
			{
				// save time step and time
				vectorTimeStep.push_back(timeStep);
				vectorTime.push_back(time);
			}
		}
	}
}


}