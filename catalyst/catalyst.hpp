/****************************************************************************** 
 * 
 *  file:  catalyst.hpp
 * 
 *  Copyright (c) 2016, Grand R. Joldes.
 *  All rights reverved.
 * 
 *  
 *  THE SOFTWARE IS PROVIDED _AS IS_, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *  DEALINGS IN THE SOFTWARE.  
 *  
 *****************************************************************************/ 

#ifndef __CATALYST_HPP__
#define __CATALYST_HPP__

#include <boost/thread/thread.hpp>
#include <boost/interprocess/sync/interprocess_semaphore.hpp>
#include <vtkCPDataDescription.h>
#include <vtkCPProcessor.h>
#include <vtkPolyData.h>
#include <vtkMultiBlockDataSet.h>
#include <vector>

namespace BioDEM {

class CatalystAdaptor {
public:
	CatalystAdaptor();
	~CatalystAdaptor();
	void vTransferData(int timeStep, double time);
	bool boAddPythonScript(const char *pcScriptName);
	void vSetParralelDataTransfer(bool boOn) {boParallelDataTransfer = boOn;};
	void vSetSendInterval(unsigned int uiInterval) {uiSendInterval = uiInterval;};
	bool boReadyToTransfer(unsigned int timeStep);
	void vSetOutputBlock(unsigned int uiBlockNo, vtkPolyData *pData) {pMultiBlockDataSet->SetBlock(uiBlockNo, pData);};
	void vSetForceOutput(bool boOn) {coProcessorData->SetForceOutput(boOn);};
	friend void vTransferThreadFunction(CatalystAdaptor *pCA);
private:
	vtkCPProcessor* coProcessor;
	vtkCPDataDescription* coProcessorData;
	vtkMultiBlockDataSet *pMultiBlockDataSet;
	bool boParallelDataTransfer;
	bool boReadyForTransfer;
	int iLastTimeStep;
	unsigned int uiSendInterval;
	int iTimeStep;
	double dTime;
	boost::thread *pTransferThread;
	bool boTerminateTransferThread;
	boost::interprocess::interprocess_semaphore *pSemaphoreSendData;
	std::string outputFileName;
	std::vector<unsigned int> vectorTimeStep;
	std::vector<float> vectorTime;
};

} // namespace
#endif /* __CATALYST_HPP__ */