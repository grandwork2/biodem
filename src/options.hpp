/****************************************************************************** 
 * 
 *  file:  options.hpp
 * 
 *  Copyright (c) 2016, Grand R. Joldes.
 *  All rights reverved.
 * 
 *  
 *  THE SOFTWARE IS PROVIDED _AS IS_, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *  DEALINGS IN THE SOFTWARE.  
 *  
 *****************************************************************************/ 

#ifndef __OPTIONS_HPP__
#define __OPTIONS_HPP__

#include <boost/program_options/variables_map.hpp>
#include <vector>

namespace BioDEM {

namespace po = boost::program_options;

typedef enum {
	enST,
	enMT,
	enGPU
} tenSolverType;

class program_options 
{
public:
	program_options(int argc, char* argv[]);
	~program_options();
	po::variables_map *pGetVariablesMap(void) {return &vm;};
	unsigned int uiGetNumDimensions(void);
	tenSolverType enGetSolverType(void);
	std::string GetSolverName(tenSolverType enSolverType);
	unsigned int uiGetNumberOfThreads(void);
	unsigned int uiGetGPU(void) {return vm["GPU"].as<unsigned int>();};
	unsigned int uiGetNumAdditionalParticles(void) {return vm["Num_additional_particles"].as<unsigned int>();};
	float fGetRadiusDispersion(void) {return vm["Radius_dispersion"].as<float>();};
	const std::vector<std::string> *GetLoads(void);
	const std::vector<std::string> *GetDisableLoads(void);
	const std::vector<std::string> *GetInteractions(void);
	const std::vector<std::string> *GetMembrane(void);
	const std::vector<std::string> *GetMembraneVolume(void);
	const std::vector<std::string> *GetGrowth(void);
	const std::vector<std::string> *GetChangeParticleTypeInSphere(void);
	const std::vector<std::string> *GetInteractionCopy(void);
	std::string GetInputFile(void) {return vm["input_file"].as<std::string>();};
	std::string GetResultsFileName(void) {return vm["Results_file"].as<std::string>();};
	std::string GetRadiusId(void) {return vm["Radius_id"].as<std::string>();};
	std::string GetDensityId(void) {return vm["Density_id"].as<std::string>();};
	std::string GetTypeId(void) {return vm["Type_id"].as<std::string>();};
	std::string GetMembraneId(void) {return vm["Membrane_id"].as<std::string>();};
	std::string GetWalls(void) {return vm["Walls"].as<std::string>();};
	bool boGetBoundingBox(float *pfBB);
	const std::vector<std::string> *GetSteps(void) {return &vm["Step"].as<std::vector<std::string>>();};
	const std::vector<std::string> *GetExplicitParams(void);
	const std::vector<std::string> *GetWallMotion(void);
	std::string GetParticleGrowthConfig(void) {return vm["Particle_growth"].as<std::string>();};
	std::string GetStiffnessDegradationConfig(void) {return vm["Stiffness_degradation"].as<std::string>();};
	unsigned int uiGetOutputInterval(void) {return vm["Output_interval"].as<unsigned int>();};
	bool boUseParallelOutput(void) {return vm["Parallel_output"].as<unsigned int>() > 0;};
	bool boCloseMembrane(void) {return vm["Close_membrane"].as<unsigned int>() > 0;};
	bool boGrowMembrane(void) {return vm["Grow_membrane"].as<unsigned int>() > 0;};
	bool boOutputAdditionalParticles(void) {return vm["Output_additional_particles"].as<unsigned int>() > 0;};
	bool boUniqueInteractions(void) {return vm["Unique_interactions"].as<unsigned int>() > 0;};
	std::string GetPythonScript(void) {return vm["Python_script"].as<std::string>();};
private:
	po::variables_map vm;
};

} // namespace

#endif /* __OPTIONS_HPP__ */