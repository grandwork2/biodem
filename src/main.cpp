#include <stdio.h>
#include <stdlib.h>

// VTK includes
#include <vtkDebugLeaks.h>
#include <vtkSmartPointer.h>
#include <vtkPoints.h>
#include <vtkFloatArray.h>
#include <vtkUnsignedIntArray.h>
#include <vtkPointData.h>
#include <vtkPolyData.h>
#include <vtkXMLPolyDataWriter.h>
#include <vtkGenericDataObjectReader.h>
#include <vtkSetGet.h>

#include <boost/timer/timer.hpp>
#include <boost/thread/thread.hpp>

#include "options.hpp"
#include "workspace.hpp"

void vSetBucketIndexes(unsigned int ix, unsigned int iy, unsigned int iz, unsigned int pointIdx, 
	vtkSmartPointer<vtkUnsignedIntArray> arrayBucketIdxX, vtkSmartPointer<vtkUnsignedIntArray> arrayBucketIdxY,
	vtkSmartPointer<vtkUnsignedIntArray> arrayBucketIdxZ)
{
	arrayBucketIdxX->SetValue(pointIdx, ix);
	arrayBucketIdxY->SetValue(pointIdx, iy);
	arrayBucketIdxZ->SetValue(pointIdx, iz);
};

using namespace BioDEM;
namespace po = boost::program_options;

int main(int argc, char* argv[])
{
	vtkDebugLeaks::SetExitError(true);
	program_options *pPO = new program_options(argc, argv);

	po::variables_map *pVarMap = pPO->pGetVariablesMap();
	CWorkspace *pWorkspace = new CWorkspace();

	// read program parameters
	std::cout << "Reading configuration ..." << std::endl;
	if (!pWorkspace->boConfigure(pPO)) exit(1);

	if (!pWorkspace->boRunAnalysis()) 
		std::cout << "Analysis completed with errors!" << std::endl;
	else
		std::cout << "Analysis completed successfully!" << std::endl;
	
	delete pWorkspace;
	
#if 0
	
	
	GPU_device *pGpuDevice = new GPU_device();
	//pGpuDevice->vPrintSystemInformation();
	int iDev = pGpuDevice->iGetMaxGflopsDeviceId();
	pGpuDevice->vSelectDevice(0);
	points_GPU *pGPUPoints = pGpuDevice->pInitPoints(pcParticles);
	BucketSearch::buckets_GPU *pGPUBUckets = pGpuDevice->pInitBuckets(pBuckets);
	BucketSearch::BucketSearch_GPU *pGPUBucketSearch = pGpuDevice->pInitBucketSearch(pGPUPoints, pGPUBUckets);

	// bucket points
	std::cout << "Single thread: " << std::endl;
	boost::timer::auto_cpu_timer *pBoostTimer = new boost::timer::auto_cpu_timer();
	pBucketSearch->vDistributePointsToBuckets();
	delete pBoostTimer;


	std::cout << "Multiple threads: " << std::endl;
	pBoostTimer = new boost::timer::auto_cpu_timer();
	pBucketSearch->vDistributePointsToBucketsMT();
	delete pBoostTimer;


	std::cout << "GPU: " << std::endl;
	pBoostTimer = new boost::timer::auto_cpu_timer();
	pGPUBucketSearch->vDistributePointsToBucketsGPU();
	pGpuDevice->vFinalizeComputations();
	delete pBoostTimer;

	pGPUBucketSearch->vReadBucketedPoints(pBucketSearch);
	pGpuDevice->vFinalizeComputations();

#if 1

	std::cout << "Init Catalyst: ";
	CatalystAdaptor *pCA = new CatalystAdaptor();
	pCA->boAddPythonScript("points.py");
	std::cout << "done" << std::endl;

	//return 0;
	// check the results
	vtkSmartPointer<vtkUnsignedIntArray> arrayBucketIdxX = vtkSmartPointer<vtkUnsignedIntArray>::New();
	arrayBucketIdxX->SetNumberOfValues(1);
	arrayBucketIdxX->SetName("BucketIdxX");
	arrayBucketIdxX->SetNumberOfTuples(pcParticles->uiGetCount());
	vtkSmartPointer<vtkUnsignedIntArray> arrayBucketIdxY = vtkSmartPointer<vtkUnsignedIntArray>::New();
	arrayBucketIdxY->SetNumberOfValues(1);
	arrayBucketIdxY->SetName("BucketIdxY");
	arrayBucketIdxY->SetNumberOfTuples(pcParticles->uiGetCount());
	vtkSmartPointer<vtkUnsignedIntArray> arrayBucketIdxZ = vtkSmartPointer<vtkUnsignedIntArray>::New();
	arrayBucketIdxZ->SetNumberOfValues(1);
	arrayBucketIdxZ->SetName("BucketIdxZ");
	arrayBucketIdxZ->SetNumberOfTuples(pcParticles->uiGetCount());
	for (unsigned int i = 0; i < pcParticles->uiGetCount(); i++)
	{
		arrayBucketIdxX->SetValue(i, 0);
		arrayBucketIdxY->SetValue(i, 0);
		arrayBucketIdxZ->SetValue(i, 0);
	}
	vBucketsOperation(pBucketSearch, vSetBucketIndexes, arrayBucketIdxX, arrayBucketIdxY, arrayBucketIdxZ);
	
	// output data
	vtkPolyData *pPolyData = pCA->pGetPolyData();
	vCreateVTKData(pcParticles, pPolyData);
	// Add point data
	pPolyData->GetPointData()->SetActiveScalars("BucketIdxX");
	pPolyData->GetPointData()->SetScalars(arrayBucketIdxX);
	pPolyData->GetPointData()->SetActiveScalars("BucketIdxY");
	pPolyData->GetPointData()->SetScalars(arrayBucketIdxY);
	pPolyData->GetPointData()->SetActiveScalars("BucketIdxZ");
	pPolyData->GetPointData()->SetScalars(arrayBucketIdxZ);
	//iWriteOutput(".\\points.vtp", pPolyData);

	unsigned int timeStep = 0;
	double time = 0;
	pCA->vSetParralelDataTransfer(true);
	pCA->vSetSendInterval(3);
	printf("Iteration \n");
	while (timeStep < 10) {
		double p[3];
		printf("\r%d", timeStep);
#if 1
		if (pCA->boReadyToTransfer(timeStep))
		{
			for (unsigned int i = 0; i < pcParticles->uiGetCount(); i++)
			{
				pPolyData->GetPoints()->GetPoint(i, p);
				p[0]+=5; p[1]+=5;
				pPolyData->GetPoints()->SetPoint(i, p);
			}
			pCA->vTransferData(timeStep, time);
		}
#endif
		timeStep++;
		time += 0.1;
		boost::this_thread::sleep( boost::posix_time::seconds(1) );
		
	}
	delete pCA;

#endif


	delete pBuckets;
	delete pcWalls;
	delete pGpuDevice;
	delete pcParticles;
	delete pBucketSearch;

#endif
	delete pPO;
	return 0;
}
