/****************************************************************************** 
 * 
 *  file:  options.cpp
 * 
 *  Copyright (c) 2016, Grand R. Joldes.
 *  All rights reverved.
 * 
 *  
 *  THE SOFTWARE IS PROVIDED _AS IS_, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *  DEALINGS IN THE SOFTWARE.  
 *  
 *****************************************************************************/ 
#define _CRT_SECURE_NO_WARNINGS
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/value_semantic.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/thread/thread.hpp>
#include <string>
#include <iostream>
#include <exception>

#include "main_gpu.hpp"
#include "options.hpp"

#define BIODEM_CFG_FILE "BioDEM.cfg"
#define BIODEM_PYTHON_SCRIPT "BioDEM.py"


namespace BioDEM {

program_options::program_options(int argc, char* argv[])
{
	po::options_description general("General options");
	po::options_description input_options("Input options");
	po::options_description solver_options("Solver options");
	po::options_description output_options("Output options");

	general.add_options()
		("help,H", "Displays a help message")
		("help-module", po::value<std::string>(), "Displays the help for a given module (input, solver, output)")
		("help-system", "Displays system information (GPU, CPU)")
		("config_file,C", po::value<std::string>()->default_value(BIODEM_CFG_FILE), "Configuration file to use")
		("input_file,I", po::value<std::string>()->required(), "Input file (vtk format)")
		("Results_file,R", po::value<std::string>()->default_value("results.vtp"), "Output file (vtp format)");

	po::positional_options_description pd;
	pd.add("config_file", 2).add("input-file", -1);

	input_options.add_options()
		("Radius_id", po::value<std::string>()->default_value("R"), "Name of scalars defining the radius in the input file")
		("Density_id", po::value<std::string>()->default_value("Ro"), "Name of scalars defining the density in the input file")
		("Membrane_id", po::value<std::string>()->default_value("Membrane"), "Name of scalars defining membranes in the input file")
		("Type_id", po::value<std::string>()->default_value("Type"), "Name of scalars defining the particle type in the input file");

	solver_options.add_options()
		("Solver_type", po::value<std::string>()->default_value("ST"), "Type of solver (ST, MT, GPU)")
		("GPU", po::value<unsigned int>()->default_value(0), "GPU number to use (if Solver_type = GPU)")
		("No_threads", po::value<unsigned int>(), "Number of threads to use (if Solver_type = MT)")
		("Dim", po::value<unsigned int>()->default_value(2), "Number of dimensions (2/3)")
		("Num_additional_particles", po::value<unsigned int>()->default_value(0), "Number of additional particles")
		("Radius_dispersion", po::value<float>()->default_value(0.2f), "Radius dispersion (0 <= d <= 1)")
		("BBox", po::value<std::string>(), "Bounding box")
		("Walls", po::value<std::string>()->required(), "Walls definition (as Xmin,Xmax,Ymin,Ymax,Zmin,Zmax,activeXmin,activeXmax,...")
		("Step", po::value<std::vector<std::string>>()->required()->composing(), "Step definition (Name, Solver, Params)")
		("Load", po::value<std::vector<std::string>>()->composing(), "Load definition (on specific particles)")
		("Change_type_in_sphere", po::value<std::vector<std::string>>()->composing(), "Change particle type for particles included in a sphere")
		("Disable_load", po::value<std::vector<std::string>>()->composing(), "Disabled loads at specific steps")
		("Explicit_param", po::value<std::vector<std::string>>()->composing(), "Explicit solver parameters")
		("Wall_motion", po::value<std::vector<std::string>>()->composing(), "Wall motion")
		("Unique_interactions", po::value<unsigned int>()->default_value(1), "Unique interaction detection between particle pairs (0/1)")
		("Interaction", po::value<std::vector<std::string>>()->composing(), "Interaction definition (between different particle types)")
		("Interaction_copy", po::value<std::vector<std::string>>()->composing(), "Copy interactions between two particle types")
		("Grow", po::value<std::vector<std::string>>()->composing(), "General particle growth")
		("Particle_growth", po::value<std::string>()->default_value(""), "Particle growth due to contact to an initiator particle type")
		("Stiffness_degradation", po::value<std::string>()->default_value(""), "Particle stiffness degradation due to contact to an initiator particle type")
		("Membrane", po::value<std::vector<std::string>>()->composing(), "Particles with constant tension membrane behaviour")
		("Membrane_volume", po::value<std::vector<std::string>>()->composing(), "Particles with constant tension membrane behaviour around a volume")
		("Close_membrane", po::value<unsigned int>()->default_value(0), "Add particles to close the membranes?")
		("Grow_membrane", po::value<unsigned int>()->default_value(0), "Add particles to grow the membranes?");

	output_options.add_options()
		("Output_interval", po::value<unsigned int>()->default_value(0), "Output interval (in time steps)")
		("Parallel_output", po::value<unsigned int>()->default_value(1), "If 1, output data is done in parallel with computations (0/1)")
		("Output_additional_particles", po::value<unsigned int>()->default_value(0), "Output additional particles? (0/1)")
		("Python_script", po::value<std::string>()->default_value(BIODEM_PYTHON_SCRIPT), "Python script for Catalyst");


	po::options_description all_options("Allowed options");
	all_options.add(general).add(solver_options).add(output_options).add(input_options);

	try {
		po::store(po::command_line_parser(argc, argv).options(all_options).positional(pd).run(), vm);
		const char *pcConfigFile = vm["config_file"].as<std::string>().c_str();
		po::store(po::parse_config_file<char>(pcConfigFile, all_options, false), vm);
	}
	catch(std::exception& e)
	{
		std::cout << "Error in parsing program options: " << e.what() << std::endl;
	}

	if (vm.count("help")) 
	{
		std::cout << general;
		exit(0);
	}

	if (vm.count("help-module")) {
		const std::string& s = vm["help-module"].as<std::string>();
		if (s == "solver") {
			std::cout << solver_options;
		} else if (s == "output") {
			std::cout << output_options;
		} else if (s == "input") {
			std::cout << input_options;
		} else {
			std::cout << "Unknown module '" 
				 << s << "' in the --help-module option\n";
			exit(1);
		}
		exit(0);
	}

	if (vm.count("help-system")) {
		std::cout << "Hardware concurency: " << boost::thread::hardware_concurrency() << std::endl << std::endl;
		std::cout << "GPU info: " << std::endl << std::endl;
		GPU_device::vPrintSystemInformation();
		exit(0);
	}

	try {
		vm.notify();
	}
	catch(std::exception& e)
	{
		std::cout << "Error in parsing program options: " << e.what() << std::endl;
		exit(1);
	}
};

program_options::~program_options()
{
	
};

unsigned int program_options::uiGetNumDimensions(void)
{
	unsigned int uiDimensions;
	if (vm.count("Dim")) {
		uiDimensions = vm["Dim"].as<unsigned int>();
		if ((uiDimensions < 2) || (uiDimensions > 3))
		{
			std::cout << "Dim value not acceptable (" << uiDimensions <<")!. Using Dim = 3." << std::endl;
			uiDimensions = 3;
		}
	}
	return uiDimensions;
};

bool program_options::boGetBoundingBox(float *pfBB)
{
	if (vm.count("BBox")) {
		std::string bbox = 	vm["BBox"].as<std::string>();
		if (sscanf(bbox.c_str(), "%f,%f,%f,%f,%f,%f", pfBB, pfBB+1, pfBB+2, pfBB+3, pfBB+4, pfBB+5) != 6)
		{
			std::cout << "Bounding box must have 6 components!" << std::endl;
			return false;
		}
		else return true;
	}
	else
	{
		std::cout << "Bounding box not specified!" << std::endl;
		return false;
	}
};

const std::vector<std::string> *program_options::GetLoads(void) 
{
	if (vm.count("Load")) 
		return &(vm["Load"].as<std::vector<std::string>>());
	else
		return NULL;
};

const std::vector<std::string> *program_options::GetChangeParticleTypeInSphere(void)
{
	if (vm.count("Change_type_in_sphere")) 
		return &(vm["Change_type_in_sphere"].as<std::vector<std::string>>());
	else
		return NULL;
};

const std::vector<std::string> *program_options::GetInteractionCopy(void)
{
	if (vm.count("Interaction_copy")) 
		return &(vm["Interaction_copy"].as<std::vector<std::string>>());
	else
		return NULL;
};

const std::vector<std::string> *program_options::GetMembrane(void)
{
	if (vm.count("Membrane")) 
		return &(vm["Membrane"].as<std::vector<std::string>>());
	else
		return NULL;
};

const std::vector<std::string> *program_options::GetMembraneVolume(void)
{
	if (vm.count("Membrane_volume")) 
		return &(vm["Membrane_volume"].as<std::vector<std::string>>());
	else
		return NULL;
};

const std::vector<std::string> *program_options::GetGrowth(void)
{
	if (vm.count("Grow")) 
		return &(vm["Grow"].as<std::vector<std::string>>());
	else
		return NULL;
};

const std::vector<std::string> *program_options::GetDisableLoads(void)
{
	if (vm.count("Disable_load")) 
		return &(vm["Disable_load"].as<std::vector<std::string>>());
	else
		return NULL;
};

const std::vector<std::string> *program_options::GetExplicitParams(void) 
{
	if (vm.count("Explicit_param")) 
		return &vm["Explicit_param"].as<std::vector<std::string>>();
	else
		return NULL;
};

const std::vector<std::string> *program_options::GetWallMotion(void)
{
	if (vm.count("Wall_motion")) 
		return &vm["Wall_motion"].as<std::vector<std::string>>();
	else
		return NULL;
};

const std::vector<std::string> *program_options::GetInteractions(void)
{
	if (vm.count("Interaction")) 
		return &(vm["Interaction"].as<std::vector<std::string>>());
	else
		return NULL;
};

unsigned int program_options::uiGetNumberOfThreads(void) 
{
	if (vm.count("No_threads")) 
	{
		unsigned int n = vm["No_threads"].as<unsigned int>();
		if (n > 0) return n;
		else return boost::thread::hardware_concurrency();
	}
	else return boost::thread::hardware_concurrency();
};

tenSolverType program_options::enGetSolverType(void)
{
	if (vm.count("Solver_type")) {
		const std::string& s = vm["Solver_type"].as<std::string>();
		if (s == "ST") {
			return enST;
		} else if (s == "MT") {
			return enMT;
		} else if (s == "GPU") {
			return enGPU;
		} else {
			std::cout << "Unknown solver type '" 
				 << s << "'. Using Solver_type = ST.";
		}
	}
	return enST;
};

std::string program_options::GetSolverName(tenSolverType enSolverType)
{
	if (enSolverType == enST) {
		return "Single threaded";
	} else if (enSolverType == enMT) {
		return "Multi threaded";
	} else if (enSolverType == enGPU) {
		return "GPU";
	} else {
		return "Unknown!";
	};
};

}; // namespace