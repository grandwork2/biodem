/****************************************************************************** 
 * 
 *  file:  main_gpu.hpp
 * 
 *  Copyright (c) 2016, Grand R. Joldes.
 *  All rights reverved.
 * 
 *  
 *  THE SOFTWARE IS PROVIDED _AS IS_, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *  DEALINGS IN THE SOFTWARE.  
 *  
 *****************************************************************************/ 

#ifndef __MAIN_GPU_HPP__
#define __MAIN_GPU_HPP__

#include <cuda_runtime.h>
#include "helper_cuda.h"
#include "points_gpu.hpp"
#include "bucketsearch_gpu.hpp"

namespace BioDEM {

class GPU_device 
{
public:
	GPU_device();
	~GPU_device();

	void vSelectDevice(int devID);
	static int iGetMaxGflopsDeviceId(void) {return gpuGetMaxGflopsDeviceId();};
	static void vFinalizeComputations(void) {checkCudaErrors(cudaDeviceSynchronize());};
	static void vPrintSystemInformation(void);
	
	//points_GPU *pInitPoints(CPoints *pPoints_, unsigned int uiNumPoints);
	BucketSearch::buckets_GPU *pInitBuckets(BucketSearch::buckets *pBuckets_);
	BucketSearch::BucketSearch_GPU *pInitBucketSearch(points_GPU *pGPUPoints, BucketSearch::buckets_GPU *pGPUBuckets);

private:
	points_GPU *pPoints;
	BucketSearch::buckets_GPU *pBuckets;
	BucketSearch::BucketSearch_GPU *pBucketSearch;
};

} // namespace

#endif /* __MAIN_GPU_HPP__ */