/****************************************************************************** 
 * 
 *  file:  main.cu
 * 
 *  Copyright (c) 2016, Grand R. Joldes.
 *  All rights reverved.
 * 
 *  
 *  THE SOFTWARE IS PROVIDED _AS IS_, WITHOUT WARRANTY OF ANY KIND, EXPRESS 
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
 *  THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *  DEALINGS IN THE SOFTWARE.  
 *  
 *****************************************************************************/ 
#include <stdlib.h>
#include <stdio.h>
#include <cuda_runtime.h>
#include <vector>
#include "helper_cuda.h"

#include "main_gpu.hpp"

namespace BioDEM {

GPU_device::GPU_device():pPoints(NULL), pBuckets(NULL), pBucketSearch(NULL)
{
};

GPU_device::~GPU_device()
{
	if (pPoints != NULL) delete pPoints;
	if (pBuckets != NULL) delete pBuckets;
	if (pBucketSearch != NULL) delete pBucketSearch;
	cudaDeviceReset();
};

void GPU_device::vSelectDevice(int devID)
{
	 gpuDeviceInit(devID);
};

void GPU_device::vPrintSystemInformation(void)
{
	int deviceCount;
	std::vector<int> aSuitableDevices;
    checkCudaErrors(cudaGetDeviceCount(&deviceCount));
    if (deviceCount == 0)
        printf("There is no device supporting CUDA\n");
    int dev;
    for (dev = 0; dev < deviceCount; ++dev) {
        cudaDeviceProp deviceProp;
        checkCudaErrors(cudaGetDeviceProperties(&deviceProp, dev));
        if (dev == 0) {
            if (deviceProp.major == 9999 && deviceProp.minor == 9999)
                printf("There is no device supporting CUDA.\n");
            else if (deviceCount == 1)
                printf("There is 1 device supporting CUDA\n");
            else
                printf("There are %d devices supporting CUDA\n", deviceCount);
        }
        printf("\nDevice %d: \"%s\"\n", dev, deviceProp.name);
        printf("  Major revision number:                         %d\n",
               deviceProp.major);
        printf("  Minor revision number:                         %d\n",
               deviceProp.minor);
        printf("  Total amount of global memory:                 %u bytes\n",
               deviceProp.totalGlobalMem);
    #if CUDART_VERSION >= 2000
        printf("  Number of multiprocessors:                     %d\n",
               deviceProp.multiProcessorCount);
        printf("  Number of cores:                               %d\n",
               8 * deviceProp.multiProcessorCount);
    #endif
        printf("  Total amount of constant memory:               %u bytes\n",
               deviceProp.totalConstMem); 
        printf("  Total amount of shared memory per block:       %u bytes\n",
               deviceProp.sharedMemPerBlock);
        printf("  Total number of registers available per block: %d\n",
               deviceProp.regsPerBlock);
        printf("  Warp size:                                     %d\n",
               deviceProp.warpSize);
        printf("  Maximum number of threads per block:           %d\n",
               deviceProp.maxThreadsPerBlock);
        printf("  Maximum sizes of each dimension of a block:    %d x %d x %d\n",
               deviceProp.maxThreadsDim[0],
               deviceProp.maxThreadsDim[1],
               deviceProp.maxThreadsDim[2]);
        printf("  Maximum sizes of each dimension of a grid:     %d x %d x %d\n",
               deviceProp.maxGridSize[0],
               deviceProp.maxGridSize[1],
               deviceProp.maxGridSize[2]);
        printf("  Maximum memory pitch:                          %u bytes\n",
               deviceProp.memPitch);
        printf("  Texture alignment:                             %u bytes\n",
               deviceProp.textureAlignment);
        printf("  Clock rate:                                    %.2f GHz\n",
               deviceProp.clockRate * 1e-6f);
    #if CUDART_VERSION >= 2000
        printf("  Concurrent copy and execution:                 %s\n",
               deviceProp.deviceOverlap ? "Yes" : "No");
    #endif

		if (deviceProp.major >= 2) aSuitableDevices.push_back(dev);
    }
	printf("\n\n");
	printf("Number of suitable devices (Compute capability > 2.0): ");
	unsigned int n = (unsigned int)aSuitableDevices.size();
	if (n > 0)
	{
		printf("%d; device ids =", n);
		for (unsigned int i = 0; i < n; i++)
		{
			printf(" %d,", aSuitableDevices[i]);
		}
		printf("\b \b\n\n");
	}
	else printf("none!\n\n");
};

/*points_GPU *GPU_device::pInitPoints(CPoints *pPoints_, unsigned int uiNumPoints)
{
	pPoints = new points_GPU(pPoints_->pfGetCoordinates(), uiNumPoints);
	return pPoints;
};*/

BucketSearch::buckets_GPU *GPU_device::pInitBuckets(BucketSearch::buckets *pBuckets_)
{
	pBuckets = new BucketSearch::buckets_GPU(pBuckets_);
	return pBuckets;
};

BucketSearch::BucketSearch_GPU *GPU_device::pInitBucketSearch(points_GPU *pGPUPoints, BucketSearch::buckets_GPU *pGPUBuckets)
{
	pBucketSearch = new BucketSearch::BucketSearch_GPU(pGPUBuckets, pGPUPoints);
	return pBucketSearch;
}

}; // namespace